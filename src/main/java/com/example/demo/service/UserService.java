package com.example.demo.service;

import com.example.common.camel.producer.ProducerInterceptor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@AllArgsConstructor
public class UserService {
  private static final String TOPIC = "users";
  private final ProducerInterceptor producer;

  public void bookCab(String message) {
    log.info(String.format("$$ -> Broadcasting --> %s", message));
    producer.produce(TOPIC, null, null, null, message, null);
  }
}