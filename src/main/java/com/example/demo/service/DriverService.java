package com.example.demo.service;

import com.example.common.camel.common.MessageRequest;
import com.example.common.camel.consumer.MessageProcessor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DriverService implements MessageProcessor {
  public void process(MessageRequest message) {
    log.info(String.format("$$ -> Receiving -> %s", message));
  }

}
