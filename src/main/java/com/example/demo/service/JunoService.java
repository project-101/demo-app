package com.example.demo.service;

import com.example.demo.juno.model.JunoApiResponse;
import com.example.demo.juno.model.Session;
import com.example.demo.utils.HttpHeadersUtils;
import com.example.demo.utils.RestTemplateUtils;
import com.example.demo.utils.WebClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import java.util.Objects;
import java.util.UUID;

@Slf4j
public class JunoService {

  private static String env = "preprod";
  private static String client = "android";
  private static String sessionUrl;
  private static String createCustomerUrl;
  private static String authenticateEmailUrl;
  private static String addToCartUrl;
  private static String clearCartUrl;
  private static String addShippingAddressCartUrl;
  private static String orderPaymentUri;
  private static String junoAuthTokenUrl;
  private String session;

  static {
    setup(env);
  }

  public JunoService() {
    this.session = createSession();
  }

  public static void setup(String env) {
    JunoService.env = env;
    String host;
    switch (env) {
      case "testing":
        host = "https://api-testing.lenskart.com/";
        break;
      case "singapore":
        host = "https://api-lenskartsg.lenskart.com";
        break;
      case "preprod":
        host = "https://api-preprod.lenskart.com/";
        break;
      case "prod":
        host = "https://api.lenskart.com/";
        break;
      default:
        host = "https://localhost/";
        break;
    }

    sessionUrl = host + "v2/sessions";
    createCustomerUrl = host + "v2/customers/register";
    authenticateEmailUrl = host + "v2/customers/authenticate";
    addToCartUrl = host + "v2/carts";
    clearCartUrl = host + "v2/carts/items";
    addShippingAddressCartUrl = host + "v2/carts/shippingAddress";
    junoAuthTokenUrl = host + "v2/money/wallet/generate-token";
    orderPaymentUri = host + "v2/orderpayment/";
  }

  public HttpHeaders getDefaultHeader() {
    HttpHeaders httpHeaders = HttpHeadersUtils.getHttpHeaders();
    httpHeaders.add("X-Api-Client", client);
    httpHeaders.add("X-Session-Token", session);
    return httpHeaders;
  }

  public HttpHeaders getXAuthHeader() {
    HttpHeaders httpHeaders = HttpHeadersUtils.getAuthHttpHeader();
    httpHeaders.add("X-Api-Client", client);
    httpHeaders.add("X-Session-Token", session);
    return httpHeaders;
  }

  public HttpHeaders getJunoAuthHeader() {
    HttpHeaders httpHeaders = HttpHeadersUtils.getHttpHeaders();
    httpHeaders.add("X-Api-Client", client);
    httpHeaders.add("X-Session-Token", session);
    httpHeaders.add("X-Api-Auth-Token", getJunoAuthToken());
    return httpHeaders;
  }

  public String getJunoAuthToken() {
    Session jsonObject = Objects.requireNonNull(WebClientUtils.exchange(junoAuthTokenUrl, HttpMethod.POST,
        new HttpEntity<>(getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<Session>>() {
        }).block()).getResult();
    return jsonObject.getToken();
  }

  public String createSession() {
    Session jsonObject = Objects.requireNonNull(WebClientUtils.exchange(sessionUrl, HttpMethod.POST,
        new HttpEntity<>(HttpHeadersUtils.getHttpHeaders()), new ParameterizedTypeReference<JunoApiResponse<Session>>() {
        }).block()).getResult();
    session = jsonObject.getId();
    return jsonObject.getId();
  }

  public void login(String email) {
    String body = "{\n" +
        "    \"username\": \"" + email + "\",\n" +
        "    \"password\": \"testing@123\"\n" +
        "}";
    createSession();
    Session jsonObject = Objects.requireNonNull(WebClientUtils.exchange(authenticateEmailUrl, HttpMethod.POST,
        new HttpEntity<>(body, getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<Session>>() {
        }).block()).getResult();
    session = jsonObject.getToken();
  }

  public Session createCustomer() {
    String requestBody = "{\n" +
        "   \"dob\": \"1990-12-12\",\n" +
        "   \"email\": \"" + randomString() + "@gmail.com" + "\",\n" +
        "   \"firstName\": \"haha\",\n" +
        "   \"gender\": \"Male\",\n" +
        "   \"isFirstOrder\": false,\n" +
        "   \"isLoyalty\": true,\n" +
        "   \"lastName\": \"haha\",\n" +
        "   \"loyaltyStartDate\": 0,\n" +
        "   \"mobile\": \"" + randomLong(10) + "\",\n" +
        "   \"password\": \"testing@123\",\n" +
        "   \"phoneCode\": \"+91\"\n" +
        " }";
    Session result = Objects.requireNonNull(WebClientUtils.exchange(createCustomerUrl, HttpMethod.POST,
        new HttpEntity<>(requestBody, getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<Session>>() {
        }).block()).getResult();
    session = result.getToken();
    return result;
  }

  public void addToCartForDrools() {
    String requestBody = "{\n" +
        "    \"powerType\": \"single_vision\",\n" +
        "    \"productId\": \"129078\",\n" +
        "    \"packageId\": \"59a65ce3e4b014c5d41d7f50\"\n" +
        "}";
    RestTemplateUtils.exchange(addToCartUrl, HttpMethod.POST, new HttpEntity<>(requestBody, getDefaultHeader()), Object.class);
    RestTemplateUtils.exchange(addToCartUrl, HttpMethod.POST, new HttpEntity<>(requestBody, getDefaultHeader()), Object.class);
    requestBody = "{\n" +
        "    \"productId\": \"128269\"\n" +
        "}";
    RestTemplateUtils.exchange(addToCartUrl, HttpMethod.POST, new HttpEntity<>(requestBody, getDefaultHeader()), Object.class);
  }

  public void clearCart() {
    RestTemplateUtils.exchange(addToCartUrl, HttpMethod.GET, new HttpEntity<>(getDefaultHeader()), Object.class);
    RestTemplateUtils.exchange(clearCartUrl, HttpMethod.DELETE, new HttpEntity<>(getDefaultHeader()), Object.class);
  }

  public <T> T addToCart() {
    String requestBody = "{\n" +
        "    \"productId\": \"38749\",\n" +
        "    \"quantity\": \"2\",\n" +
        "    \"prescription\": {\n" +
        "        \"dob\": \"\",\n" +
        "        \"gender\": \"\",\n" +
        "        \"notes\": \"\",\n" +
        "        \"userName\": \"xyz\",\n" +
        "        \"left\": {\n" +
        "            \"boxes\": \"1\",\n" +
        "            \"sph\": \"-0.75\"\n" +
        "        },\n" +
        "        \"right\": {\n" +
        "            \"boxes\": \"1\",\n" +
        "            \"sph\": \"-0.75\"\n" +
        "        }\n" +
        "    }\n" +
        "}";
    return Objects.requireNonNull(WebClientUtils.exchange(addToCartUrl, HttpMethod.POST,
        new HttpEntity<>(requestBody, getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<T>>() {
        }).block()).getResult();
  }

  public <T> T addShippingAddress() {
    String requestBody = "{\n" +
        "    \"address\": {\n" +
        "        \"firstName\": \"haha\",\n" +
        "        \"lastName\": \"haha\",\n" +
        "        \"phone\": \"9999999999\",\n" +
        "        \"email\": \"haha@gmail.com\",\n" +
        "        \"addressline1\": \"12\",\n" +
        "        \"addressline2\": \"sg\",\n" +
        "        \"landmark\": \"12\",\n" +
        "        \"postcode\": \"110001\",\n" +
        "        \"city\": \"NEW DELHI\",\n" +
        "        \"state\": \"Delhi\",\n" +
        "        \"country\": \"IN\",\n" +
        "        \"formattedAddress\": \"sg\"\n" +
        "    }\n" +
        "}";
    return RestTemplateUtils.exchange(addShippingAddressCartUrl, HttpMethod.POST,
        new HttpEntity<>(requestBody, getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<T>>() {
        }).getResult();
  }

  public <T> T doOrderPayment(String method) {
    String requestBody;
    if (method.equalsIgnoreCase("cod")) {
      requestBody = "{\n" +
          "    \"paymentInfo\": {\n" +
          "        \"paymentMethod\": \"cod\"\n" +
          "    },\n" +
          "    \"device\": \"" + client + "\",\n" +
          "}";
    } else {
      requestBody = "{\n" +
          "    \"device\": \"" + client + "\",\n" +
          "    \"leadSource\": null,\n" +
          "    \"nonFranchiseOrder\": false,\n" +
          "    \"orderId\": null,\n" +
          "    \"paymentInfo\": {\n" +
          "        \"card\": {\n" +
          "            \"cardBrand\": null,\n" +
          "            \"cardMode\": null,\n" +
          "            \"expired\": null,\n" +
          "            \"oneClick\": false,\n" +
          "            \"oneClickToken\": null\n" +
          "        },\n" +
          "        \"gatewayId\": \"PU:TEZ\",\n" +
          "        \"netbanking\": {},\n" +
          "        \"partialPaymentInfo\": {\n" +
          "            \"partialPayment\": false,\n" +
          "            \"partialPaymentAmount\": 0,\n" +
          "            \"partialPaymentMethod\": null\n" +
          "        },\n" +
          "        \"paymentMethod\": \"payuwallet\",\n" +
          "        \"subscriptionOrderId\": null\n" +
          "    }\n" +
          "}";
    }
    return RestTemplateUtils.exchange(orderPaymentUri, HttpMethod.POST,
        new HttpEntity<>(requestBody, getDefaultHeader()), new ParameterizedTypeReference<JunoApiResponse<T>>() {
        }).getResult();
  }


  private static String randomString() {
    return UUID.randomUUID().toString().replace("-", "");
  }

  private static long randomLong(int digit) {
    return (long) (Math.random() * Math.pow(10, digit));
  }


}
