package com.example.demo.service;

import com.example.common.Constant;
import com.example.common.utils.sequence.SequenceGenerator;
import com.example.demo.dto.DownloadDto;
import com.example.demo.dto.TestAddress;
import com.example.demo.dto.TestBackSync;
import com.example.demo.dto.TestOrder;
import com.example.demo.juno.model.JunoApiResponse;
import com.example.demo.juno.model.Session;
import com.example.demo.utils.ConverterUtils;
import com.example.demo.utils.HttpHeadersUtils;
import com.example.demo.utils.RestTemplateUtils;
import com.example.demo.utils.WebClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Service
@Slf4j
public class TestService {

  private final SequenceGenerator redisSequenceGenerator;
  private final SequenceGenerator mongoSequenceGenerator;
  private static String url = "https://api-preprod.lenskart.com/juno-utility/v2/admin/store/code/st02";
  private static HttpHeaders headers = HttpHeadersUtils.getHttpHeaders();
  private static RestTemplate restTemplate;
  @Autowired
  private Executor executor;

  {
    headers.add("X-Auth-Token", "8e8b0816-4c73-4f08-8f7d-022dcd186a91");
    restTemplate = new RestTemplate();
    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
    mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
    restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
  }

  public TestService(@Qualifier(Constant.REDIS_SEQ_GEN_BEAN) SequenceGenerator redisSequenceGenerator,
                     @Qualifier(Constant.MONGO_SEQ_GEN_BEAN) SequenceGenerator mongoSequenceGenerator) {
    this.redisSequenceGenerator = redisSequenceGenerator;
    this.mongoSequenceGenerator = mongoSequenceGenerator;
  }

  @Async
  public void syncShippingAndBillingAddress(Long orderId) {
    String orderUrl = "https://api.lenskart.com/juno-order/v2/orders/{orderId}";
    TestOrder order = Objects.requireNonNull(WebClientUtils.exchange(orderUrl, HttpMethod.GET, new HttpEntity<>(headers),
        new ParameterizedTypeReference<JunoApiResponse<TestOrder>>() {
        }, orderId).block()).getResult();
    String email = order.getCustomerEmail();
    String billingEmail = order.getBillingAddress().getEmail();
    String shippingEmail = order.getShippingAddress().getEmail();
    if (StringUtils.isEmpty(shippingEmail)) {
      TestAddress address = order.getShippingAddress();
      address.setEmail(email);
      log.info("shipping Address: {}", ConverterUtils.toStringUsingJavaProperty(address));
      String shippingUrl = "https://api.lenskart.com/juno-order/v2/orders/{orderId}/shippingAddress";
      WebClientUtils.exchange(shippingUrl, HttpMethod.PUT, new HttpEntity<>(address, headers), Object.class, orderId).block();
    }
    if (StringUtils.isEmpty(billingEmail)) {
      TestAddress address = order.getBillingAddress();
      address.setEmail(email);
      log.info("billing Address: {}", ConverterUtils.toStringUsingJavaProperty(address));
      String billingUrl = "https://api.lenskart.com/juno-order/v2/orders/{orderId}/billingAddress";
      WebClientUtils.exchange(billingUrl, HttpMethod.PUT, new HttpEntity<>(address, headers), Object.class, orderId).block();
    }
  }

  public void syncOrderStatus(TestBackSync request) {
    String backsyncUrl = "https://api.lenskart.com/juno-order/v2/orders/{orderId}/syncStatus";
    WebClientUtils.exchange(backsyncUrl, HttpMethod.PATCH, new HttpEntity<>(request, headers), Object.class, request.getOrderId()).block();
  }

  @Async
  public CompletableFuture<Mono<Object>> testWebClient() {
    return CompletableFuture.completedFuture(WebClientUtils.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), Object.class));
  }

  @Async
  public CompletableFuture<Object> testRestClient() {
    return CompletableFuture.completedFuture(RestTemplateUtils.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), Object.class));
  }

  public void testCompletableFuture() {
    CompletableFuture.runAsync((() -> log.info("Thread is: {}", Thread.currentThread().getName())));
  }

  public void testAsyncThread() {
    log.info("Thread is: {}", Thread.currentThread().getName());
  }

  public void testSyncThread() {
    log.info("Thread is: {}", Thread.currentThread().getName());
  }

  @Async
  public CompletableFuture<Long> testRedisSequenceGenerator() {
    return CompletableFuture.completedFuture(redisSequenceGenerator.generateId(String.class));
  }

  public void testRedisSequenceGeneratorSync() {
    redisSequenceGenerator.generateId(String.class);
  }

  @Async
  public CompletableFuture<Long> testMongoSequenceGenerator() {
    return CompletableFuture.completedFuture(mongoSequenceGenerator.generateId(String.class));
  }

  public void testMongoSequenceGeneratorSync() {
    mongoSequenceGenerator.generateId(String.class);
  }

  public void orderRecursionTest(String email) {
    JunoService service = new JunoService();
    service.login(email);
    try {
      service.clearCart();
    } catch (Exception ignored) {
    }
    service.addToCartForDrools();
    service.addShippingAddress();
    service.doOrderPayment("cod");
  }

  public void orderRecursionTest() {
    JunoService junoService = new JunoService();
    Session session = junoService.createCustomer();
    orderRecursionTest(session.getUsername());
  }

  public DownloadDto imageDownload(String filename) throws IOException {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Collections.singletonList(MediaType.ALL));
    ResponseEntity<byte[]> file = restTemplate.exchange("https://static.lenskart.com/media/prescription/{filename}", HttpMethod.GET,
        new HttpEntity<>(headers), byte[].class, filename);
    MediaType mediaType = MediaType.APPLICATION_OCTET_STREAM;
    try {
      if (file.getHeaders().containsKey("Content-Type")) {
        mediaType = MediaType.valueOf(Objects.requireNonNull(file.getHeaders().get("Content-Type")).get(0));
      }
    } catch (Exception ignored) {
    }
    return new DownloadDto(filename, file.getBody(), mediaType);
  }

  public void testSomething() {
    executor.execute(this::testMultiThreadCanRunPrivateMethod);
  }

  private void testMultiThreadCanRunPrivateMethod() {
    log.info("ran in testMultiThreadCanRunPrivateMethod");
  }

}
