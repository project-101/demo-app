package com.example.demo.service;

import com.example.demo.dto.docsap.Goods;
import com.example.demo.dto.docsap.InvoiceAddress;
import com.example.demo.dto.docsap.TaxInvoice;
import com.example.demo.dto.docsap.UnitPriceSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class ThymeLeafPractice {
  @Autowired
  private TemplateEngine htmlTemplateEngine;

  public String getHtmlText(Locale locale) {
    final Context ctx = new Context(locale);
    List<Goods> goods = new ArrayList<>();
    goods.add(new Goods("Insurance -Product Id:135217 Inclusive of Lens Package: HSN Code:'9962'", new UnitPriceSummary()));
    goods.add(new Goods("Golden Black Full Rim Hexagon Large (Size-52) John " +
        "Jacobs Pro Titanium JJ E11835 - C4 Eyeglasses -Product " +
        "Id:134725 " +
        "Inclusive of Lens Package: hydrophobic anti-glare - Rs 1000.0 " +
        "Photo-Chromatic Coating | HSN Code:'9004'", new UnitPriceSummary()));
    goods.add(new Goods("3", new UnitPriceSummary()));
    goods.add(new Goods("4", new UnitPriceSummary()));
    TaxInvoice taxInvoice = new TaxInvoice("Hello, world", "123 & 123", LocalDate.now(ZoneId.of("Asia/Kolkata")),
        new InvoiceAddress(), new InvoiceAddress(), goods, new UnitPriceSummary());
    ctx.setVariable("invoice", taxInvoice);
    return this.htmlTemplateEngine.process("/html/docs-app-tax-invoice", ctx);
//    return this.htmlTemplateEngine.process("/html/index", ctx);
  }

}
