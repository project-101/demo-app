package com.example.demo.service;

import com.example.demo.aspect.redislock.TransactionAction;
import com.example.demo.aspect.redislock.TransactionLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RedisLockTestService {

  @TransactionLock(action = TransactionAction.BYVALUE, value = "TEST1")
  public void test1() {
    log.info("Test1");
  }

  @TransactionLock(action = TransactionAction.BYVALUE, value = "TEST2")
  public void test2() {
    log.info("Test2");
    throw new NullPointerException("Test2");
  }

  @TransactionLock(action = TransactionAction.BYVALUE, value = "TEST3")
  public void test3(String val) {
    log.info("Test3");
  }

  @TransactionLock(action = TransactionAction.BYVALUE, value = "TEST4")
  public void test4(String val) {
    log.info("Test4");
    throw new NullPointerException("Test4");
  }

  @TransactionLock(action = TransactionAction.BYVALUE, value = "TEST5")
  public void test5(String val, String val2) {
    log.info("Test5");
  }


}
