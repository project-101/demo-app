//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum OrderTrackingStatus {
  ORDER_PLACED("ORDER_PLACED"),
  ORDER_NOT_CONFIRMED("ORDER_NOT_CONFIRMED"),
  PAYMENT_NOT_INITIATED("PAYMENT_NOT_INITIATED"),
  STOCKED_OUT("STOCKED_OUT"),
  FITTING_DONE("FITTING_DONE"),
  QUALITY_CHECK_DONE("QUALITY_CHECK_DONE"),
  PACKED("PACKED"),
  DISPATCHED("DISPATCHED"),
  DELIVERED("DELIVERED"),
  CANCELLED("CANCELLED"),
  PROCESSING_RED_FLAG("PROCESSING_RED_FLAG"),
  HOLDED("HOLDED"),
  RETURN_REQUESTED("RETURN_REQUESTED"),
  RETURN_TO_ORIGIN("RETURN_TO_ORIGIN"),
  PARTIAL_RETURNED("PARTIAL_RETURNED"),
  RETURNED("RETURNED"),
  RETURN("RETURN"),
  IN_HOUSE_PROCESSING("IN_HOUSE_PROCESSING");

  private final String value;
  private static final Map<String, OrderTrackingStatus> CONSTANTS = new HashMap();

  private OrderTrackingStatus(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static OrderTrackingStatus fromValue(String value) {
    OrderTrackingStatus constant = (OrderTrackingStatus)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    OrderTrackingStatus[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      OrderTrackingStatus c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
