//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "text"})
public class Reason {
  @JsonProperty("id")
  private Integer id;
  @JsonProperty("text")
  private String text;

  public Reason() {
  }

  @JsonProperty("id")
  public Integer getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Integer id) {
    this.id = id;
  }

  @JsonProperty("text")
  public String getText() {
    return this.text;
  }

  @JsonProperty("text")
  public void setText(String text) {
    this.text = text;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("text", this.text).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.id).append(this.text).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Reason)) {
      return false;
    } else {
      Reason rhs = (Reason)other;
      return (new EqualsBuilder()).append(this.id, rhs.id).append(this.text, rhs.text).isEquals();
    }
  }
}
