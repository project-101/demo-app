//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"paymentList", "totalPrepaid", "totalCOD", "totalRefunded", "refund"})
public class Payments {
  @JsonProperty("paymentList")
  @Valid
  private List<Payment> paymentList = new ArrayList();
  @JsonProperty("totalPrepaid")
  private BigDecimal totalPrepaid;
  @JsonProperty("totalCOD")
  private BigDecimal totalCOD;
  @JsonProperty("totalRefunded")
  private BigDecimal totalRefunded;
  @JsonProperty("refund")
  @Valid
  private Refund refund;

  public Payments() {
  }

  @JsonProperty("paymentList")
  public List<Payment> getPaymentList() {
    return this.paymentList;
  }

  @JsonProperty("paymentList")
  public void setPaymentList(List<Payment> paymentList) {
    this.paymentList = paymentList;
  }

  @JsonProperty("totalPrepaid")
  public BigDecimal getTotalPrepaid() {
    return this.totalPrepaid;
  }

  @JsonProperty("totalPrepaid")
  public void setTotalPrepaid(BigDecimal totalPrepaid) {
    this.totalPrepaid = totalPrepaid;
  }

  @JsonProperty("totalCOD")
  public BigDecimal getTotalCOD() {
    return this.totalCOD;
  }

  @JsonProperty("totalCOD")
  public void setTotalCOD(BigDecimal totalCOD) {
    this.totalCOD = totalCOD;
  }

  @JsonProperty("totalRefunded")
  public BigDecimal getTotalRefunded() {
    return this.totalRefunded;
  }

  @JsonProperty("totalRefunded")
  public void setTotalRefunded(BigDecimal totalRefunded) {
    this.totalRefunded = totalRefunded;
  }

  @JsonProperty("refund")
  public Refund getRefund() {
    return this.refund;
  }

  @JsonProperty("refund")
  public void setRefund(Refund refund) {
    this.refund = refund;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("paymentList", this.paymentList).append("totalPrepaid", this.totalPrepaid).append("totalCOD", this.totalCOD).append("totalRefunded", this.totalRefunded).append("refund", this.refund).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.totalPrepaid).append(this.totalRefunded).append(this.totalCOD).append(this.paymentList).append(this.refund).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Payments)) {
      return false;
    } else {
      Payments rhs = (Payments)other;
      return (new EqualsBuilder()).append(this.totalPrepaid, rhs.totalPrepaid).append(this.totalRefunded, rhs.totalRefunded).append(this.totalCOD, rhs.totalCOD).append(this.paymentList, rhs.paymentList).append(this.refund, rhs.refund).isEquals();
    }
  }
}
