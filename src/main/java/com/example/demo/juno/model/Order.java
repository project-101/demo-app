//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "legacyEntityId", "parentId", "exchangeOrderId", "cartId", "isFutureOrder", "exchangeCartId", "storeId", "customerId", "customerEmail", "customerWalletId", "isNonFranchise", "isAutoDiscountEnabled", "type", "childOrderIds", "quantity", "itemCount", "status", "statusHistory", "items", "appliedRules", "amount", "payments", "billingAddress", "shippingAddress", "otpState", "guest", "giftMessage", "offer3OrFree", "customerNote", "createdAt", "updatedAt", "deliveryDate", "dispatchDate", "trackingUrl", "trackingDetails", "deliveryOption", "version", "isLoyalty", "loyaltyEndDate", "successMessage", "mall", "storeType", "facilityCode", "isDualCompanyEnabled", "isBulkOrder", "shipToStoreRequired", "isLocalFittingRequired", "localFittingFacility", "isPaymentCaptured", "transactionId", "merchantId", "isWhatsappConsented", "gatewayWalletCampaign", "metadata"})
public class Order {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("legacyEntityId")
  private Long legacyEntityId;
  @JsonProperty("parentId")
  private Long parentId;
  @JsonProperty("exchangeOrderId")
  private Long exchangeOrderId;
  @JsonProperty("cartId")
  private Long cartId;
  @JsonProperty("isFutureOrder")
  private Boolean isFutureOrder;
  @JsonProperty("exchangeCartId")
  private Long exchangeCartId;
  @JsonProperty("storeId")
  private Long storeId;
  @JsonProperty("customerId")
  private Long customerId;
  @JsonProperty("customerEmail")
  private String customerEmail;
  @JsonProperty("customerWalletId")
  private String customerWalletId;
  @JsonProperty("isNonFranchise")
  private Boolean isNonFranchise;
  @JsonProperty("isAutoDiscountEnabled")
  private Boolean isAutoDiscountEnabled;
  @JsonProperty("type")
  private OrderType type;
  @JsonProperty("childOrderIds")
  @Valid
  private List<Long> childOrderIds = new ArrayList();
  @JsonProperty("quantity")
  private Integer quantity;
  @JsonProperty("itemCount")
  private Integer itemCount;
  @JsonProperty("status")
  @Valid
  private OrderStatus status;
  @JsonProperty("statusHistory")
  @Valid
  private List<OrderStatus> statusHistory = new ArrayList();
  @JsonProperty("items")
  @Valid
  private List<Item> items = new ArrayList();
  @JsonProperty("appliedRules")
  @Valid
  private List<Integer> appliedRules = new ArrayList();
  @JsonProperty("amount")
  @Valid
  private TotalAmount amount;
  @JsonProperty("payments")
  @Valid
  private Payments payments;
  @JsonProperty("billingAddress")
  @Valid
  private Address billingAddress;
  @JsonProperty("shippingAddress")
  @Valid
  private Address shippingAddress;
  @JsonProperty("otpState")
  private OtpState otpState;
  @JsonProperty("guest")
  private Boolean guest;
  @JsonProperty("giftMessage")
  @Valid
  private GiftMessage giftMessage;
  @JsonProperty("offer3OrFree")
  private String offer3OrFree;
  @JsonProperty("customerNote")
  private String customerNote;
  @JsonProperty("createdAt")
  private Long createdAt;
  @JsonProperty("updatedAt")
  private Long updatedAt;
  @JsonProperty("deliveryDate")
  private Long deliveryDate;
  @JsonProperty("dispatchDate")
  private Long dispatchDate;
  @JsonProperty("trackingUrl")
  private String trackingUrl;
  @JsonProperty("trackingDetails")
  @Valid
  private List<TrackingDetail> trackingDetails = new ArrayList();
  @JsonProperty("deliveryOption")
  private String deliveryOption;
  @JsonProperty("version")
  private Integer version;
  @JsonProperty("isLoyalty")
  private Boolean isLoyalty;
  @JsonProperty("loyaltyEndDate")
  private Long loyaltyEndDate;
  @JsonProperty("successMessage")
  private String successMessage;
  @JsonProperty("mall")
  private String mall;
  @JsonProperty("storeType")
  private StoreTypeEnum storeType;
  @JsonProperty("facilityCode")
  private String facilityCode;
  @JsonProperty("isDualCompanyEnabled")
  private Boolean isDualCompanyEnabled;
  @JsonProperty("isBulkOrder")
  private Boolean isBulkOrder;
  @JsonProperty("shipToStoreRequired")
  private Boolean shipToStoreRequired;
  @JsonProperty("isLocalFittingRequired")
  private Boolean isLocalFittingRequired;
  @JsonProperty("localFittingFacility")
  private String localFittingFacility;
  @JsonProperty("isPaymentCaptured")
  private Boolean isPaymentCaptured;
  @JsonProperty("transactionId")
  private String transactionId;
  @JsonProperty("merchantId")
  private MerchantIdEnum merchantId;
  @JsonProperty("isWhatsappConsented")
  private Boolean isWhatsappConsented;
  @JsonProperty("gatewayWalletCampaign")
  @Valid
  private Map<String, String> gatewayWalletCampaign;
  @JsonProperty("metadata")
  @Valid
  private Metadata metadata;

  public Order() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("legacyEntityId")
  public Long getLegacyEntityId() {
    return this.legacyEntityId;
  }

  @JsonProperty("legacyEntityId")
  public void setLegacyEntityId(Long legacyEntityId) {
    this.legacyEntityId = legacyEntityId;
  }

  @JsonProperty("parentId")
  public Long getParentId() {
    return this.parentId;
  }

  @JsonProperty("parentId")
  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  @JsonProperty("exchangeOrderId")
  public Long getExchangeOrderId() {
    return this.exchangeOrderId;
  }

  @JsonProperty("exchangeOrderId")
  public void setExchangeOrderId(Long exchangeOrderId) {
    this.exchangeOrderId = exchangeOrderId;
  }

  @JsonProperty("cartId")
  public Long getCartId() {
    return this.cartId;
  }

  @JsonProperty("cartId")
  public void setCartId(Long cartId) {
    this.cartId = cartId;
  }

  @JsonProperty("isFutureOrder")
  public Boolean getIsFutureOrder() {
    return this.isFutureOrder;
  }

  @JsonProperty("isFutureOrder")
  public void setIsFutureOrder(Boolean isFutureOrder) {
    this.isFutureOrder = isFutureOrder;
  }

  @JsonProperty("exchangeCartId")
  public Long getExchangeCartId() {
    return this.exchangeCartId;
  }

  @JsonProperty("exchangeCartId")
  public void setExchangeCartId(Long exchangeCartId) {
    this.exchangeCartId = exchangeCartId;
  }

  @JsonProperty("storeId")
  public Long getStoreId() {
    return this.storeId;
  }

  @JsonProperty("storeId")
  public void setStoreId(Long storeId) {
    this.storeId = storeId;
  }

  @JsonProperty("customerId")
  public Long getCustomerId() {
    return this.customerId;
  }

  @JsonProperty("customerId")
  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  @JsonProperty("customerEmail")
  public String getCustomerEmail() {
    return this.customerEmail;
  }

  @JsonProperty("customerEmail")
  public void setCustomerEmail(String customerEmail) {
    this.customerEmail = customerEmail;
  }

  @JsonProperty("customerWalletId")
  public String getCustomerWalletId() {
    return this.customerWalletId;
  }

  @JsonProperty("customerWalletId")
  public void setCustomerWalletId(String customerWalletId) {
    this.customerWalletId = customerWalletId;
  }

  @JsonProperty("isNonFranchise")
  public Boolean getIsNonFranchise() {
    return this.isNonFranchise;
  }

  @JsonProperty("isNonFranchise")
  public void setIsNonFranchise(Boolean isNonFranchise) {
    this.isNonFranchise = isNonFranchise;
  }

  @JsonProperty("isAutoDiscountEnabled")
  public Boolean getIsAutoDiscountEnabled() {
    return this.isAutoDiscountEnabled;
  }

  @JsonProperty("isAutoDiscountEnabled")
  public void setIsAutoDiscountEnabled(Boolean isAutoDiscountEnabled) {
    this.isAutoDiscountEnabled = isAutoDiscountEnabled;
  }

  @JsonProperty("type")
  public OrderType getType() {
    return this.type;
  }

  @JsonProperty("type")
  public void setType(OrderType type) {
    this.type = type;
  }

  @JsonProperty("childOrderIds")
  public List<Long> getChildOrderIds() {
    return this.childOrderIds;
  }

  @JsonProperty("childOrderIds")
  public void setChildOrderIds(List<Long> childOrderIds) {
    this.childOrderIds = childOrderIds;
  }

  @JsonProperty("quantity")
  public Integer getQuantity() {
    return this.quantity;
  }

  @JsonProperty("quantity")
  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  @JsonProperty("itemCount")
  public Integer getItemCount() {
    return this.itemCount;
  }

  @JsonProperty("itemCount")
  public void setItemCount(Integer itemCount) {
    this.itemCount = itemCount;
  }

  @JsonProperty("status")
  public OrderStatus getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  @JsonProperty("statusHistory")
  public List<OrderStatus> getStatusHistory() {
    return this.statusHistory;
  }

  @JsonProperty("statusHistory")
  public void setStatusHistory(List<OrderStatus> statusHistory) {
    this.statusHistory = statusHistory;
  }

  @JsonProperty("items")
  public List<Item> getItems() {
    return this.items;
  }

  @JsonProperty("items")
  public void setItems(List<Item> items) {
    this.items = items;
  }

  @JsonProperty("appliedRules")
  public List<Integer> getAppliedRules() {
    return this.appliedRules;
  }

  @JsonProperty("appliedRules")
  public void setAppliedRules(List<Integer> appliedRules) {
    this.appliedRules = appliedRules;
  }

  @JsonProperty("amount")
  public TotalAmount getAmount() {
    return this.amount;
  }

  @JsonProperty("amount")
  public void setAmount(TotalAmount amount) {
    this.amount = amount;
  }

  @JsonProperty("payments")
  public Payments getPayments() {
    return this.payments;
  }

  @JsonProperty("payments")
  public void setPayments(Payments payments) {
    this.payments = payments;
  }

  @JsonProperty("billingAddress")
  public Address getBillingAddress() {
    return this.billingAddress;
  }

  @JsonProperty("billingAddress")
  public void setBillingAddress(Address billingAddress) {
    this.billingAddress = billingAddress;
  }

  @JsonProperty("shippingAddress")
  public Address getShippingAddress() {
    return this.shippingAddress;
  }

  @JsonProperty("shippingAddress")
  public void setShippingAddress(Address shippingAddress) {
    this.shippingAddress = shippingAddress;
  }

  @JsonProperty("otpState")
  public OtpState getOtpState() {
    return this.otpState;
  }

  @JsonProperty("otpState")
  public void setOtpState(OtpState otpState) {
    this.otpState = otpState;
  }

  @JsonProperty("guest")
  public Boolean getGuest() {
    return this.guest;
  }

  @JsonProperty("guest")
  public void setGuest(Boolean guest) {
    this.guest = guest;
  }

  @JsonProperty("giftMessage")
  public GiftMessage getGiftMessage() {
    return this.giftMessage;
  }

  @JsonProperty("giftMessage")
  public void setGiftMessage(GiftMessage giftMessage) {
    this.giftMessage = giftMessage;
  }

  @JsonProperty("offer3OrFree")
  public String getOffer3OrFree() {
    return this.offer3OrFree;
  }

  @JsonProperty("offer3OrFree")
  public void setOffer3OrFree(String offer3OrFree) {
    this.offer3OrFree = offer3OrFree;
  }

  @JsonProperty("customerNote")
  public String getCustomerNote() {
    return this.customerNote;
  }

  @JsonProperty("customerNote")
  public void setCustomerNote(String customerNote) {
    this.customerNote = customerNote;
  }

  @JsonProperty("createdAt")
  public Long getCreatedAt() {
    return this.createdAt;
  }

  @JsonProperty("createdAt")
  public void setCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
  }

  @JsonProperty("updatedAt")
  public Long getUpdatedAt() {
    return this.updatedAt;
  }

  @JsonProperty("updatedAt")
  public void setUpdatedAt(Long updatedAt) {
    this.updatedAt = updatedAt;
  }

  @JsonProperty("deliveryDate")
  public Long getDeliveryDate() {
    return this.deliveryDate;
  }

  @JsonProperty("deliveryDate")
  public void setDeliveryDate(Long deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  @JsonProperty("dispatchDate")
  public Long getDispatchDate() {
    return this.dispatchDate;
  }

  @JsonProperty("dispatchDate")
  public void setDispatchDate(Long dispatchDate) {
    this.dispatchDate = dispatchDate;
  }

  @JsonProperty("trackingUrl")
  public String getTrackingUrl() {
    return this.trackingUrl;
  }

  @JsonProperty("trackingUrl")
  public void setTrackingUrl(String trackingUrl) {
    this.trackingUrl = trackingUrl;
  }

  @JsonProperty("trackingDetails")
  public List<TrackingDetail> getTrackingDetails() {
    return this.trackingDetails;
  }

  @JsonProperty("trackingDetails")
  public void setTrackingDetails(List<TrackingDetail> trackingDetails) {
    this.trackingDetails = trackingDetails;
  }

  @JsonProperty("deliveryOption")
  public String getDeliveryOption() {
    return this.deliveryOption;
  }

  @JsonProperty("deliveryOption")
  public void setDeliveryOption(String deliveryOption) {
    this.deliveryOption = deliveryOption;
  }

  @JsonProperty("version")
  public Integer getVersion() {
    return this.version;
  }

  @JsonProperty("version")
  public void setVersion(Integer version) {
    this.version = version;
  }

  @JsonProperty("isLoyalty")
  public Boolean getIsLoyalty() {
    return this.isLoyalty;
  }

  @JsonProperty("isLoyalty")
  public void setIsLoyalty(Boolean isLoyalty) {
    this.isLoyalty = isLoyalty;
  }

  @JsonProperty("loyaltyEndDate")
  public Long getLoyaltyEndDate() {
    return this.loyaltyEndDate;
  }

  @JsonProperty("loyaltyEndDate")
  public void setLoyaltyEndDate(Long loyaltyEndDate) {
    this.loyaltyEndDate = loyaltyEndDate;
  }

  @JsonProperty("successMessage")
  public String getSuccessMessage() {
    return this.successMessage;
  }

  @JsonProperty("successMessage")
  public void setSuccessMessage(String successMessage) {
    this.successMessage = successMessage;
  }

  @JsonProperty("mall")
  public String getMall() {
    return this.mall;
  }

  @JsonProperty("mall")
  public void setMall(String mall) {
    this.mall = mall;
  }

  @JsonProperty("storeType")
  public StoreTypeEnum getStoreType() {
    return this.storeType;
  }

  @JsonProperty("storeType")
  public void setStoreType(StoreTypeEnum storeType) {
    this.storeType = storeType;
  }

  @JsonProperty("facilityCode")
  public String getFacilityCode() {
    return this.facilityCode;
  }

  @JsonProperty("facilityCode")
  public void setFacilityCode(String facilityCode) {
    this.facilityCode = facilityCode;
  }

  @JsonProperty("isDualCompanyEnabled")
  public Boolean getIsDualCompanyEnabled() {
    return this.isDualCompanyEnabled;
  }

  @JsonProperty("isDualCompanyEnabled")
  public void setIsDualCompanyEnabled(Boolean isDualCompanyEnabled) {
    this.isDualCompanyEnabled = isDualCompanyEnabled;
  }

  @JsonProperty("isBulkOrder")
  public Boolean getIsBulkOrder() {
    return this.isBulkOrder;
  }

  @JsonProperty("isBulkOrder")
  public void setIsBulkOrder(Boolean isBulkOrder) {
    this.isBulkOrder = isBulkOrder;
  }

  @JsonProperty("shipToStoreRequired")
  public Boolean getShipToStoreRequired() {
    return this.shipToStoreRequired;
  }

  @JsonProperty("shipToStoreRequired")
  public void setShipToStoreRequired(Boolean shipToStoreRequired) {
    this.shipToStoreRequired = shipToStoreRequired;
  }

  @JsonProperty("isLocalFittingRequired")
  public Boolean getIsLocalFittingRequired() {
    return this.isLocalFittingRequired;
  }

  @JsonProperty("isLocalFittingRequired")
  public void setIsLocalFittingRequired(Boolean isLocalFittingRequired) {
    this.isLocalFittingRequired = isLocalFittingRequired;
  }

  @JsonProperty("localFittingFacility")
  public String getLocalFittingFacility() {
    return this.localFittingFacility;
  }

  @JsonProperty("localFittingFacility")
  public void setLocalFittingFacility(String localFittingFacility) {
    this.localFittingFacility = localFittingFacility;
  }

  @JsonProperty("isPaymentCaptured")
  public Boolean getIsPaymentCaptured() {
    return this.isPaymentCaptured;
  }

  @JsonProperty("isPaymentCaptured")
  public void setIsPaymentCaptured(Boolean isPaymentCaptured) {
    this.isPaymentCaptured = isPaymentCaptured;
  }

  @JsonProperty("transactionId")
  public String getTransactionId() {
    return this.transactionId;
  }

  @JsonProperty("transactionId")
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  @JsonProperty("merchantId")
  public MerchantIdEnum getMerchantId() {
    return this.merchantId;
  }

  @JsonProperty("merchantId")
  public void setMerchantId(MerchantIdEnum merchantId) {
    this.merchantId = merchantId;
  }

  @JsonProperty("isWhatsappConsented")
  public Boolean getIsWhatsappConsented() {
    return this.isWhatsappConsented;
  }

  @JsonProperty("isWhatsappConsented")
  public void setIsWhatsappConsented(Boolean isWhatsappConsented) {
    this.isWhatsappConsented = isWhatsappConsented;
  }

  @JsonProperty("gatewayWalletCampaign")
  public Map<String, String> getGatewayWalletCampaign() {
    return this.gatewayWalletCampaign;
  }

  @JsonProperty("gatewayWalletCampaign")
  public void setGatewayWalletCampaign(Map<String, String> gatewayWalletCampaign) {
    this.gatewayWalletCampaign = gatewayWalletCampaign;
  }

  @JsonProperty("metadata")
  public Metadata getMetadata() {
    return this.metadata;
  }

  @JsonProperty("metadata")
  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("legacyEntityId", this.legacyEntityId).append("parentId", this.parentId).append("exchangeOrderId", this.exchangeOrderId).append("cartId", this.cartId).append("isFutureOrder", this.isFutureOrder).append("exchangeCartId", this.exchangeCartId).append("storeId", this.storeId).append("customerId", this.customerId).append("customerEmail", this.customerEmail).append("customerWalletId", this.customerWalletId).append("isNonFranchise", this.isNonFranchise).append("isAutoDiscountEnabled", this.isAutoDiscountEnabled).append("type", this.type).append("childOrderIds", this.childOrderIds).append("quantity", this.quantity).append("itemCount", this.itemCount).append("status", this.status).append("statusHistory", this.statusHistory).append("items", this.items).append("appliedRules", this.appliedRules).append("amount", this.amount).append("payments", this.payments).append("billingAddress", this.billingAddress).append("shippingAddress", this.shippingAddress).append("otpState", this.otpState).append("guest", this.guest).append("giftMessage", this.giftMessage).append("offer3OrFree", this.offer3OrFree).append("customerNote", this.customerNote).append("createdAt", this.createdAt).append("updatedAt", this.updatedAt).append("deliveryDate", this.deliveryDate).append("dispatchDate", this.dispatchDate).append("trackingUrl", this.trackingUrl).append("trackingDetails", this.trackingDetails).append("deliveryOption", this.deliveryOption).append("version", this.version).append("isLoyalty", this.isLoyalty).append("loyaltyEndDate", this.loyaltyEndDate).append("successMessage", this.successMessage).append("mall", this.mall).append("storeType", this.storeType).append("facilityCode", this.facilityCode).append("isDualCompanyEnabled", this.isDualCompanyEnabled).append("isBulkOrder", this.isBulkOrder).append("shipToStoreRequired", this.shipToStoreRequired).append("isLocalFittingRequired", this.isLocalFittingRequired).append("localFittingFacility", this.localFittingFacility).append("isPaymentCaptured", this.isPaymentCaptured).append("transactionId", this.transactionId).append("merchantId", this.merchantId).append("isWhatsappConsented", this.isWhatsappConsented).append("gatewayWalletCampaign", this.gatewayWalletCampaign).append("metadata", this.metadata).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.childOrderIds).append(this.metadata).append(this.type).append(this.gatewayWalletCampaign).append(this.createdAt).append(this.merchantId).append(this.customerEmail).append(this.appliedRules).append(this.id).append(this.updatedAt).append(this.facilityCode).append(this.giftMessage).append(this.cartId).append(this.isNonFranchise).append(this.mall).append(this.version).append(this.successMessage).append(this.transactionId).append(this.guest).append(this.items).append(this.deliveryOption).append(this.localFittingFacility).append(this.shipToStoreRequired).append(this.isFutureOrder).append(this.status).append(this.trackingUrl).append(this.dispatchDate).append(this.payments).append(this.customerWalletId).append(this.isDualCompanyEnabled).append(this.exchangeCartId).append(this.isBulkOrder).append(this.loyaltyEndDate).append(this.customerNote).append(this.customerId).append(this.trackingDetails).append(this.deliveryDate).append(this.exchangeOrderId).append(this.isAutoDiscountEnabled).append(this.amount).append(this.storeType).append(this.quantity).append(this.isWhatsappConsented).append(this.offer3OrFree).append(this.storeId).append(this.isLocalFittingRequired).append(this.parentId).append(this.itemCount).append(this.isLoyalty).append(this.statusHistory).append(this.legacyEntityId).append(this.isPaymentCaptured).append(this.shippingAddress).append(this.billingAddress).append(this.otpState).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Order)) {
      return false;
    } else {
      Order rhs = (Order)other;
      return (new EqualsBuilder()).append(this.childOrderIds, rhs.childOrderIds).append(this.metadata, rhs.metadata).append(this.type, rhs.type).append(this.gatewayWalletCampaign, rhs.gatewayWalletCampaign).append(this.createdAt, rhs.createdAt).append(this.merchantId, rhs.merchantId).append(this.customerEmail, rhs.customerEmail).append(this.appliedRules, rhs.appliedRules).append(this.id, rhs.id).append(this.updatedAt, rhs.updatedAt).append(this.facilityCode, rhs.facilityCode).append(this.giftMessage, rhs.giftMessage).append(this.cartId, rhs.cartId).append(this.isNonFranchise, rhs.isNonFranchise).append(this.mall, rhs.mall).append(this.version, rhs.version).append(this.successMessage, rhs.successMessage).append(this.transactionId, rhs.transactionId).append(this.guest, rhs.guest).append(this.items, rhs.items).append(this.deliveryOption, rhs.deliveryOption).append(this.localFittingFacility, rhs.localFittingFacility).append(this.shipToStoreRequired, rhs.shipToStoreRequired).append(this.isFutureOrder, rhs.isFutureOrder).append(this.status, rhs.status).append(this.trackingUrl, rhs.trackingUrl).append(this.dispatchDate, rhs.dispatchDate).append(this.payments, rhs.payments).append(this.customerWalletId, rhs.customerWalletId).append(this.isDualCompanyEnabled, rhs.isDualCompanyEnabled).append(this.exchangeCartId, rhs.exchangeCartId).append(this.isBulkOrder, rhs.isBulkOrder).append(this.loyaltyEndDate, rhs.loyaltyEndDate).append(this.customerNote, rhs.customerNote).append(this.customerId, rhs.customerId).append(this.trackingDetails, rhs.trackingDetails).append(this.deliveryDate, rhs.deliveryDate).append(this.exchangeOrderId, rhs.exchangeOrderId).append(this.isAutoDiscountEnabled, rhs.isAutoDiscountEnabled).append(this.amount, rhs.amount).append(this.storeType, rhs.storeType).append(this.quantity, rhs.quantity).append(this.isWhatsappConsented, rhs.isWhatsappConsented).append(this.offer3OrFree, rhs.offer3OrFree).append(this.storeId, rhs.storeId).append(this.isLocalFittingRequired, rhs.isLocalFittingRequired).append(this.parentId, rhs.parentId).append(this.itemCount, rhs.itemCount).append(this.isLoyalty, rhs.isLoyalty).append(this.statusHistory, rhs.statusHistory).append(this.legacyEntityId, rhs.legacyEntityId).append(this.isPaymentCaptured, rhs.isPaymentCaptured).append(this.shippingAddress, rhs.shippingAddress).append(this.billingAddress, rhs.billingAddress).append(this.otpState, rhs.otpState).isEquals();
    }
  }
}
