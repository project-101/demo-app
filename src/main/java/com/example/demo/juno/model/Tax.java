//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"name", "amount"})
public class Tax {
  @JsonProperty("name")
  private String name;
  @JsonProperty("amount")
  private BigDecimal amount;

  public Tax() {
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return this.amount;
  }

  @JsonProperty("amount")
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("name", this.name).append("amount", this.amount).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.name).append(this.amount).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Tax)) {
      return false;
    } else {
      Tax rhs = (Tax)other;
      return (new EqualsBuilder()).append(this.name, rhs.name).append(this.amount, rhs.amount).isEquals();
    }
  }
}
