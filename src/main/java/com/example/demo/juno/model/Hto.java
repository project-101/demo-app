//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"city", "date", "slotId", "lenskartAtHome"})
public class Hto {
  @JsonProperty("city")
  private String city;
  @JsonProperty("date")
  private Object date;
  @JsonProperty("slotId")
  private Integer slotId;
  @JsonProperty("lenskartAtHome")
  private String lenskartAtHome;

  public Hto() {
  }

  @JsonProperty("city")
  public String getCity() {
    return this.city;
  }

  @JsonProperty("city")
  public void setCity(String city) {
    this.city = city;
  }

  @JsonProperty("date")
  public Object getDate() {
    return this.date;
  }

  @JsonProperty("date")
  public void setDate(Object date) {
    this.date = date;
  }

  @JsonProperty("slotId")
  public Integer getSlotId() {
    return this.slotId;
  }

  @JsonProperty("slotId")
  public void setSlotId(Integer slotId) {
    this.slotId = slotId;
  }

  @JsonProperty("lenskartAtHome")
  public String getLenskartAtHome() {
    return this.lenskartAtHome;
  }

  @JsonProperty("lenskartAtHome")
  public void setLenskartAtHome(String lenskartAtHome) {
    this.lenskartAtHome = lenskartAtHome;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("city", this.city).append("date", this.date).append("slotId", this.slotId).append("lenskartAtHome", this.lenskartAtHome).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.date).append(this.slotId).append(this.city).append(this.lenskartAtHome).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Hto)) {
      return false;
    } else {
      Hto rhs = (Hto)other;
      return (new EqualsBuilder()).append(this.date, rhs.date).append(this.slotId, rhs.slotId).append(this.city, rhs.city).append(this.lenskartAtHome, rhs.lenskartAtHome).isEquals();
    }
  }
}
