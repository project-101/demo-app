//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"currencyCode", "discounts", "totalDiscount", "taxes", "totalTax", "shipping", "subTotal", "total", "paid"})
public class TotalAmount {
  @JsonProperty("currencyCode")
  private String currencyCode;
  @JsonProperty("discounts")
  @Valid
  private List<Discount> discounts = new ArrayList();
  @JsonProperty("totalDiscount")
  private BigDecimal totalDiscount;
  @JsonProperty("taxes")
  @Valid
  private List<Tax> taxes = new ArrayList();
  @JsonProperty("totalTax")
  private BigDecimal totalTax;
  @JsonProperty("shipping")
  private BigDecimal shipping;
  @JsonProperty("subTotal")
  private BigDecimal subTotal;
  @JsonProperty("total")
  private BigDecimal total;
  @JsonProperty("paid")
  private BigDecimal paid;

  public TotalAmount() {
  }

  @JsonProperty("currencyCode")
  public String getCurrencyCode() {
    return this.currencyCode;
  }

  @JsonProperty("currencyCode")
  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  @JsonProperty("discounts")
  public List<Discount> getDiscounts() {
    return this.discounts;
  }

  @JsonProperty("discounts")
  public void setDiscounts(List<Discount> discounts) {
    this.discounts = discounts;
  }

  @JsonProperty("totalDiscount")
  public BigDecimal getTotalDiscount() {
    return this.totalDiscount;
  }

  @JsonProperty("totalDiscount")
  public void setTotalDiscount(BigDecimal totalDiscount) {
    this.totalDiscount = totalDiscount;
  }

  @JsonProperty("taxes")
  public List<Tax> getTaxes() {
    return this.taxes;
  }

  @JsonProperty("taxes")
  public void setTaxes(List<Tax> taxes) {
    this.taxes = taxes;
  }

  @JsonProperty("totalTax")
  public BigDecimal getTotalTax() {
    return this.totalTax;
  }

  @JsonProperty("totalTax")
  public void setTotalTax(BigDecimal totalTax) {
    this.totalTax = totalTax;
  }

  @JsonProperty("shipping")
  public BigDecimal getShipping() {
    return this.shipping;
  }

  @JsonProperty("shipping")
  public void setShipping(BigDecimal shipping) {
    this.shipping = shipping;
  }

  @JsonProperty("subTotal")
  public BigDecimal getSubTotal() {
    return this.subTotal;
  }

  @JsonProperty("subTotal")
  public void setSubTotal(BigDecimal subTotal) {
    this.subTotal = subTotal;
  }

  @JsonProperty("total")
  public BigDecimal getTotal() {
    return this.total;
  }

  @JsonProperty("total")
  public void setTotal(BigDecimal total) {
    this.total = total;
  }

  @JsonProperty("paid")
  public BigDecimal getPaid() {
    return this.paid;
  }

  @JsonProperty("paid")
  public void setPaid(BigDecimal paid) {
    this.paid = paid;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("currencyCode", this.currencyCode).append("discounts", this.discounts).append("totalDiscount", this.totalDiscount).append("taxes", this.taxes).append("totalTax", this.totalTax).append("shipping", this.shipping).append("subTotal", this.subTotal).append("total", this.total).append("paid", this.paid).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.totalTax).append(this.total).append(this.discounts).append(this.shipping).append(this.paid).append(this.totalDiscount).append(this.taxes).append(this.subTotal).append(this.currencyCode).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof TotalAmount)) {
      return false;
    } else {
      TotalAmount rhs = (TotalAmount)other;
      return (new EqualsBuilder()).append(this.totalTax, rhs.totalTax).append(this.total, rhs.total).append(this.discounts, rhs.discounts).append(this.shipping, rhs.shipping).append(this.paid, rhs.paid).append(this.totalDiscount, rhs.totalDiscount).append(this.taxes, rhs.taxes).append(this.subTotal, rhs.subTotal).append(this.currencyCode, rhs.currencyCode).isEquals();
    }
  }
}
