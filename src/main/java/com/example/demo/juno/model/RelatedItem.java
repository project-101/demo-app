//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"relatedItemId", "relation", "isRemovable"})
public class RelatedItem {
  @JsonProperty("relatedItemId")
  private Long relatedItemId;
  @JsonProperty("relation")
  private String relation;
  @JsonProperty("isRemovable")
  private Boolean isRemovable;

  public RelatedItem() {
  }

  @JsonProperty("relatedItemId")
  public Long getRelatedItemId() {
    return this.relatedItemId;
  }

  @JsonProperty("relatedItemId")
  public void setRelatedItemId(Long relatedItemId) {
    this.relatedItemId = relatedItemId;
  }

  @JsonProperty("relation")
  public String getRelation() {
    return this.relation;
  }

  @JsonProperty("relation")
  public void setRelation(String relation) {
    this.relation = relation;
  }

  @JsonProperty("isRemovable")
  public Boolean getIsRemovable() {
    return this.isRemovable;
  }

  @JsonProperty("isRemovable")
  public void setIsRemovable(Boolean isRemovable) {
    this.isRemovable = isRemovable;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("relatedItemId", this.relatedItemId).append("relation", this.relation).append("isRemovable", this.isRemovable).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.relatedItemId).append(this.isRemovable).append(this.relation).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof RelatedItem)) {
      return false;
    } else {
      RelatedItem rhs = (RelatedItem)other;
      return (new EqualsBuilder()).append(this.relatedItemId, rhs.relatedItemId).append(this.isRemovable, rhs.isRemovable).append(this.relation, rhs.relation).isEquals();
    }
  }
}
