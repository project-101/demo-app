//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"time", "trackStatus", "location"})
public class Details {
  @JsonProperty("time")
  private String time;
  @JsonProperty("trackStatus")
  private String trackStatus;
  @JsonProperty("location")
  private String location;

  public Details() {
  }

  @JsonProperty("time")
  public String getTime() {
    return this.time;
  }

  @JsonProperty("time")
  public void setTime(String time) {
    this.time = time;
  }

  @JsonProperty("trackStatus")
  public String getTrackStatus() {
    return this.trackStatus;
  }

  @JsonProperty("trackStatus")
  public void setTrackStatus(String trackStatus) {
    this.trackStatus = trackStatus;
  }

  @JsonProperty("location")
  public String getLocation() {
    return this.location;
  }

  @JsonProperty("location")
  public void setLocation(String location) {
    this.location = location;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("time", this.time).append("trackStatus", this.trackStatus).append("location", this.location).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.trackStatus).append(this.location).append(this.time).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Details)) {
      return false;
    } else {
      Details rhs = (Details)other;
      return (new EqualsBuilder()).append(this.trackStatus, rhs.trackStatus).append(this.location, rhs.location).append(this.time, rhs.time).isEquals();
    }
  }
}
