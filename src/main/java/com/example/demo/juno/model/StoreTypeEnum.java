//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum StoreTypeEnum {
  FOFO_WITH_SC("FOFO_WITH_SC"),
  FOFO_WITHOUT_SC("FOFO_WITHOUT_SC"),
  COCO_LENSKART_STORE("COCO_LENSKART_STORE"),
  HEC("HEC"),
  COCO_JJ_STORE("COCO_JJ_STORE"),
  MARKET_PLACE("MARKET_PLACE"),
  COCO_INTERNATIONAL("COCO_INTERNATIONAL");

  private final String value;
  private static final Map<String, StoreTypeEnum> CONSTANTS = new HashMap();

  private StoreTypeEnum(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static StoreTypeEnum fromValue(String value) {
    StoreTypeEnum constant = (StoreTypeEnum)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    StoreTypeEnum[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      StoreTypeEnum c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
