package com.example.demo.juno.model;

import lombok.Data;

@Data
public class Session {

  private String id;
  private String token;
  private String username;

}
