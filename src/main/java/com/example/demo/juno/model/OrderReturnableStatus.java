//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum OrderReturnableStatus {
  RETURNABLE("RETURNABLE"),
  NON_RETURNABLE("NON_RETURNABLE");

  private final String value;
  private static final Map<String, OrderReturnableStatus> CONSTANTS = new HashMap();

  private OrderReturnableStatus(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static OrderReturnableStatus fromValue(String value) {
    OrderReturnableStatus constant = (OrderReturnableStatus)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    OrderReturnableStatus[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      OrderReturnableStatus c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
