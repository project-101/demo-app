//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"reviewId", "reviewTitle", "reviewDetail", "reviewee", "noOfStars", "reviewDate", "email", "reviewerType", "images", "products"})
public class Reviews {
  @JsonProperty("reviewId")
  private String reviewId;
  @JsonProperty("reviewTitle")
  private String reviewTitle;
  @JsonProperty("reviewDetail")
  private String reviewDetail;
  @JsonProperty("reviewee")
  private String reviewee;
  @JsonProperty("noOfStars")
  private String noOfStars;
  @JsonProperty("reviewDate")
  private String reviewDate;
  @JsonProperty("email")
  private String email;
  @JsonProperty("reviewerType")
  private String reviewerType;
  @JsonProperty("images")
  @Valid
  private List<Image> images = new ArrayList();
  @JsonProperty("products")
  @Valid
  private ProductDetail products;

  public Reviews() {
  }

  @JsonProperty("reviewId")
  public String getReviewId() {
    return this.reviewId;
  }

  @JsonProperty("reviewId")
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  @JsonProperty("reviewTitle")
  public String getReviewTitle() {
    return this.reviewTitle;
  }

  @JsonProperty("reviewTitle")
  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  @JsonProperty("reviewDetail")
  public String getReviewDetail() {
    return this.reviewDetail;
  }

  @JsonProperty("reviewDetail")
  public void setReviewDetail(String reviewDetail) {
    this.reviewDetail = reviewDetail;
  }

  @JsonProperty("reviewee")
  public String getReviewee() {
    return this.reviewee;
  }

  @JsonProperty("reviewee")
  public void setReviewee(String reviewee) {
    this.reviewee = reviewee;
  }

  @JsonProperty("noOfStars")
  public String getNoOfStars() {
    return this.noOfStars;
  }

  @JsonProperty("noOfStars")
  public void setNoOfStars(String noOfStars) {
    this.noOfStars = noOfStars;
  }

  @JsonProperty("reviewDate")
  public String getReviewDate() {
    return this.reviewDate;
  }

  @JsonProperty("reviewDate")
  public void setReviewDate(String reviewDate) {
    this.reviewDate = reviewDate;
  }

  @JsonProperty("email")
  public String getEmail() {
    return this.email;
  }

  @JsonProperty("email")
  public void setEmail(String email) {
    this.email = email;
  }

  @JsonProperty("reviewerType")
  public String getReviewerType() {
    return this.reviewerType;
  }

  @JsonProperty("reviewerType")
  public void setReviewerType(String reviewerType) {
    this.reviewerType = reviewerType;
  }

  @JsonProperty("images")
  public List<Image> getImages() {
    return this.images;
  }

  @JsonProperty("images")
  public void setImages(List<Image> images) {
    this.images = images;
  }

  @JsonProperty("products")
  public ProductDetail getProducts() {
    return this.products;
  }

  @JsonProperty("products")
  public void setProducts(ProductDetail products) {
    this.products = products;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("reviewId", this.reviewId).append("reviewTitle", this.reviewTitle).append("reviewDetail", this.reviewDetail).append("reviewee", this.reviewee).append("noOfStars", this.noOfStars).append("reviewDate", this.reviewDate).append("email", this.email).append("reviewerType", this.reviewerType).append("images", this.images).append("products", this.products).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.images).append(this.reviewDetail).append(this.noOfStars).append(this.reviewDate).append(this.reviewee).append(this.reviewerType).append(this.reviewId).append(this.email).append(this.reviewTitle).append(this.products).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Reviews)) {
      return false;
    } else {
      Reviews rhs = (Reviews)other;
      return (new EqualsBuilder()).append(this.images, rhs.images).append(this.reviewDetail, rhs.reviewDetail).append(this.noOfStars, rhs.noOfStars).append(this.reviewDate, rhs.reviewDate).append(this.reviewee, rhs.reviewee).append(this.reviewerType, rhs.reviewerType).append(this.reviewId, rhs.reviewId).append(this.email, rhs.email).append(this.reviewTitle, rhs.reviewTitle).append(this.products, rhs.products).isEquals();
    }
  }
}
