//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotNull;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "firstName", "lastName", "phone", "phoneCode", "email", "addressType", "addressline1", "addressline2", "city", "floor", "liftAvailable", "landmark", "state", "postcode", "country", "locality", "alternatePhone", "gender", "comment", "hasCod", "created", "updated", "action", "lkCountry", "defaultAddress"})
public class Address {
  @JsonProperty("id")
  private Integer id;
  @JsonProperty("firstName")
  @NotNull
  private String firstName;
  @JsonProperty("lastName")
  @NotNull
  private String lastName;
  @JsonProperty("phone")
  @NotNull
  private String phone;
  @JsonProperty("phoneCode")
  private String phoneCode = "+91";
  @JsonProperty("email")
  private String email;
  @JsonProperty("addressType")
  private String addressType;
  @JsonProperty("addressline1")
  @NotNull
  private String addressline1;
  @JsonProperty("addressline2")
  private String addressline2;
  @JsonProperty("city")
  @NotNull
  private String city;
  @JsonProperty("floor")
  private Long floor;
  @JsonProperty("liftAvailable")
  private Boolean liftAvailable;
  @JsonProperty("landmark")
  private String landmark;
  @JsonProperty("state")
  @NotNull
  private String state;
  @JsonProperty("postcode")
  @NotNull
  private String postcode;
  @JsonProperty("country")
  @NotNull
  private String country;
  @JsonProperty("locality")
  private String locality;
  @JsonProperty("alternatePhone")
  private String alternatePhone;
  @JsonProperty("gender")
  private String gender;
  @JsonProperty("comment")
  private String comment;
  @JsonProperty("hasCod")
  private Boolean hasCod;
  @JsonProperty("created")
  private Long created;
  @JsonProperty("updated")
  private Long updated;
  @JsonProperty("action")
  private AddressActionEnum action;
  @JsonProperty("lkCountry")
  private String lkCountry;
  @JsonProperty("defaultAddress")
  private Boolean defaultAddress;

  public Address() {
  }

  @JsonProperty("id")
  public Integer getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Integer id) {
    this.id = id;
  }

  @JsonProperty("firstName")
  public String getFirstName() {
    return this.firstName;
  }

  @JsonProperty("firstName")
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @JsonProperty("lastName")
  public String getLastName() {
    return this.lastName;
  }

  @JsonProperty("lastName")
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @JsonProperty("phone")
  public String getPhone() {
    return this.phone;
  }

  @JsonProperty("phone")
  public void setPhone(String phone) {
    this.phone = phone;
  }

  @JsonProperty("phoneCode")
  public String getPhoneCode() {
    return this.phoneCode;
  }

  @JsonProperty("phoneCode")
  public void setPhoneCode(String phoneCode) {
    this.phoneCode = phoneCode;
  }

  @JsonProperty("email")
  public String getEmail() {
    return this.email;
  }

  @JsonProperty("email")
  public void setEmail(String email) {
    this.email = email;
  }

  @JsonProperty("addressType")
  public String getAddressType() {
    return this.addressType;
  }

  @JsonProperty("addressType")
  public void setAddressType(String addressType) {
    this.addressType = addressType;
  }

  @JsonProperty("addressline1")
  public String getAddressline1() {
    return this.addressline1;
  }

  @JsonProperty("addressline1")
  public void setAddressline1(String addressline1) {
    this.addressline1 = addressline1;
  }

  @JsonProperty("addressline2")
  public String getAddressline2() {
    return this.addressline2;
  }

  @JsonProperty("addressline2")
  public void setAddressline2(String addressline2) {
    this.addressline2 = addressline2;
  }

  @JsonProperty("city")
  public String getCity() {
    return this.city;
  }

  @JsonProperty("city")
  public void setCity(String city) {
    this.city = city;
  }

  @JsonProperty("floor")
  public Long getFloor() {
    return this.floor;
  }

  @JsonProperty("floor")
  public void setFloor(Long floor) {
    this.floor = floor;
  }

  @JsonProperty("liftAvailable")
  public Boolean getLiftAvailable() {
    return this.liftAvailable;
  }

  @JsonProperty("liftAvailable")
  public void setLiftAvailable(Boolean liftAvailable) {
    this.liftAvailable = liftAvailable;
  }

  @JsonProperty("landmark")
  public String getLandmark() {
    return this.landmark;
  }

  @JsonProperty("landmark")
  public void setLandmark(String landmark) {
    this.landmark = landmark;
  }

  @JsonProperty("state")
  public String getState() {
    return this.state;
  }

  @JsonProperty("state")
  public void setState(String state) {
    this.state = state;
  }

  @JsonProperty("postcode")
  public String getPostcode() {
    return this.postcode;
  }

  @JsonProperty("postcode")
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }

  @JsonProperty("country")
  public String getCountry() {
    return this.country;
  }

  @JsonProperty("country")
  public void setCountry(String country) {
    this.country = country;
  }

  @JsonProperty("locality")
  public String getLocality() {
    return this.locality;
  }

  @JsonProperty("locality")
  public void setLocality(String locality) {
    this.locality = locality;
  }

  @JsonProperty("alternatePhone")
  public String getAlternatePhone() {
    return this.alternatePhone;
  }

  @JsonProperty("alternatePhone")
  public void setAlternatePhone(String alternatePhone) {
    this.alternatePhone = alternatePhone;
  }

  @JsonProperty("gender")
  public String getGender() {
    return this.gender;
  }

  @JsonProperty("gender")
  public void setGender(String gender) {
    this.gender = gender;
  }

  @JsonProperty("comment")
  public String getComment() {
    return this.comment;
  }

  @JsonProperty("comment")
  public void setComment(String comment) {
    this.comment = comment;
  }

  @JsonProperty("hasCod")
  public Boolean getHasCod() {
    return this.hasCod;
  }

  @JsonProperty("hasCod")
  public void setHasCod(Boolean hasCod) {
    this.hasCod = hasCod;
  }

  @JsonProperty("created")
  public Long getCreated() {
    return this.created;
  }

  @JsonProperty("created")
  public void setCreated(Long created) {
    this.created = created;
  }

  @JsonProperty("updated")
  public Long getUpdated() {
    return this.updated;
  }

  @JsonProperty("updated")
  public void setUpdated(Long updated) {
    this.updated = updated;
  }

  @JsonProperty("action")
  public AddressActionEnum getAction() {
    return this.action;
  }

  @JsonProperty("action")
  public void setAction(AddressActionEnum action) {
    this.action = action;
  }

  @JsonProperty("lkCountry")
  public String getLkCountry() {
    return this.lkCountry;
  }

  @JsonProperty("lkCountry")
  public void setLkCountry(String lkCountry) {
    this.lkCountry = lkCountry;
  }

  @JsonProperty("defaultAddress")
  public Boolean getDefaultAddress() {
    return this.defaultAddress;
  }

  @JsonProperty("defaultAddress")
  public void setDefaultAddress(Boolean defaultAddress) {
    this.defaultAddress = defaultAddress;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("firstName", this.firstName).append("lastName", this.lastName).append("phone", this.phone).append("phoneCode", this.phoneCode).append("email", this.email).append("addressType", this.addressType).append("addressline1", this.addressline1).append("addressline2", this.addressline2).append("city", this.city).append("floor", this.floor).append("liftAvailable", this.liftAvailable).append("landmark", this.landmark).append("state", this.state).append("postcode", this.postcode).append("country", this.country).append("locality", this.locality).append("alternatePhone", this.alternatePhone).append("gender", this.gender).append("comment", this.comment).append("hasCod", this.hasCod).append("created", this.created).append("updated", this.updated).append("action", this.action).append("lkCountry", this.lkCountry).append("defaultAddress", this.defaultAddress).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.lastName).append(this.country).append(this.liftAvailable).append(this.gender).append(this.city).append(this.phoneCode).append(this.action).append(this.id).append(this.state).append(this.lkCountry).append(this.floor).append(this.landmark).append(this.email).append(this.addressType).append(this.created).append(this.postcode).append(this.locality).append(this.alternatePhone).append(this.firstName).append(this.phone).append(this.addressline2).append(this.addressline1).append(this.comment).append(this.hasCod).append(this.updated).append(this.defaultAddress).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Address)) {
      return false;
    } else {
      Address rhs = (Address)other;
      return (new EqualsBuilder()).append(this.lastName, rhs.lastName).append(this.country, rhs.country).append(this.liftAvailable, rhs.liftAvailable).append(this.gender, rhs.gender).append(this.city, rhs.city).append(this.phoneCode, rhs.phoneCode).append(this.action, rhs.action).append(this.id, rhs.id).append(this.state, rhs.state).append(this.lkCountry, rhs.lkCountry).append(this.floor, rhs.floor).append(this.landmark, rhs.landmark).append(this.email, rhs.email).append(this.addressType, rhs.addressType).append(this.created, rhs.created).append(this.postcode, rhs.postcode).append(this.locality, rhs.locality).append(this.alternatePhone, rhs.alternatePhone).append(this.firstName, rhs.firstName).append(this.phone, rhs.phone).append(this.addressline2, rhs.addressline2).append(this.addressline1, rhs.addressline1).append(this.comment, rhs.comment).append(this.hasCod, rhs.hasCod).append(this.updated, rhs.updated).append(this.defaultAddress, rhs.defaultAddress).isEquals();
    }
  }
}
