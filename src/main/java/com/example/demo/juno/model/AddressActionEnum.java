//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum AddressActionEnum {
  UPDATE("UPDATE"),
  INSERT("INSERT"),
  NOT_INSERT("NOT_INSERT");

  private final String value;
  private static final Map<String, AddressActionEnum> CONSTANTS = new HashMap();

  private AddressActionEnum(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static AddressActionEnum fromValue(String value) {
    AddressActionEnum constant = (AddressActionEnum)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    AddressActionEnum[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      AddressActionEnum c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
