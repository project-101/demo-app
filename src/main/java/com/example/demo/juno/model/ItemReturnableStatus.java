//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ItemReturnableStatus {
  RETURNABLE("RETURNABLE"),
  NON_RETURNABLE("NON_RETURNABLE"),
  RETURN_IN_PROCESS("RETURN_IN_PROCESS"),
  RETURN_FAILED("RETURN_FAILED"),
  RETURN_REQUESTED("RETURN_REQUESTED"),
  RETURN_PICKUP_INITIATED("RETURN_PICKUP_INITIATED"),
  RETURN_PICKUP_COMPLETED("RETURN_PICKUP_COMPLETED"),
  RETURN_IN_TRANSIT("RETURN_IN_TRANSIT"),
  RETURN_RECEIVED("RETURN_RECEIVED"),
  RETURN_QC_COMPLETE("RETURN_QC_COMPLETE"),
  RETURN_REFUND_INITIATED("RETURN_REFUND_INITIATED"),
  RETURN_COMPLETE("RETURN_COMPLETE"),
  RETURN_ITEM_NOT_RECEIVED("RETURN_ITEM_NOT_RECEIVED"),
  RETURN_TO_ORIGIN("RETURN_TO_ORIGIN");

  private final String value;
  private static final Map<String, ItemReturnableStatus> CONSTANTS = new HashMap();

  private ItemReturnableStatus(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static ItemReturnableStatus fromValue(String value) {
    ItemReturnableStatus constant = (ItemReturnableStatus)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    ItemReturnableStatus[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      ItemReturnableStatus c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
