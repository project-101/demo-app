//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ProductDeliveryTypeEnum {
  B_2_B("B2B"),
  DTC("DTC"),
  OTC("OTC");

  private final String value;
  private static final Map<String, ProductDeliveryTypeEnum> CONSTANTS = new HashMap();

  private ProductDeliveryTypeEnum(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static ProductDeliveryTypeEnum fromValue(String value) {
    ProductDeliveryTypeEnum constant = (ProductDeliveryTypeEnum)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    ProductDeliveryTypeEnum[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      ProductDeliveryTypeEnum c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
