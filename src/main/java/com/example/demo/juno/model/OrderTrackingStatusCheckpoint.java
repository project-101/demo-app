//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum OrderTrackingStatusCheckpoint {
  ORDER_PLACED("ORDER_PLACED"),
  IN_HOUSE_PROCESSING("IN_HOUSE_PROCESSING"),
  DISPATCHED("DISPATCHED"),
  DELIVERED("DELIVERED");

  private final String value;
  private static final Map<String, OrderTrackingStatusCheckpoint> CONSTANTS = new HashMap();

  private OrderTrackingStatusCheckpoint(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static OrderTrackingStatusCheckpoint fromValue(String value) {
    OrderTrackingStatusCheckpoint constant = (OrderTrackingStatusCheckpoint)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    OrderTrackingStatusCheckpoint[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      OrderTrackingStatusCheckpoint c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
