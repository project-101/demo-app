//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"sph", "cyl", "axis", "ap", "pd", "nearPD", "lensHeight", "lensWidth", "fittingHeight", "ed", "dbl", "side", "boxes", "baseCurve", "bottomDistance", "topDistance", "color", "tint", "effectiveDia", "svpd", "thickness"})
public class Power {
  @JsonProperty("sph")
  private String sph;
  @JsonProperty("cyl")
  private String cyl;
  @JsonProperty("axis")
  private String axis;
  @JsonProperty("ap")
  private String ap;
  @JsonProperty("pd")
  private String pd;
  @JsonProperty("nearPD")
  private String nearPD;
  @JsonProperty("lensHeight")
  private String lensHeight;
  @JsonProperty("lensWidth")
  private String lensWidth;
  @JsonProperty("fittingHeight")
  private String fittingHeight;
  @JsonProperty("ed")
  private String ed;
  @JsonProperty("dbl")
  private String dbl;
  @JsonProperty("side")
  private String side;
  @JsonProperty("boxes")
  private Integer boxes;
  @JsonProperty("baseCurve")
  private String baseCurve;
  @JsonProperty("bottomDistance")
  private String bottomDistance;
  @JsonProperty("topDistance")
  private String topDistance;
  @JsonProperty("color")
  private String color;
  @JsonProperty("tint")
  private String tint;
  @JsonProperty("effectiveDia")
  private String effectiveDia;
  @JsonProperty("svpd")
  private String svpd;
  @JsonProperty("thickness")
  private String thickness;

  public Power() {
  }

  @JsonProperty("sph")
  public String getSph() {
    return this.sph;
  }

  @JsonProperty("sph")
  public void setSph(String sph) {
    this.sph = sph;
  }

  @JsonProperty("cyl")
  public String getCyl() {
    return this.cyl;
  }

  @JsonProperty("cyl")
  public void setCyl(String cyl) {
    this.cyl = cyl;
  }

  @JsonProperty("axis")
  public String getAxis() {
    return this.axis;
  }

  @JsonProperty("axis")
  public void setAxis(String axis) {
    this.axis = axis;
  }

  @JsonProperty("ap")
  public String getAp() {
    return this.ap;
  }

  @JsonProperty("ap")
  public void setAp(String ap) {
    this.ap = ap;
  }

  @JsonProperty("pd")
  public String getPd() {
    return this.pd;
  }

  @JsonProperty("pd")
  public void setPd(String pd) {
    this.pd = pd;
  }

  @JsonProperty("nearPD")
  public String getNearPD() {
    return this.nearPD;
  }

  @JsonProperty("nearPD")
  public void setNearPD(String nearPD) {
    this.nearPD = nearPD;
  }

  @JsonProperty("lensHeight")
  public String getLensHeight() {
    return this.lensHeight;
  }

  @JsonProperty("lensHeight")
  public void setLensHeight(String lensHeight) {
    this.lensHeight = lensHeight;
  }

  @JsonProperty("lensWidth")
  public String getLensWidth() {
    return this.lensWidth;
  }

  @JsonProperty("lensWidth")
  public void setLensWidth(String lensWidth) {
    this.lensWidth = lensWidth;
  }

  @JsonProperty("fittingHeight")
  public String getFittingHeight() {
    return this.fittingHeight;
  }

  @JsonProperty("fittingHeight")
  public void setFittingHeight(String fittingHeight) {
    this.fittingHeight = fittingHeight;
  }

  @JsonProperty("ed")
  public String getEd() {
    return this.ed;
  }

  @JsonProperty("ed")
  public void setEd(String ed) {
    this.ed = ed;
  }

  @JsonProperty("dbl")
  public String getDbl() {
    return this.dbl;
  }

  @JsonProperty("dbl")
  public void setDbl(String dbl) {
    this.dbl = dbl;
  }

  @JsonProperty("side")
  public String getSide() {
    return this.side;
  }

  @JsonProperty("side")
  public void setSide(String side) {
    this.side = side;
  }

  @JsonProperty("boxes")
  public Integer getBoxes() {
    return this.boxes;
  }

  @JsonProperty("boxes")
  public void setBoxes(Integer boxes) {
    this.boxes = boxes;
  }

  @JsonProperty("baseCurve")
  public String getBaseCurve() {
    return this.baseCurve;
  }

  @JsonProperty("baseCurve")
  public void setBaseCurve(String baseCurve) {
    this.baseCurve = baseCurve;
  }

  @JsonProperty("bottomDistance")
  public String getBottomDistance() {
    return this.bottomDistance;
  }

  @JsonProperty("bottomDistance")
  public void setBottomDistance(String bottomDistance) {
    this.bottomDistance = bottomDistance;
  }

  @JsonProperty("topDistance")
  public String getTopDistance() {
    return this.topDistance;
  }

  @JsonProperty("topDistance")
  public void setTopDistance(String topDistance) {
    this.topDistance = topDistance;
  }

  @JsonProperty("color")
  public String getColor() {
    return this.color;
  }

  @JsonProperty("color")
  public void setColor(String color) {
    this.color = color;
  }

  @JsonProperty("tint")
  public String getTint() {
    return this.tint;
  }

  @JsonProperty("tint")
  public void setTint(String tint) {
    this.tint = tint;
  }

  @JsonProperty("effectiveDia")
  public String getEffectiveDia() {
    return this.effectiveDia;
  }

  @JsonProperty("effectiveDia")
  public void setEffectiveDia(String effectiveDia) {
    this.effectiveDia = effectiveDia;
  }

  @JsonProperty("svpd")
  public String getSvpd() {
    return this.svpd;
  }

  @JsonProperty("svpd")
  public void setSvpd(String svpd) {
    this.svpd = svpd;
  }

  @JsonProperty("thickness")
  public String getThickness() {
    return this.thickness;
  }

  @JsonProperty("thickness")
  public void setThickness(String thickness) {
    this.thickness = thickness;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("sph", this.sph).append("cyl", this.cyl).append("axis", this.axis).append("ap", this.ap).append("pd", this.pd).append("nearPD", this.nearPD).append("lensHeight", this.lensHeight).append("lensWidth", this.lensWidth).append("fittingHeight", this.fittingHeight).append("ed", this.ed).append("dbl", this.dbl).append("side", this.side).append("boxes", this.boxes).append("baseCurve", this.baseCurve).append("bottomDistance", this.bottomDistance).append("topDistance", this.topDistance).append("color", this.color).append("tint", this.tint).append("effectiveDia", this.effectiveDia).append("svpd", this.svpd).append("thickness", this.thickness).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.boxes).append(this.svpd).append(this.side).append(this.lensHeight).append(this.color).append(this.thickness).append(this.sph).append(this.axis).append(this.fittingHeight).append(this.tint).append(this.nearPD).append(this.ap).append(this.dbl).append(this.baseCurve).append(this.effectiveDia).append(this.lensWidth).append(this.pd).append(this.cyl).append(this.bottomDistance).append(this.ed).append(this.topDistance).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Power)) {
      return false;
    } else {
      Power rhs = (Power)other;
      return (new EqualsBuilder()).append(this.boxes, rhs.boxes).append(this.svpd, rhs.svpd).append(this.side, rhs.side).append(this.lensHeight, rhs.lensHeight).append(this.color, rhs.color).append(this.thickness, rhs.thickness).append(this.sph, rhs.sph).append(this.axis, rhs.axis).append(this.fittingHeight, rhs.fittingHeight).append(this.tint, rhs.tint).append(this.nearPD, rhs.nearPD).append(this.ap, rhs.ap).append(this.dbl, rhs.dbl).append(this.baseCurve, rhs.baseCurve).append(this.effectiveDia, rhs.effectiveDia).append(this.lensWidth, rhs.lensWidth).append(this.pd, rhs.pd).append(this.cyl, rhs.cyl).append(this.bottomDistance, rhs.bottomDistance).append(this.ed, rhs.ed).append(this.topDistance, rhs.topDistance).isEquals();
    }
  }
}
