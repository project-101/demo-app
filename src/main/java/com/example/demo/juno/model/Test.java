package com.example.demo.juno.model;

public class Test {
  private StoreTypeEnum storeTypeEnum;

  public StoreTypeEnum getStoreTypeEnum() {
    return storeTypeEnum;
  }

  public void setStoreTypeEnum(StoreTypeEnum storeTypeEnum) {
    this.storeTypeEnum = storeTypeEnum;
  }
}
