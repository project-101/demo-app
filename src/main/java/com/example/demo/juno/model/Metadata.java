//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"apiClient"})
public class Metadata {
  @JsonProperty("apiClient")
  private String apiClient;

  public Metadata() {
  }

  @JsonProperty("apiClient")
  public String getApiClient() {
    return this.apiClient;
  }

  @JsonProperty("apiClient")
  public void setApiClient(String apiClient) {
    this.apiClient = apiClient;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("apiClient", this.apiClient).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.apiClient).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Metadata)) {
      return false;
    } else {
      Metadata rhs = (Metadata)other;
      return (new EqualsBuilder()).append(this.apiClient, rhs.apiClient).isEquals();
    }
  }
}
