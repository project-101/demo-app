//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum PowerRequired {
  POWER_NOT_REQUIRED("POWER_NOT_REQUIRED"),
  POWER_REQUIRED("POWER_REQUIRED"),
  POWER_SUBMITTED("POWER_SUBMITTED");

  private final String value;
  private static final Map<String, PowerRequired> CONSTANTS = new HashMap();

  private PowerRequired(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static PowerRequired fromValue(String value) {
    PowerRequired constant = (PowerRequired)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    PowerRequired[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      PowerRequired c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
