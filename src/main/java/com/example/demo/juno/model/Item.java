//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "productId", "productName", "productType", "relatedItems", "sku", "quantity", "availableQuantity", "quantityCancelled", "quantityInvoiced", "quantityRefunded", "quantityShipped", "quantityReturnable", "powerRequired", "lensCategory", "lensType", "prescription", "powerwiseId", "promoItems", "options", "catalogPrices", "price", "framePrice", "amountRefunded", "giftAmount", "franchiseAmount", "freeFrame", "gender", "productTypeValue", "amount", "appliedRules", "coupons", "giftMessage", "description", "inventory", "brandName", "name", "modelName", "productUrl", "thumbnail", "courierTrackingNumber", "courierTrackingUrl", "brandId", "classificationId", "status", "statusHistory", "createdAt", "updatedAt", "barCode", "subscription", "hto", "frameType", "classification", "frameShape", "frameSize", "subcategory", "shipToStoreRequired", "isLocalFittingRequired", "localFittingFacility", "productDeliveryType", "reviews"})
public class Item {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("productId")
  private Long productId;
  @JsonProperty("productName")
  private String productName;
  @JsonProperty("productType")
  private String productType;
  @JsonProperty("relatedItems")
  @Valid
  private List<RelatedItem> relatedItems = new ArrayList();
  @JsonProperty("sku")
  private String sku;
  @JsonProperty("quantity")
  private Integer quantity;
  @JsonProperty("availableQuantity")
  private Integer availableQuantity;
  @JsonProperty("quantityCancelled")
  private Integer quantityCancelled;
  @JsonProperty("quantityInvoiced")
  private Integer quantityInvoiced;
  @JsonProperty("quantityRefunded")
  private Integer quantityRefunded;
  @JsonProperty("quantityShipped")
  private Integer quantityShipped;
  @JsonProperty("quantityReturnable")
  private Integer quantityReturnable;
  @JsonProperty("powerRequired")
  private PowerRequired powerRequired;
  @JsonProperty("lensCategory")
  private LensCategory lensCategory;
  @JsonProperty("lensType")
  private LensType lensType;
  @JsonProperty("prescription")
  @Valid
  private Prescription prescription;
  @JsonProperty("powerwiseId")
  private String powerwiseId;
  @JsonProperty("promoItems")
  private String promoItems;
  @JsonProperty("options")
  @Valid
  private List<Option> options = new ArrayList();
  @JsonProperty("catalogPrices")
  @Valid
  private List<Price> catalogPrices = new ArrayList();
  @JsonProperty("price")
  @Valid
  private Price price;
  @JsonProperty("framePrice")
  @Valid
  private Price framePrice;
  @JsonProperty("amountRefunded")
  @Valid
  private Price amountRefunded;
  @JsonProperty("giftAmount")
  @Valid
  private Price giftAmount;
  @JsonProperty("franchiseAmount")
  @Valid
  private Price franchiseAmount;
  @JsonProperty("freeFrame")
  private Boolean freeFrame;
  @JsonProperty("gender")
  private String gender;
  @JsonProperty("productTypeValue")
  private String productTypeValue;
  @JsonProperty("amount")
  @Valid
  private TotalAmount amount;
  @JsonProperty("appliedRules")
  @Valid
  private List<Integer> appliedRules = new ArrayList();
  @JsonProperty("coupons")
  @Valid
  private List<String> coupons = new ArrayList();
  @JsonProperty("giftMessage")
  @Valid
  private GiftMessage giftMessage;
  @JsonProperty("description")
  private String description;
  @JsonProperty("inventory")
  @Valid
  private Inventory inventory;
  @JsonProperty("brandName")
  private String brandName;
  @JsonProperty("name")
  private String name;
  @JsonProperty("modelName")
  private String modelName;
  @JsonProperty("productUrl")
  private String productUrl;
  @JsonProperty("thumbnail")
  private String thumbnail;
  @JsonProperty("courierTrackingNumber")
  private String courierTrackingNumber;
  @JsonProperty("courierTrackingUrl")
  private String courierTrackingUrl;
  @JsonProperty("brandId")
  private String brandId;
  @JsonProperty("classificationId")
  private String classificationId;
  @JsonProperty("status")
  @Valid
  private ItemStatus status;
  @JsonProperty("statusHistory")
  @Valid
  private List<ItemStatus> statusHistory = new ArrayList();
  @JsonProperty("createdAt")
  private Long createdAt;
  @JsonProperty("updatedAt")
  private Long updatedAt;
  @JsonProperty("barCode")
  private String barCode;
  @JsonProperty("subscription")
  @Valid
  private Subscription subscription;
  @JsonProperty("hto")
  @Valid
  private Hto hto;
  @JsonProperty("frameType")
  private String frameType;
  @JsonProperty("classification")
  private String classification;
  @JsonProperty("frameShape")
  private String frameShape;
  @JsonProperty("frameSize")
  private String frameSize;
  @JsonProperty("subcategory")
  private String subcategory;
  @JsonProperty("shipToStoreRequired")
  private Boolean shipToStoreRequired;
  @JsonProperty("isLocalFittingRequired")
  private Boolean isLocalFittingRequired;
  @JsonProperty("localFittingFacility")
  private String localFittingFacility;
  @JsonProperty("productDeliveryType")
  private ProductDeliveryTypeEnum productDeliveryType;
  @JsonProperty("reviews")
  @Valid
  private List<Reviews> reviews = new ArrayList();

  public Item() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("productId")
  public Long getProductId() {
    return this.productId;
  }

  @JsonProperty("productId")
  public void setProductId(Long productId) {
    this.productId = productId;
  }

  @JsonProperty("productName")
  public String getProductName() {
    return this.productName;
  }

  @JsonProperty("productName")
  public void setProductName(String productName) {
    this.productName = productName;
  }

  @JsonProperty("productType")
  public String getProductType() {
    return this.productType;
  }

  @JsonProperty("productType")
  public void setProductType(String productType) {
    this.productType = productType;
  }

  @JsonProperty("relatedItems")
  public List<RelatedItem> getRelatedItems() {
    return this.relatedItems;
  }

  @JsonProperty("relatedItems")
  public void setRelatedItems(List<RelatedItem> relatedItems) {
    this.relatedItems = relatedItems;
  }

  @JsonProperty("sku")
  public String getSku() {
    return this.sku;
  }

  @JsonProperty("sku")
  public void setSku(String sku) {
    this.sku = sku;
  }

  @JsonProperty("quantity")
  public Integer getQuantity() {
    return this.quantity;
  }

  @JsonProperty("quantity")
  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  @JsonProperty("availableQuantity")
  public Integer getAvailableQuantity() {
    return this.availableQuantity;
  }

  @JsonProperty("availableQuantity")
  public void setAvailableQuantity(Integer availableQuantity) {
    this.availableQuantity = availableQuantity;
  }

  @JsonProperty("quantityCancelled")
  public Integer getQuantityCancelled() {
    return this.quantityCancelled;
  }

  @JsonProperty("quantityCancelled")
  public void setQuantityCancelled(Integer quantityCancelled) {
    this.quantityCancelled = quantityCancelled;
  }

  @JsonProperty("quantityInvoiced")
  public Integer getQuantityInvoiced() {
    return this.quantityInvoiced;
  }

  @JsonProperty("quantityInvoiced")
  public void setQuantityInvoiced(Integer quantityInvoiced) {
    this.quantityInvoiced = quantityInvoiced;
  }

  @JsonProperty("quantityRefunded")
  public Integer getQuantityRefunded() {
    return this.quantityRefunded;
  }

  @JsonProperty("quantityRefunded")
  public void setQuantityRefunded(Integer quantityRefunded) {
    this.quantityRefunded = quantityRefunded;
  }

  @JsonProperty("quantityShipped")
  public Integer getQuantityShipped() {
    return this.quantityShipped;
  }

  @JsonProperty("quantityShipped")
  public void setQuantityShipped(Integer quantityShipped) {
    this.quantityShipped = quantityShipped;
  }

  @JsonProperty("quantityReturnable")
  public Integer getQuantityReturnable() {
    return this.quantityReturnable;
  }

  @JsonProperty("quantityReturnable")
  public void setQuantityReturnable(Integer quantityReturnable) {
    this.quantityReturnable = quantityReturnable;
  }

  @JsonProperty("powerRequired")
  public PowerRequired getPowerRequired() {
    return this.powerRequired;
  }

  @JsonProperty("powerRequired")
  public void setPowerRequired(PowerRequired powerRequired) {
    this.powerRequired = powerRequired;
  }

  @JsonProperty("lensCategory")
  public LensCategory getLensCategory() {
    return this.lensCategory;
  }

  @JsonProperty("lensCategory")
  public void setLensCategory(LensCategory lensCategory) {
    this.lensCategory = lensCategory;
  }

  @JsonProperty("lensType")
  public LensType getLensType() {
    return this.lensType;
  }

  @JsonProperty("lensType")
  public void setLensType(LensType lensType) {
    this.lensType = lensType;
  }

  @JsonProperty("prescription")
  public Prescription getPrescription() {
    return this.prescription;
  }

  @JsonProperty("prescription")
  public void setPrescription(Prescription prescription) {
    this.prescription = prescription;
  }

  @JsonProperty("powerwiseId")
  public String getPowerwiseId() {
    return this.powerwiseId;
  }

  @JsonProperty("powerwiseId")
  public void setPowerwiseId(String powerwiseId) {
    this.powerwiseId = powerwiseId;
  }

  @JsonProperty("promoItems")
  public String getPromoItems() {
    return this.promoItems;
  }

  @JsonProperty("promoItems")
  public void setPromoItems(String promoItems) {
    this.promoItems = promoItems;
  }

  @JsonProperty("options")
  public List<Option> getOptions() {
    return this.options;
  }

  @JsonProperty("options")
  public void setOptions(List<Option> options) {
    this.options = options;
  }

  @JsonProperty("catalogPrices")
  public List<Price> getCatalogPrices() {
    return this.catalogPrices;
  }

  @JsonProperty("catalogPrices")
  public void setCatalogPrices(List<Price> catalogPrices) {
    this.catalogPrices = catalogPrices;
  }

  @JsonProperty("price")
  public Price getPrice() {
    return this.price;
  }

  @JsonProperty("price")
  public void setPrice(Price price) {
    this.price = price;
  }

  @JsonProperty("framePrice")
  public Price getFramePrice() {
    return this.framePrice;
  }

  @JsonProperty("framePrice")
  public void setFramePrice(Price framePrice) {
    this.framePrice = framePrice;
  }

  @JsonProperty("amountRefunded")
  public Price getAmountRefunded() {
    return this.amountRefunded;
  }

  @JsonProperty("amountRefunded")
  public void setAmountRefunded(Price amountRefunded) {
    this.amountRefunded = amountRefunded;
  }

  @JsonProperty("giftAmount")
  public Price getGiftAmount() {
    return this.giftAmount;
  }

  @JsonProperty("giftAmount")
  public void setGiftAmount(Price giftAmount) {
    this.giftAmount = giftAmount;
  }

  @JsonProperty("franchiseAmount")
  public Price getFranchiseAmount() {
    return this.franchiseAmount;
  }

  @JsonProperty("franchiseAmount")
  public void setFranchiseAmount(Price franchiseAmount) {
    this.franchiseAmount = franchiseAmount;
  }

  @JsonProperty("freeFrame")
  public Boolean getFreeFrame() {
    return this.freeFrame;
  }

  @JsonProperty("freeFrame")
  public void setFreeFrame(Boolean freeFrame) {
    this.freeFrame = freeFrame;
  }

  @JsonProperty("gender")
  public String getGender() {
    return this.gender;
  }

  @JsonProperty("gender")
  public void setGender(String gender) {
    this.gender = gender;
  }

  @JsonProperty("productTypeValue")
  public String getProductTypeValue() {
    return this.productTypeValue;
  }

  @JsonProperty("productTypeValue")
  public void setProductTypeValue(String productTypeValue) {
    this.productTypeValue = productTypeValue;
  }

  @JsonProperty("amount")
  public TotalAmount getAmount() {
    return this.amount;
  }

  @JsonProperty("amount")
  public void setAmount(TotalAmount amount) {
    this.amount = amount;
  }

  @JsonProperty("appliedRules")
  public List<Integer> getAppliedRules() {
    return this.appliedRules;
  }

  @JsonProperty("appliedRules")
  public void setAppliedRules(List<Integer> appliedRules) {
    this.appliedRules = appliedRules;
  }

  @JsonProperty("coupons")
  public List<String> getCoupons() {
    return this.coupons;
  }

  @JsonProperty("coupons")
  public void setCoupons(List<String> coupons) {
    this.coupons = coupons;
  }

  @JsonProperty("giftMessage")
  public GiftMessage getGiftMessage() {
    return this.giftMessage;
  }

  @JsonProperty("giftMessage")
  public void setGiftMessage(GiftMessage giftMessage) {
    this.giftMessage = giftMessage;
  }

  @JsonProperty("description")
  public String getDescription() {
    return this.description;
  }

  @JsonProperty("description")
  public void setDescription(String description) {
    this.description = description;
  }

  @JsonProperty("inventory")
  public Inventory getInventory() {
    return this.inventory;
  }

  @JsonProperty("inventory")
  public void setInventory(Inventory inventory) {
    this.inventory = inventory;
  }

  @JsonProperty("brandName")
  public String getBrandName() {
    return this.brandName;
  }

  @JsonProperty("brandName")
  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("modelName")
  public String getModelName() {
    return this.modelName;
  }

  @JsonProperty("modelName")
  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  @JsonProperty("productUrl")
  public String getProductUrl() {
    return this.productUrl;
  }

  @JsonProperty("productUrl")
  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  @JsonProperty("thumbnail")
  public String getThumbnail() {
    return this.thumbnail;
  }

  @JsonProperty("thumbnail")
  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  @JsonProperty("courierTrackingNumber")
  public String getCourierTrackingNumber() {
    return this.courierTrackingNumber;
  }

  @JsonProperty("courierTrackingNumber")
  public void setCourierTrackingNumber(String courierTrackingNumber) {
    this.courierTrackingNumber = courierTrackingNumber;
  }

  @JsonProperty("courierTrackingUrl")
  public String getCourierTrackingUrl() {
    return this.courierTrackingUrl;
  }

  @JsonProperty("courierTrackingUrl")
  public void setCourierTrackingUrl(String courierTrackingUrl) {
    this.courierTrackingUrl = courierTrackingUrl;
  }

  @JsonProperty("brandId")
  public String getBrandId() {
    return this.brandId;
  }

  @JsonProperty("brandId")
  public void setBrandId(String brandId) {
    this.brandId = brandId;
  }

  @JsonProperty("classificationId")
  public String getClassificationId() {
    return this.classificationId;
  }

  @JsonProperty("classificationId")
  public void setClassificationId(String classificationId) {
    this.classificationId = classificationId;
  }

  @JsonProperty("status")
  public ItemStatus getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(ItemStatus status) {
    this.status = status;
  }

  @JsonProperty("statusHistory")
  public List<ItemStatus> getStatusHistory() {
    return this.statusHistory;
  }

  @JsonProperty("statusHistory")
  public void setStatusHistory(List<ItemStatus> statusHistory) {
    this.statusHistory = statusHistory;
  }

  @JsonProperty("createdAt")
  public Long getCreatedAt() {
    return this.createdAt;
  }

  @JsonProperty("createdAt")
  public void setCreatedAt(Long createdAt) {
    this.createdAt = createdAt;
  }

  @JsonProperty("updatedAt")
  public Long getUpdatedAt() {
    return this.updatedAt;
  }

  @JsonProperty("updatedAt")
  public void setUpdatedAt(Long updatedAt) {
    this.updatedAt = updatedAt;
  }

  @JsonProperty("barCode")
  public String getBarCode() {
    return this.barCode;
  }

  @JsonProperty("barCode")
  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  @JsonProperty("subscription")
  public Subscription getSubscription() {
    return this.subscription;
  }

  @JsonProperty("subscription")
  public void setSubscription(Subscription subscription) {
    this.subscription = subscription;
  }

  @JsonProperty("hto")
  public Hto getHto() {
    return this.hto;
  }

  @JsonProperty("hto")
  public void setHto(Hto hto) {
    this.hto = hto;
  }

  @JsonProperty("frameType")
  public String getFrameType() {
    return this.frameType;
  }

  @JsonProperty("frameType")
  public void setFrameType(String frameType) {
    this.frameType = frameType;
  }

  @JsonProperty("classification")
  public String getClassification() {
    return this.classification;
  }

  @JsonProperty("classification")
  public void setClassification(String classification) {
    this.classification = classification;
  }

  @JsonProperty("frameShape")
  public String getFrameShape() {
    return this.frameShape;
  }

  @JsonProperty("frameShape")
  public void setFrameShape(String frameShape) {
    this.frameShape = frameShape;
  }

  @JsonProperty("frameSize")
  public String getFrameSize() {
    return this.frameSize;
  }

  @JsonProperty("frameSize")
  public void setFrameSize(String frameSize) {
    this.frameSize = frameSize;
  }

  @JsonProperty("subcategory")
  public String getSubcategory() {
    return this.subcategory;
  }

  @JsonProperty("subcategory")
  public void setSubcategory(String subcategory) {
    this.subcategory = subcategory;
  }

  @JsonProperty("shipToStoreRequired")
  public Boolean getShipToStoreRequired() {
    return this.shipToStoreRequired;
  }

  @JsonProperty("shipToStoreRequired")
  public void setShipToStoreRequired(Boolean shipToStoreRequired) {
    this.shipToStoreRequired = shipToStoreRequired;
  }

  @JsonProperty("isLocalFittingRequired")
  public Boolean getIsLocalFittingRequired() {
    return this.isLocalFittingRequired;
  }

  @JsonProperty("isLocalFittingRequired")
  public void setIsLocalFittingRequired(Boolean isLocalFittingRequired) {
    this.isLocalFittingRequired = isLocalFittingRequired;
  }

  @JsonProperty("localFittingFacility")
  public String getLocalFittingFacility() {
    return this.localFittingFacility;
  }

  @JsonProperty("localFittingFacility")
  public void setLocalFittingFacility(String localFittingFacility) {
    this.localFittingFacility = localFittingFacility;
  }

  @JsonProperty("productDeliveryType")
  public ProductDeliveryTypeEnum getProductDeliveryType() {
    return this.productDeliveryType;
  }

  @JsonProperty("productDeliveryType")
  public void setProductDeliveryType(ProductDeliveryTypeEnum productDeliveryType) {
    this.productDeliveryType = productDeliveryType;
  }

  @JsonProperty("reviews")
  public List<Reviews> getReviews() {
    return this.reviews;
  }

  @JsonProperty("reviews")
  public void setReviews(List<Reviews> reviews) {
    this.reviews = reviews;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("productId", this.productId).append("productName", this.productName).append("productType", this.productType).append("relatedItems", this.relatedItems).append("sku", this.sku).append("quantity", this.quantity).append("availableQuantity", this.availableQuantity).append("quantityCancelled", this.quantityCancelled).append("quantityInvoiced", this.quantityInvoiced).append("quantityRefunded", this.quantityRefunded).append("quantityShipped", this.quantityShipped).append("quantityReturnable", this.quantityReturnable).append("powerRequired", this.powerRequired).append("lensCategory", this.lensCategory).append("lensType", this.lensType).append("prescription", this.prescription).append("powerwiseId", this.powerwiseId).append("promoItems", this.promoItems).append("options", this.options).append("catalogPrices", this.catalogPrices).append("price", this.price).append("framePrice", this.framePrice).append("amountRefunded", this.amountRefunded).append("giftAmount", this.giftAmount).append("franchiseAmount", this.franchiseAmount).append("freeFrame", this.freeFrame).append("gender", this.gender).append("productTypeValue", this.productTypeValue).append("amount", this.amount).append("appliedRules", this.appliedRules).append("coupons", this.coupons).append("giftMessage", this.giftMessage).append("description", this.description).append("inventory", this.inventory).append("brandName", this.brandName).append("name", this.name).append("modelName", this.modelName).append("productUrl", this.productUrl).append("thumbnail", this.thumbnail).append("courierTrackingNumber", this.courierTrackingNumber).append("courierTrackingUrl", this.courierTrackingUrl).append("brandId", this.brandId).append("classificationId", this.classificationId).append("status", this.status).append("statusHistory", this.statusHistory).append("createdAt", this.createdAt).append("updatedAt", this.updatedAt).append("barCode", this.barCode).append("subscription", this.subscription).append("hto", this.hto).append("frameType", this.frameType).append("classification", this.classification).append("frameShape", this.frameShape).append("frameSize", this.frameSize).append("subcategory", this.subcategory).append("shipToStoreRequired", this.shipToStoreRequired).append("isLocalFittingRequired", this.isLocalFittingRequired).append("localFittingFacility", this.localFittingFacility).append("productDeliveryType", this.productDeliveryType).append("reviews", this.reviews).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.quantityReturnable).append(this.quantityCancelled).append(this.powerwiseId).append(this.frameShape).append(this.productName).append(this.productTypeValue).append(this.createdAt).append(this.reviews).append(this.coupons).append(this.price).append(this.amountRefunded).append(this.appliedRules).append(this.promoItems).append(this.options).append(this.id).append(this.sku).append(this.updatedAt).append(this.brandName).append(this.thumbnail).append(this.productId).append(this.giftMessage).append(this.relatedItems).append(this.freeFrame).append(this.classification).append(this.classificationId).append(this.barCode).append(this.quantityShipped).append(this.courierTrackingNumber).append(this.brandId).append(this.name).append(this.frameType).append(this.quantityInvoiced).append(this.subcategory).append(this.localFittingFacility).append(this.shipToStoreRequired).append(this.status).append(this.powerRequired).append(this.gender).append(this.lensCategory).append(this.description).append(this.franchiseAmount).append(this.subscription).append(this.inventory).append(this.catalogPrices).append(this.quantityRefunded).append(this.courierTrackingUrl).append(this.framePrice).append(this.lensType).append(this.productDeliveryType).append(this.productType).append(this.giftAmount).append(this.availableQuantity).append(this.amount).append(this.hto).append(this.quantity).append(this.isLocalFittingRequired).append(this.modelName).append(this.statusHistory).append(this.frameSize).append(this.prescription).append(this.productUrl).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Item)) {
      return false;
    } else {
      Item rhs = (Item)other;
      return (new EqualsBuilder()).append(this.quantityReturnable, rhs.quantityReturnable).append(this.quantityCancelled, rhs.quantityCancelled).append(this.powerwiseId, rhs.powerwiseId).append(this.frameShape, rhs.frameShape).append(this.productName, rhs.productName).append(this.productTypeValue, rhs.productTypeValue).append(this.createdAt, rhs.createdAt).append(this.reviews, rhs.reviews).append(this.coupons, rhs.coupons).append(this.price, rhs.price).append(this.amountRefunded, rhs.amountRefunded).append(this.appliedRules, rhs.appliedRules).append(this.promoItems, rhs.promoItems).append(this.options, rhs.options).append(this.id, rhs.id).append(this.sku, rhs.sku).append(this.updatedAt, rhs.updatedAt).append(this.brandName, rhs.brandName).append(this.thumbnail, rhs.thumbnail).append(this.productId, rhs.productId).append(this.giftMessage, rhs.giftMessage).append(this.relatedItems, rhs.relatedItems).append(this.freeFrame, rhs.freeFrame).append(this.classification, rhs.classification).append(this.classificationId, rhs.classificationId).append(this.barCode, rhs.barCode).append(this.quantityShipped, rhs.quantityShipped).append(this.courierTrackingNumber, rhs.courierTrackingNumber).append(this.brandId, rhs.brandId).append(this.name, rhs.name).append(this.frameType, rhs.frameType).append(this.quantityInvoiced, rhs.quantityInvoiced).append(this.subcategory, rhs.subcategory).append(this.localFittingFacility, rhs.localFittingFacility).append(this.shipToStoreRequired, rhs.shipToStoreRequired).append(this.status, rhs.status).append(this.powerRequired, rhs.powerRequired).append(this.gender, rhs.gender).append(this.lensCategory, rhs.lensCategory).append(this.description, rhs.description).append(this.franchiseAmount, rhs.franchiseAmount).append(this.subscription, rhs.subscription).append(this.inventory, rhs.inventory).append(this.catalogPrices, rhs.catalogPrices).append(this.quantityRefunded, rhs.quantityRefunded).append(this.courierTrackingUrl, rhs.courierTrackingUrl).append(this.framePrice, rhs.framePrice).append(this.lensType, rhs.lensType).append(this.productDeliveryType, rhs.productDeliveryType).append(this.productType, rhs.productType).append(this.giftAmount, rhs.giftAmount).append(this.availableQuantity, rhs.availableQuantity).append(this.amount, rhs.amount).append(this.hto, rhs.hto).append(this.quantity, rhs.quantity).append(this.isLocalFittingRequired, rhs.isLocalFittingRequired).append(this.modelName, rhs.modelName).append(this.statusHistory, rhs.statusHistory).append(this.frameSize, rhs.frameSize).append(this.prescription, rhs.prescription).append(this.productUrl, rhs.productUrl).isEquals();
    }
  }
}
