//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum OrderCancellableStatus {
  CANCELLABLE("CANCELLABLE"),
  NON_CANCELLABLE("NON_CANCELLABLE"),
  CANCELLATION_IN_PROCESS("CANCELLATION_IN_PROCESS"),
  CANCELLATION_FAILED("CANCELLATION_FAILED"),
  CANCELLED_NO_REFUND_NEEDED("CANCELLED_NO_REFUND_NEEDED"),
  CANCELLED_PAYMENT_REFUNDED("CANCELLED_PAYMENT_REFUNDED"),
  CANCELLED_PAYMENT_NOT_REFUNDED("CANCELLED_PAYMENT_NOT_REFUNDED");

  private final String value;
  private static final Map<String, OrderCancellableStatus> CONSTANTS = new HashMap();

  private OrderCancellableStatus(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static OrderCancellableStatus fromValue(String value) {
    OrderCancellableStatus constant = (OrderCancellableStatus)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    OrderCancellableStatus[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      OrderCancellableStatus c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
