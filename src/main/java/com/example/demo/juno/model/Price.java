//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"currencyCode", "value", "name"})
public class Price {
  @JsonProperty("currencyCode")
  private String currencyCode;
  @JsonProperty("value")
  private BigDecimal value;
  @JsonProperty("name")
  private String name;

  public Price() {
  }

  @JsonProperty("currencyCode")
  public String getCurrencyCode() {
    return this.currencyCode;
  }

  @JsonProperty("currencyCode")
  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  @JsonProperty("value")
  public BigDecimal getValue() {
    return this.value;
  }

  @JsonProperty("value")
  public void setValue(BigDecimal value) {
    this.value = value;
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("currencyCode", this.currencyCode).append("value", this.value).append("name", this.name).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.name).append(this.currencyCode).append(this.value).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Price)) {
      return false;
    } else {
      Price rhs = (Price)other;
      return (new EqualsBuilder()).append(this.name, rhs.name).append(this.currencyCode, rhs.currencyCode).append(this.value, rhs.value).isEquals();
    }
  }
}
