//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum LensCategory {
  EYEGLASS("EYEGLASS"),
  SUNGLASS("SUNGLASS"),
  CONTACT_LENS("CONTACT_LENS");

  private final String value;
  private static final Map<String, LensCategory> CONSTANTS = new HashMap();

  private LensCategory(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static LensCategory fromValue(String value) {
    LensCategory constant = (LensCategory)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    LensCategory[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      LensCategory c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
