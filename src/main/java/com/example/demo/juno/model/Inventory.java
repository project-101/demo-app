//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"deliveryStoreId", "posStoreInventoryId"})
public class Inventory {
  @JsonProperty("deliveryStoreId")
  private Long deliveryStoreId;
  @JsonProperty("posStoreInventoryId")
  private Integer posStoreInventoryId;

  public Inventory() {
  }

  @JsonProperty("deliveryStoreId")
  public Long getDeliveryStoreId() {
    return this.deliveryStoreId;
  }

  @JsonProperty("deliveryStoreId")
  public void setDeliveryStoreId(Long deliveryStoreId) {
    this.deliveryStoreId = deliveryStoreId;
  }

  @JsonProperty("posStoreInventoryId")
  public Integer getPosStoreInventoryId() {
    return this.posStoreInventoryId;
  }

  @JsonProperty("posStoreInventoryId")
  public void setPosStoreInventoryId(Integer posStoreInventoryId) {
    this.posStoreInventoryId = posStoreInventoryId;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("deliveryStoreId", this.deliveryStoreId).append("posStoreInventoryId", this.posStoreInventoryId).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.deliveryStoreId).append(this.posStoreInventoryId).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Inventory)) {
      return false;
    } else {
      Inventory rhs = (Inventory)other;
      return (new EqualsBuilder()).append(this.deliveryStoreId, rhs.deliveryStoreId).append(this.posStoreInventoryId, rhs.posStoreInventoryId).isEquals();
    }
  }
}
