//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "label", "disposability", "numberOfBoxes", "discount", "supply", "duration", "prices", "isBothEye"})
public class Subscription {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("label")
  private String label;
  @JsonProperty("disposability")
  private Integer disposability;
  @JsonProperty("numberOfBoxes")
  private Integer numberOfBoxes;
  @JsonProperty("discount")
  private BigDecimal discount;
  @JsonProperty("supply")
  private String supply;
  @JsonProperty("duration")
  private Integer duration;
  @JsonProperty("prices")
  @Valid
  private List<Price> prices = new ArrayList();
  @JsonProperty("isBothEye")
  private String isBothEye;

  public Subscription() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("label")
  public String getLabel() {
    return this.label;
  }

  @JsonProperty("label")
  public void setLabel(String label) {
    this.label = label;
  }

  @JsonProperty("disposability")
  public Integer getDisposability() {
    return this.disposability;
  }

  @JsonProperty("disposability")
  public void setDisposability(Integer disposability) {
    this.disposability = disposability;
  }

  @JsonProperty("numberOfBoxes")
  public Integer getNumberOfBoxes() {
    return this.numberOfBoxes;
  }

  @JsonProperty("numberOfBoxes")
  public void setNumberOfBoxes(Integer numberOfBoxes) {
    this.numberOfBoxes = numberOfBoxes;
  }

  @JsonProperty("discount")
  public BigDecimal getDiscount() {
    return this.discount;
  }

  @JsonProperty("discount")
  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @JsonProperty("supply")
  public String getSupply() {
    return this.supply;
  }

  @JsonProperty("supply")
  public void setSupply(String supply) {
    this.supply = supply;
  }

  @JsonProperty("duration")
  public Integer getDuration() {
    return this.duration;
  }

  @JsonProperty("duration")
  public void setDuration(Integer duration) {
    this.duration = duration;
  }

  @JsonProperty("prices")
  public List<Price> getPrices() {
    return this.prices;
  }

  @JsonProperty("prices")
  public void setPrices(List<Price> prices) {
    this.prices = prices;
  }

  @JsonProperty("isBothEye")
  public String getIsBothEye() {
    return this.isBothEye;
  }

  @JsonProperty("isBothEye")
  public void setIsBothEye(String isBothEye) {
    this.isBothEye = isBothEye;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("label", this.label).append("disposability", this.disposability).append("numberOfBoxes", this.numberOfBoxes).append("discount", this.discount).append("supply", this.supply).append("duration", this.duration).append("prices", this.prices).append("isBothEye", this.isBothEye).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.duration).append(this.isBothEye).append(this.numberOfBoxes).append(this.discount).append(this.disposability).append(this.id).append(this.label).append(this.prices).append(this.supply).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Subscription)) {
      return false;
    } else {
      Subscription rhs = (Subscription)other;
      return (new EqualsBuilder()).append(this.duration, rhs.duration).append(this.isBothEye, rhs.isBothEye).append(this.numberOfBoxes, rhs.numberOfBoxes).append(this.discount, rhs.discount).append(this.disposability, rhs.disposability).append(this.id, rhs.id).append(this.label, rhs.label).append(this.prices, rhs.prices).append(this.supply, rhs.supply).isEquals();
    }
  }
}
