//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "from", "to", "message", "orderComment", "mobileNumber"})
public class GiftMessage {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("from")
  private String from;
  @JsonProperty("to")
  private String to;
  @JsonProperty("message")
  private String message;
  @JsonProperty("orderComment")
  private String orderComment;
  @JsonProperty("mobileNumber")
  private String mobileNumber;

  public GiftMessage() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("from")
  public String getFrom() {
    return this.from;
  }

  @JsonProperty("from")
  public void setFrom(String from) {
    this.from = from;
  }

  @JsonProperty("to")
  public String getTo() {
    return this.to;
  }

  @JsonProperty("to")
  public void setTo(String to) {
    this.to = to;
  }

  @JsonProperty("message")
  public String getMessage() {
    return this.message;
  }

  @JsonProperty("message")
  public void setMessage(String message) {
    this.message = message;
  }

  @JsonProperty("orderComment")
  public String getOrderComment() {
    return this.orderComment;
  }

  @JsonProperty("orderComment")
  public void setOrderComment(String orderComment) {
    this.orderComment = orderComment;
  }

  @JsonProperty("mobileNumber")
  public String getMobileNumber() {
    return this.mobileNumber;
  }

  @JsonProperty("mobileNumber")
  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("from", this.from).append("to", this.to).append("message", this.message).append("orderComment", this.orderComment).append("mobileNumber", this.mobileNumber).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.orderComment).append(this.mobileNumber).append(this.from).append(this.id).append(this.to).append(this.message).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof GiftMessage)) {
      return false;
    } else {
      GiftMessage rhs = (GiftMessage)other;
      return (new EqualsBuilder()).append(this.orderComment, rhs.orderComment).append(this.mobileNumber, rhs.mobileNumber).append(this.from, rhs.from).append(this.id, rhs.id).append(this.to, rhs.to).append(this.message, rhs.message).isEquals();
    }
  }
}
