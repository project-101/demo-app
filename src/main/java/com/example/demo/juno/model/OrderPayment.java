package com.example.demo.juno.model;

import lombok.Data;

@Data
public class OrderPayment {

  private Order order;
}
