//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"createdTime", "updatedTime", "trackingNo", "courier", "trackStatus", "location", "status", "details"})
public class TrackingDetail {
  @JsonProperty("createdTime")
  private Long createdTime;
  @JsonProperty("updatedTime")
  private Long updatedTime;
  @JsonProperty("trackingNo")
  private String trackingNo;
  @JsonProperty("courier")
  private String courier;
  @JsonProperty("trackStatus")
  private String trackStatus;
  @JsonProperty("location")
  private String location;
  @JsonProperty("status")
  private String status;
  @JsonProperty("details")
  @Valid
  private List<Details> details = new ArrayList();

  public TrackingDetail() {
  }

  @JsonProperty("createdTime")
  public Long getCreatedTime() {
    return this.createdTime;
  }

  @JsonProperty("createdTime")
  public void setCreatedTime(Long createdTime) {
    this.createdTime = createdTime;
  }

  @JsonProperty("updatedTime")
  public Long getUpdatedTime() {
    return this.updatedTime;
  }

  @JsonProperty("updatedTime")
  public void setUpdatedTime(Long updatedTime) {
    this.updatedTime = updatedTime;
  }

  @JsonProperty("trackingNo")
  public String getTrackingNo() {
    return this.trackingNo;
  }

  @JsonProperty("trackingNo")
  public void setTrackingNo(String trackingNo) {
    this.trackingNo = trackingNo;
  }

  @JsonProperty("courier")
  public String getCourier() {
    return this.courier;
  }

  @JsonProperty("courier")
  public void setCourier(String courier) {
    this.courier = courier;
  }

  @JsonProperty("trackStatus")
  public String getTrackStatus() {
    return this.trackStatus;
  }

  @JsonProperty("trackStatus")
  public void setTrackStatus(String trackStatus) {
    this.trackStatus = trackStatus;
  }

  @JsonProperty("location")
  public String getLocation() {
    return this.location;
  }

  @JsonProperty("location")
  public void setLocation(String location) {
    this.location = location;
  }

  @JsonProperty("status")
  public String getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  @JsonProperty("details")
  public List<Details> getDetails() {
    return this.details;
  }

  @JsonProperty("details")
  public void setDetails(List<Details> details) {
    this.details = details;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("createdTime", this.createdTime).append("updatedTime", this.updatedTime).append("trackingNo", this.trackingNo).append("courier", this.courier).append("trackStatus", this.trackStatus).append("location", this.location).append("status", this.status).append("details", this.details).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.updatedTime).append(this.trackingNo).append(this.courier).append(this.trackStatus).append(this.createdTime).append(this.location).append(this.details).append(this.status).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof TrackingDetail)) {
      return false;
    } else {
      TrackingDetail rhs = (TrackingDetail)other;
      return (new EqualsBuilder()).append(this.updatedTime, rhs.updatedTime).append(this.trackingNo, rhs.trackingNo).append(this.courier, rhs.courier).append(this.trackStatus, rhs.trackStatus).append(this.createdTime, rhs.createdTime).append(this.location, rhs.location).append(this.details, rhs.details).append(this.status, rhs.status).isEquals();
    }
  }
}
