//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum PrescriptionType {
  EYEGLASS("EYEGLASS"),
  CONTACT_LENS("CONTACT_LENS"),
  SINGLE_VISION("SINGLE_VISION"),
  BIFOCAL("BIFOCAL"),
  ZERO_POWER("ZERO_POWER"),
  SUNGLASSES("SUNGLASSES"),
  TINTED_SV("TINTED_SV");

  private final String value;
  private static final Map<String, PrescriptionType> CONSTANTS = new HashMap();

  private PrescriptionType(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static PrescriptionType fromValue(String value) {
    PrescriptionType constant = (PrescriptionType)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    PrescriptionType[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      PrescriptionType c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
