//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum LensType {
  SINGLE_VISION("SINGLE_VISION"),
  BIFOCAL("BIFOCAL"),
  PROGRESSIVE("PROGRESSIVE"),
  ZERO_POWER("ZERO_POWER"),
  READING_GLASS("READING_GLASS"),
  MULTI_FOCAL("MULTI_FOCAL"),
  TORIC("TORIC"),
  FRAME_ONLY("FRAME_ONLY"),
  LENS_ONLY("LENS_ONLY"),
  LENS_ONLY_SINGLE_VISION("LENS_ONLY_SINGLE_VISION"),
  LENS_ONLY_BIFOCAL("LENS_ONLY_BIFOCAL"),
  LENS_ONLY_ZERO_POWER("LENS_ONLY_ZERO_POWER"),
  SINGLE_VISION_TOKAI("SINGLE_VISION_TOKAI"),
  BIFOCAL_TOKAI("BIFOCAL_TOKAI"),
  NORMAL("NORMAL"),
  POWER("POWER"),
  CONTACT_LENS("CONTACT_LENS"),
  TINTED_SV("TINTED_SV"),
  LENS_ONLY_TINTED_SV("LENS_ONLY_TINTED_SV"),
  SINGLE_VISION_RODENSTOCK("SINGLE_VISION_RODENSTOCK"),
  BIFOCAL_RODENSTOCK("BIFOCAL_RODENSTOCK"),
  PROGRESSIVE_RODENSTOCK("PROGRESSIVE_RODENSTOCK");

  private final String value;
  private static final Map<String, LensType> CONSTANTS = new HashMap();

  private LensType(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static LensType fromValue(String value) {
    LensType constant = (LensType)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    LensType[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      LensType c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
