//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"name", "branch", "ifsc", "accountHolderName", "accountNumber"})
public class Bank {
  @JsonProperty("name")
  private String name;
  @JsonProperty("branch")
  private String branch;
  @JsonProperty("ifsc")
  private String ifsc;
  @JsonProperty("accountHolderName")
  private String accountHolderName;
  @JsonProperty("accountNumber")
  private String accountNumber;

  public Bank() {
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("branch")
  public String getBranch() {
    return this.branch;
  }

  @JsonProperty("branch")
  public void setBranch(String branch) {
    this.branch = branch;
  }

  @JsonProperty("ifsc")
  public String getIfsc() {
    return this.ifsc;
  }

  @JsonProperty("ifsc")
  public void setIfsc(String ifsc) {
    this.ifsc = ifsc;
  }

  @JsonProperty("accountHolderName")
  public String getAccountHolderName() {
    return this.accountHolderName;
  }

  @JsonProperty("accountHolderName")
  public void setAccountHolderName(String accountHolderName) {
    this.accountHolderName = accountHolderName;
  }

  @JsonProperty("accountNumber")
  public String getAccountNumber() {
    return this.accountNumber;
  }

  @JsonProperty("accountNumber")
  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("name", this.name).append("branch", this.branch).append("ifsc", this.ifsc).append("accountHolderName", this.accountHolderName).append("accountNumber", this.accountNumber).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.name).append(this.ifsc).append(this.accountNumber).append(this.branch).append(this.accountHolderName).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Bank)) {
      return false;
    } else {
      Bank rhs = (Bank)other;
      return (new EqualsBuilder()).append(this.name, rhs.name).append(this.ifsc, rhs.ifsc).append(this.accountNumber, rhs.accountNumber).append(this.branch, rhs.branch).append(this.accountHolderName, rhs.accountHolderName).isEquals();
    }
  }
}
