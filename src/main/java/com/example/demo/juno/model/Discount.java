//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"code", "type", "amount", "message", "txnNum", "merchantRefId"})
public class Discount {
  @JsonProperty("code")
  private String code;
  @JsonProperty("type")
  private String type;
  @JsonProperty("amount")
  private BigDecimal amount;
  @JsonProperty("message")
  private String message;
  @JsonProperty("txnNum")
  private String txnNum;
  @JsonProperty("merchantRefId")
  private String merchantRefId;

  public Discount() {
  }

  @JsonProperty("code")
  public String getCode() {
    return this.code;
  }

  @JsonProperty("code")
  public void setCode(String code) {
    this.code = code;
  }

  @JsonProperty("type")
  public String getType() {
    return this.type;
  }

  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }

  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return this.amount;
  }

  @JsonProperty("amount")
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @JsonProperty("message")
  public String getMessage() {
    return this.message;
  }

  @JsonProperty("message")
  public void setMessage(String message) {
    this.message = message;
  }

  @JsonProperty("txnNum")
  public String getTxnNum() {
    return this.txnNum;
  }

  @JsonProperty("txnNum")
  public void setTxnNum(String txnNum) {
    this.txnNum = txnNum;
  }

  @JsonProperty("merchantRefId")
  public String getMerchantRefId() {
    return this.merchantRefId;
  }

  @JsonProperty("merchantRefId")
  public void setMerchantRefId(String merchantRefId) {
    this.merchantRefId = merchantRefId;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("code", this.code).append("type", this.type).append("amount", this.amount).append("message", this.message).append("txnNum", this.txnNum).append("merchantRefId", this.merchantRefId).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.amount).append(this.code).append(this.merchantRefId).append(this.type).append(this.message).append(this.txnNum).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Discount)) {
      return false;
    } else {
      Discount rhs = (Discount)other;
      return (new EqualsBuilder()).append(this.amount, rhs.amount).append(this.code, rhs.code).append(this.merchantRefId, rhs.merchantRefId).append(this.type, rhs.type).append(this.message, rhs.message).append(this.txnNum, rhs.txnNum).isEquals();
    }
  }
}
