//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"state", "status", "returnableStatus", "statusOrigin", "timestamp", "reason", "selfDispatched", "returnable"})
public class ItemStatus {
  @JsonProperty("state")
  private String state;
  @JsonProperty("status")
  private String status;
  @JsonProperty("returnableStatus")
  private ItemReturnableStatus returnableStatus;
  @JsonProperty("statusOrigin")
  private String statusOrigin;
  @JsonProperty("timestamp")
  private Long timestamp;
  @JsonProperty("reason")
  @Valid
  private Reason reason;
  @JsonProperty("selfDispatched")
  private Boolean selfDispatched;
  @JsonProperty("returnable")
  private Boolean returnable;

  public ItemStatus() {
  }

  @JsonProperty("state")
  public String getState() {
    return this.state;
  }

  @JsonProperty("state")
  public void setState(String state) {
    this.state = state;
  }

  @JsonProperty("status")
  public String getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  @JsonProperty("returnableStatus")
  public ItemReturnableStatus getReturnableStatus() {
    return this.returnableStatus;
  }

  @JsonProperty("returnableStatus")
  public void setReturnableStatus(ItemReturnableStatus returnableStatus) {
    this.returnableStatus = returnableStatus;
  }

  @JsonProperty("statusOrigin")
  public String getStatusOrigin() {
    return this.statusOrigin;
  }

  @JsonProperty("statusOrigin")
  public void setStatusOrigin(String statusOrigin) {
    this.statusOrigin = statusOrigin;
  }

  @JsonProperty("timestamp")
  public Long getTimestamp() {
    return this.timestamp;
  }

  @JsonProperty("timestamp")
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  @JsonProperty("reason")
  public Reason getReason() {
    return this.reason;
  }

  @JsonProperty("reason")
  public void setReason(Reason reason) {
    this.reason = reason;
  }

  @JsonProperty("selfDispatched")
  public Boolean getSelfDispatched() {
    return this.selfDispatched;
  }

  @JsonProperty("selfDispatched")
  public void setSelfDispatched(Boolean selfDispatched) {
    this.selfDispatched = selfDispatched;
  }

  @JsonProperty("returnable")
  public Boolean getReturnable() {
    return this.returnable;
  }

  @JsonProperty("returnable")
  public void setReturnable(Boolean returnable) {
    this.returnable = returnable;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("state", this.state).append("status", this.status).append("returnableStatus", this.returnableStatus).append("statusOrigin", this.statusOrigin).append("timestamp", this.timestamp).append("reason", this.reason).append("selfDispatched", this.selfDispatched).append("returnable", this.returnable).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.reason).append(this.returnable).append(this.selfDispatched).append(this.returnableStatus).append(this.statusOrigin).append(this.state).append(this.status).append(this.timestamp).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof ItemStatus)) {
      return false;
    } else {
      ItemStatus rhs = (ItemStatus)other;
      return (new EqualsBuilder()).append(this.reason, rhs.reason).append(this.returnable, rhs.returnable).append(this.selfDispatched, rhs.selfDispatched).append(this.returnableStatus, rhs.returnableStatus).append(this.statusOrigin, rhs.statusOrigin).append(this.state, rhs.state).append(this.status, rhs.status).append(this.timestamp, rhs.timestamp).isEquals();
    }
  }
}
