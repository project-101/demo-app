//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "prescriptionType", "powerType", "left", "right", "notes", "gender", "dob", "userName", "imageFileName", "prescriptionMethod", "optomId", "recordedAt"})
public class Prescription {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("prescriptionType")
  private PrescriptionType prescriptionType;
  @JsonProperty("powerType")
  private PowerType powerType;
  @JsonProperty("left")
  @Valid
  private Power left;
  @JsonProperty("right")
  @Valid
  private Power right;
  @JsonProperty("notes")
  private String notes;
  @JsonProperty("gender")
  private String gender;
  @JsonProperty("dob")
  private String dob;
  @JsonProperty("userName")
  private String userName;
  @JsonProperty("imageFileName")
  private String imageFileName;
  @JsonProperty("prescriptionMethod")
  private String prescriptionMethod;
  @JsonProperty("optomId")
  private String optomId;
  @JsonProperty("recordedAt")
  private Long recordedAt;

  public Prescription() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("prescriptionType")
  public PrescriptionType getPrescriptionType() {
    return this.prescriptionType;
  }

  @JsonProperty("prescriptionType")
  public void setPrescriptionType(PrescriptionType prescriptionType) {
    this.prescriptionType = prescriptionType;
  }

  @JsonProperty("powerType")
  public PowerType getPowerType() {
    return this.powerType;
  }

  @JsonProperty("powerType")
  public void setPowerType(PowerType powerType) {
    this.powerType = powerType;
  }

  @JsonProperty("left")
  public Power getLeft() {
    return this.left;
  }

  @JsonProperty("left")
  public void setLeft(Power left) {
    this.left = left;
  }

  @JsonProperty("right")
  public Power getRight() {
    return this.right;
  }

  @JsonProperty("right")
  public void setRight(Power right) {
    this.right = right;
  }

  @JsonProperty("notes")
  public String getNotes() {
    return this.notes;
  }

  @JsonProperty("notes")
  public void setNotes(String notes) {
    this.notes = notes;
  }

  @JsonProperty("gender")
  public String getGender() {
    return this.gender;
  }

  @JsonProperty("gender")
  public void setGender(String gender) {
    this.gender = gender;
  }

  @JsonProperty("dob")
  public String getDob() {
    return this.dob;
  }

  @JsonProperty("dob")
  public void setDob(String dob) {
    this.dob = dob;
  }

  @JsonProperty("userName")
  public String getUserName() {
    return this.userName;
  }

  @JsonProperty("userName")
  public void setUserName(String userName) {
    this.userName = userName;
  }

  @JsonProperty("imageFileName")
  public String getImageFileName() {
    return this.imageFileName;
  }

  @JsonProperty("imageFileName")
  public void setImageFileName(String imageFileName) {
    this.imageFileName = imageFileName;
  }

  @JsonProperty("prescriptionMethod")
  public String getPrescriptionMethod() {
    return this.prescriptionMethod;
  }

  @JsonProperty("prescriptionMethod")
  public void setPrescriptionMethod(String prescriptionMethod) {
    this.prescriptionMethod = prescriptionMethod;
  }

  @JsonProperty("optomId")
  public String getOptomId() {
    return this.optomId;
  }

  @JsonProperty("optomId")
  public void setOptomId(String optomId) {
    this.optomId = optomId;
  }

  @JsonProperty("recordedAt")
  public Long getRecordedAt() {
    return this.recordedAt;
  }

  @JsonProperty("recordedAt")
  public void setRecordedAt(Long recordedAt) {
    this.recordedAt = recordedAt;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("prescriptionType", this.prescriptionType).append("powerType", this.powerType).append("left", this.left).append("right", this.right).append("notes", this.notes).append("gender", this.gender).append("dob", this.dob).append("userName", this.userName).append("imageFileName", this.imageFileName).append("prescriptionMethod", this.prescriptionMethod).append("optomId", this.optomId).append("recordedAt", this.recordedAt).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.prescriptionType).append(this.notes).append(this.gender).append(this.optomId).append(this.prescriptionMethod).append(this.right).append(this.userName).append(this.left).append(this.dob).append(this.recordedAt).append(this.imageFileName).append(this.id).append(this.powerType).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Prescription)) {
      return false;
    } else {
      Prescription rhs = (Prescription)other;
      return (new EqualsBuilder()).append(this.prescriptionType, rhs.prescriptionType).append(this.notes, rhs.notes).append(this.gender, rhs.gender).append(this.optomId, rhs.optomId).append(this.prescriptionMethod, rhs.prescriptionMethod).append(this.right, rhs.right).append(this.userName, rhs.userName).append(this.left, rhs.left).append(this.dob, rhs.dob).append(this.recordedAt, rhs.recordedAt).append(this.imageFileName, rhs.imageFileName).append(this.id, rhs.id).append(this.powerType, rhs.powerType).isEquals();
    }
  }
}
