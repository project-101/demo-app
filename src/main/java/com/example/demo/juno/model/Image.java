//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"reviewId", "thumbUrl", "originalUrl"})
public class Image {
  @JsonProperty("reviewId")
  private String reviewId;
  @JsonProperty("thumbUrl")
  private String thumbUrl;
  @JsonProperty("originalUrl")
  private String originalUrl;
  @JsonIgnore
  @Valid
  private Map<String, Object> additionalProperties = new HashMap();

  public Image() {
  }

  @JsonProperty("reviewId")
  public String getReviewId() {
    return this.reviewId;
  }

  @JsonProperty("reviewId")
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  @JsonProperty("thumbUrl")
  public String getThumbUrl() {
    return this.thumbUrl;
  }

  @JsonProperty("thumbUrl")
  public void setThumbUrl(String thumbUrl) {
    this.thumbUrl = thumbUrl;
  }

  @JsonProperty("originalUrl")
  public String getOriginalUrl() {
    return this.originalUrl;
  }

  @JsonProperty("originalUrl")
  public void setOriginalUrl(String originalUrl) {
    this.originalUrl = originalUrl;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("reviewId", this.reviewId).append("thumbUrl", this.thumbUrl).append("originalUrl", this.originalUrl).append("additionalProperties", this.additionalProperties).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.originalUrl).append(this.additionalProperties).append(this.thumbUrl).append(this.reviewId).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Image)) {
      return false;
    } else {
      Image rhs = (Image)other;
      return (new EqualsBuilder()).append(this.originalUrl, rhs.originalUrl).append(this.additionalProperties, rhs.additionalProperties).append(this.thumbUrl, rhs.thumbUrl).append(this.reviewId, rhs.reviewId).isEquals();
    }
  }
}
