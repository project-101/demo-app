//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum MerchantIdEnum {
  LENSKART("LENSKART"),
  DEALSKART("DEALSKART");

  private final String value;
  private static final Map<String, MerchantIdEnum> CONSTANTS = new HashMap();

  private MerchantIdEnum(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static MerchantIdEnum fromValue(String value) {
    MerchantIdEnum constant = (MerchantIdEnum)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    MerchantIdEnum[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      MerchantIdEnum c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
