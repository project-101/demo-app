//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "name", "url", "imageUrls"})
public class ProductDetail {
  @JsonProperty("id")
  private String id;
  @JsonProperty("name")
  private String name;
  @JsonProperty("url")
  private String url;
  @JsonProperty("imageUrls")
  private String imageUrls;
  @JsonIgnore
  @Valid
  private Map<String, Object> additionalProperties = new HashMap();

  public ProductDetail() {
  }

  @JsonProperty("id")
  public String getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(String id) {
    this.id = id;
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("url")
  public String getUrl() {
    return this.url;
  }

  @JsonProperty("url")
  public void setUrl(String url) {
    this.url = url;
  }

  @JsonProperty("imageUrls")
  public String getImageUrls() {
    return this.imageUrls;
  }

  @JsonProperty("imageUrls")
  public void setImageUrls(String imageUrls) {
    this.imageUrls = imageUrls;
  }

  @JsonAnyGetter
  public Map<String, Object> getAdditionalProperties() {
    return this.additionalProperties;
  }

  @JsonAnySetter
  public void setAdditionalProperty(String name, Object value) {
    this.additionalProperties.put(name, value);
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("name", this.name).append("url", this.url).append("imageUrls", this.imageUrls).append("additionalProperties", this.additionalProperties).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.name).append(this.id).append(this.additionalProperties).append(this.url).append(this.imageUrls).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof ProductDetail)) {
      return false;
    } else {
      ProductDetail rhs = (ProductDetail)other;
      return (new EqualsBuilder()).append(this.name, rhs.name).append(this.id, rhs.id).append(this.additionalProperties, rhs.additionalProperties).append(this.url, rhs.url).append(this.imageUrls, rhs.imageUrls).isEquals();
    }
  }
}
