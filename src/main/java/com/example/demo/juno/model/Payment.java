//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.math.BigDecimal;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"method", "gateway", "refCode", "amount"})
public class Payment {
  @JsonProperty("method")
  private String method;
  @JsonProperty("gateway")
  private String gateway;
  @JsonProperty("refCode")
  private String refCode;
  @JsonProperty("amount")
  private BigDecimal amount;

  public Payment() {
  }

  @JsonProperty("method")
  public String getMethod() {
    return this.method;
  }

  @JsonProperty("method")
  public void setMethod(String method) {
    this.method = method;
  }

  @JsonProperty("gateway")
  public String getGateway() {
    return this.gateway;
  }

  @JsonProperty("gateway")
  public void setGateway(String gateway) {
    this.gateway = gateway;
  }

  @JsonProperty("refCode")
  public String getRefCode() {
    return this.refCode;
  }

  @JsonProperty("refCode")
  public void setRefCode(String refCode) {
    this.refCode = refCode;
  }

  @JsonProperty("amount")
  public BigDecimal getAmount() {
    return this.amount;
  }

  @JsonProperty("amount")
  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("method", this.method).append("gateway", this.gateway).append("refCode", this.refCode).append("amount", this.amount).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.amount).append(this.method).append(this.refCode).append(this.gateway).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Payment)) {
      return false;
    } else {
      Payment rhs = (Payment)other;
      return (new EqualsBuilder()).append(this.amount, rhs.amount).append(this.method, rhs.method).append(this.refCode, rhs.refCode).append(this.gateway, rhs.gateway).isEquals();
    }
  }
}
