//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"refundId", "bankDetails", "status"})
public class Refund {
  @JsonProperty("refundId")
  private String refundId;
  @JsonProperty("bankDetails")
  @Valid
  private Bank bankDetails;
  @JsonProperty("status")
  private String status;

  public Refund() {
  }

  @JsonProperty("refundId")
  public String getRefundId() {
    return this.refundId;
  }

  @JsonProperty("refundId")
  public void setRefundId(String refundId) {
    this.refundId = refundId;
  }

  @JsonProperty("bankDetails")
  public Bank getBankDetails() {
    return this.bankDetails;
  }

  @JsonProperty("bankDetails")
  public void setBankDetails(Bank bankDetails) {
    this.bankDetails = bankDetails;
  }

  @JsonProperty("status")
  public String getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("refundId", this.refundId).append("bankDetails", this.bankDetails).append("status", this.status).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.bankDetails).append(this.refundId).append(this.status).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Refund)) {
      return false;
    } else {
      Refund rhs = (Refund)other;
      return (new EqualsBuilder()).append(this.bankDetails, rhs.bankDetails).append(this.refundId, rhs.refundId).append(this.status, rhs.status).isEquals();
    }
  }
}
