//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"status", "state", "returnable", "cancellable", "comment", "isVisibleOnFront", "statusOrigin", "timestamp", "reason", "trackingStatus", "cancellableStatus", "returnableStatus", "orderTrackingStatusCheckpoint"})
public class OrderStatus {
  @JsonProperty("status")
  private String status;
  @JsonProperty("state")
  private String state;
  @JsonProperty("returnable")
  private Boolean returnable;
  @JsonProperty("cancellable")
  private Boolean cancellable;
  @JsonProperty("comment")
  private String comment;
  @JsonProperty("isVisibleOnFront")
  private Boolean isVisibleOnFront;
  @JsonProperty("statusOrigin")
  private String statusOrigin;
  @JsonProperty("timestamp")
  private Long timestamp;
  @JsonProperty("reason")
  @Valid
  private Reason reason;
  @JsonProperty("trackingStatus")
  private OrderTrackingStatus trackingStatus;
  @JsonProperty("cancellableStatus")
  private OrderCancellableStatus cancellableStatus;
  @JsonProperty("returnableStatus")
  private OrderReturnableStatus returnableStatus;
  @JsonProperty("orderTrackingStatusCheckpoint")
  private OrderTrackingStatusCheckpoint orderTrackingStatusCheckpoint;

  public OrderStatus() {
  }

  @JsonProperty("status")
  public String getStatus() {
    return this.status;
  }

  @JsonProperty("status")
  public void setStatus(String status) {
    this.status = status;
  }

  @JsonProperty("state")
  public String getState() {
    return this.state;
  }

  @JsonProperty("state")
  public void setState(String state) {
    this.state = state;
  }

  @JsonProperty("returnable")
  public Boolean getReturnable() {
    return this.returnable;
  }

  @JsonProperty("returnable")
  public void setReturnable(Boolean returnable) {
    this.returnable = returnable;
  }

  @JsonProperty("cancellable")
  public Boolean getCancellable() {
    return this.cancellable;
  }

  @JsonProperty("cancellable")
  public void setCancellable(Boolean cancellable) {
    this.cancellable = cancellable;
  }

  @JsonProperty("comment")
  public String getComment() {
    return this.comment;
  }

  @JsonProperty("comment")
  public void setComment(String comment) {
    this.comment = comment;
  }

  @JsonProperty("isVisibleOnFront")
  public Boolean getIsVisibleOnFront() {
    return this.isVisibleOnFront;
  }

  @JsonProperty("isVisibleOnFront")
  public void setIsVisibleOnFront(Boolean isVisibleOnFront) {
    this.isVisibleOnFront = isVisibleOnFront;
  }

  @JsonProperty("statusOrigin")
  public String getStatusOrigin() {
    return this.statusOrigin;
  }

  @JsonProperty("statusOrigin")
  public void setStatusOrigin(String statusOrigin) {
    this.statusOrigin = statusOrigin;
  }

  @JsonProperty("timestamp")
  public Long getTimestamp() {
    return this.timestamp;
  }

  @JsonProperty("timestamp")
  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  @JsonProperty("reason")
  public Reason getReason() {
    return this.reason;
  }

  @JsonProperty("reason")
  public void setReason(Reason reason) {
    this.reason = reason;
  }

  @JsonProperty("trackingStatus")
  public OrderTrackingStatus getTrackingStatus() {
    return this.trackingStatus;
  }

  @JsonProperty("trackingStatus")
  public void setTrackingStatus(OrderTrackingStatus trackingStatus) {
    this.trackingStatus = trackingStatus;
  }

  @JsonProperty("cancellableStatus")
  public OrderCancellableStatus getCancellableStatus() {
    return this.cancellableStatus;
  }

  @JsonProperty("cancellableStatus")
  public void setCancellableStatus(OrderCancellableStatus cancellableStatus) {
    this.cancellableStatus = cancellableStatus;
  }

  @JsonProperty("returnableStatus")
  public OrderReturnableStatus getReturnableStatus() {
    return this.returnableStatus;
  }

  @JsonProperty("returnableStatus")
  public void setReturnableStatus(OrderReturnableStatus returnableStatus) {
    this.returnableStatus = returnableStatus;
  }

  @JsonProperty("orderTrackingStatusCheckpoint")
  public OrderTrackingStatusCheckpoint getOrderTrackingStatusCheckpoint() {
    return this.orderTrackingStatusCheckpoint;
  }

  @JsonProperty("orderTrackingStatusCheckpoint")
  public void setOrderTrackingStatusCheckpoint(OrderTrackingStatusCheckpoint orderTrackingStatusCheckpoint) {
    this.orderTrackingStatusCheckpoint = orderTrackingStatusCheckpoint;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("status", this.status).append("state", this.state).append("returnable", this.returnable).append("cancellable", this.cancellable).append("comment", this.comment).append("isVisibleOnFront", this.isVisibleOnFront).append("statusOrigin", this.statusOrigin).append("timestamp", this.timestamp).append("reason", this.reason).append("trackingStatus", this.trackingStatus).append("cancellableStatus", this.cancellableStatus).append("returnableStatus", this.returnableStatus).append("orderTrackingStatusCheckpoint", this.orderTrackingStatusCheckpoint).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.isVisibleOnFront).append(this.reason).append(this.returnableStatus).append(this.cancellable).append(this.cancellableStatus).append(this.returnable).append(this.statusOrigin).append(this.comment).append(this.state).append(this.trackingStatus).append(this.orderTrackingStatusCheckpoint).append(this.status).append(this.timestamp).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof OrderStatus)) {
      return false;
    } else {
      OrderStatus rhs = (OrderStatus)other;
      return (new EqualsBuilder()).append(this.isVisibleOnFront, rhs.isVisibleOnFront).append(this.reason, rhs.reason).append(this.returnableStatus, rhs.returnableStatus).append(this.cancellable, rhs.cancellable).append(this.cancellableStatus, rhs.cancellableStatus).append(this.returnable, rhs.returnable).append(this.statusOrigin, rhs.statusOrigin).append(this.comment, rhs.comment).append(this.state, rhs.state).append(this.trackingStatus, rhs.trackingStatus).append(this.orderTrackingStatusCheckpoint, rhs.orderTrackingStatusCheckpoint).append(this.status, rhs.status).append(this.timestamp, rhs.timestamp).isEquals();
    }
  }
}
