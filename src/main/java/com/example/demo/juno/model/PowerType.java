//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum PowerType {
  SINGLE_VISION("SINGLE_VISION"),
  BIFOCAL("BIFOCAL"),
  ZERO_POWER("ZERO_POWER"),
  CONTACT_LENS("CONTACT_LENS"),
  SUNGLASSES("SUNGLASSES"),
  TINTED_SV("TINTED_SV");

  private final String value;
  private static final Map<String, PowerType> CONSTANTS = new HashMap();

  private PowerType(String value) {
    this.value = value;
  }

  public String toString() {
    return this.value;
  }

  @JsonValue
  public String value() {
    return this.value;
  }

  @JsonCreator
  public static PowerType fromValue(String value) {
    PowerType constant = (PowerType)CONSTANTS.get(value);
    if (constant == null) {
      throw new IllegalArgumentException(value);
    } else {
      return constant;
    }
  }

  static {
    PowerType[] var0 = values();
    int var1 = var0.length;

    for(int var2 = 0; var2 < var1; ++var2) {
      PowerType c = var0[var2];
      CONSTANTS.put(c.value, c);
    }

  }
}
