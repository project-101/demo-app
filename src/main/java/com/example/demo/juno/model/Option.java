//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.example.demo.juno.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"id", "oid", "type", "name", "label", "value", "optionValue", "catalogPrices", "sku"})
public class Option {
  @JsonProperty("id")
  private Long id;
  @JsonProperty("oid")
  private String oid;
  @JsonProperty("type")
  private String type;
  @JsonProperty("name")
  private String name;
  @JsonProperty("label")
  private String label;
  @JsonProperty("value")
  private String value;
  @JsonProperty("optionValue")
  private String optionValue;
  @JsonProperty("catalogPrices")
  @Valid
  private List<Price> catalogPrices = new ArrayList();
  @JsonProperty("sku")
  private String sku;

  public Option() {
  }

  @JsonProperty("id")
  public Long getId() {
    return this.id;
  }

  @JsonProperty("id")
  public void setId(Long id) {
    this.id = id;
  }

  @JsonProperty("oid")
  public String getOid() {
    return this.oid;
  }

  @JsonProperty("oid")
  public void setOid(String oid) {
    this.oid = oid;
  }

  @JsonProperty("type")
  public String getType() {
    return this.type;
  }

  @JsonProperty("type")
  public void setType(String type) {
    this.type = type;
  }

  @JsonProperty("name")
  public String getName() {
    return this.name;
  }

  @JsonProperty("name")
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty("label")
  public String getLabel() {
    return this.label;
  }

  @JsonProperty("label")
  public void setLabel(String label) {
    this.label = label;
  }

  @JsonProperty("value")
  public String getValue() {
    return this.value;
  }

  @JsonProperty("value")
  public void setValue(String value) {
    this.value = value;
  }

  @JsonProperty("optionValue")
  public String getOptionValue() {
    return this.optionValue;
  }

  @JsonProperty("optionValue")
  public void setOptionValue(String optionValue) {
    this.optionValue = optionValue;
  }

  @JsonProperty("catalogPrices")
  public List<Price> getCatalogPrices() {
    return this.catalogPrices;
  }

  @JsonProperty("catalogPrices")
  public void setCatalogPrices(List<Price> catalogPrices) {
    this.catalogPrices = catalogPrices;
  }

  @JsonProperty("sku")
  public String getSku() {
    return this.sku;
  }

  @JsonProperty("sku")
  public void setSku(String sku) {
    this.sku = sku;
  }

  public String toString() {
    return (new ToStringBuilder(this)).append("id", this.id).append("oid", this.oid).append("type", this.type).append("name", this.name).append("label", this.label).append("value", this.value).append("optionValue", this.optionValue).append("catalogPrices", this.catalogPrices).append("sku", this.sku).toString();
  }

  public int hashCode() {
    return (new HashCodeBuilder()).append(this.optionValue).append(this.name).append(this.id).append(this.oid).append(this.label).append(this.type).append(this.sku).append(this.value).append(this.catalogPrices).toHashCode();
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof Option)) {
      return false;
    } else {
      Option rhs = (Option)other;
      return (new EqualsBuilder()).append(this.optionValue, rhs.optionValue).append(this.name, rhs.name).append(this.id, rhs.id).append(this.oid, rhs.oid).append(this.label, rhs.label).append(this.type, rhs.type).append(this.sku, rhs.sku).append(this.value, rhs.value).append(this.catalogPrices, rhs.catalogPrices).isEquals();
    }
  }
}
