package com.example.demo.security;

public class Constant {

    final static String SECRET_KEY = "DAOtvVuYrWiH0B1Y";
    final static String INDIA_TIMEZONE = "Asia/Kolkata";
    final static String SECRET_MESSAGE_FORMAT = "dd/MM/yyyy";

}
