package com.example.demo.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * Progressive hashing with Hex codecs
 */
@Slf4j
public class BasicHash {

  private static final HashFunction DEFAULT_HASH = HashFunction.SHA256;

  public static String hash(HashFunction hashFunc, String... messages) {
    try {
      MessageDigest md = MessageDigest.getInstance(hashFunc.getValue());
      for (String message : messages) {
        md.update(message.getBytes());
      }
      return Hex.encodeHexString(md.digest());
    } catch (NoSuchAlgorithmException e) {
      log.error("Exception while creating {} hash: ", hashFunc, e);
      throw new IllegalArgumentException();
    }
  }

  public static String hash(String... messages) {
    return hash(DEFAULT_HASH, messages);
  }

  public static boolean authenticate(HashFunction hashFunc, String hash, String... messages) {
    try {
      return Objects.equals(hash(hashFunc, messages), hash);
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean authenticate(String hash, String... messages) {
    return authenticate(DEFAULT_HASH, hash, messages);
  }

}
