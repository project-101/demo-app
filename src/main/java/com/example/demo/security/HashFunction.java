package com.example.demo.security;

public enum HashFunction {
  SHA512("SHA-512"), SHA256("SHA-256"), SHA1("SHA-1"), MD5("MD5"),
  HmacMD5("HmacMD5"), HmacSHA1("HmacSHA1"), HmacSHA256("HmacSHA256"), HmacSHA512("HmacSHA512");
  String value;

  HashFunction(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}