package com.example.demo.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Progressive Hmac hashing with Hex codecs
 */
@Slf4j
public class HmacHash {

  private static final HashFunction DEFAULT_HASH = HashFunction.HmacSHA256;

  public static String hash(HashFunction hashFunc, String secret, String... messages) {
    try {
      Mac mac = Mac.getInstance(hashFunc.getValue());
      SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), hashFunc.getValue());
      mac.init(secret_key);
      for (String message : messages) {
        mac.update(message.getBytes());
      }
      return Hex.encodeHexString(mac.doFinal());
    } catch (Exception e) {
      log.error("Exception while creating {} hash: ", hashFunc, e);
      throw new IllegalArgumentException();
    }
  }

  public static String hash(String secret, String... messages) {
    return hash(DEFAULT_HASH, secret, messages);
  }

  public static boolean authenticate(HashFunction hashFunc, String hashedMessage, String secret, String... messages) {
    try {
      return Objects.equals(hash(hashFunc, secret, messages), hashedMessage);
    } catch (Exception e) {
      return false;
    }
  }

  public static boolean authenticate(String hashedMessage, String secret, String... messages) {
    return authenticate(DEFAULT_HASH, hashedMessage, secret, messages);
  }

  /**
   * @param hashMessage Hmac sha256 hash generated using current date in india
   *                    Message: Today's day like 15/10/2019
   *                    Key: DAOtvVuYrWiH0B1Y
   * @return true if matches else false
   */
  public static boolean authenticate(String hashMessage) {
    return authenticate(hashMessage, Constant.SECRET_KEY, getCurrentDateInIndia());
  }

  /**
   * @return default authenticate hash
   */
  public static String generateAuthHash() {
    return hash(Constant.SECRET_KEY, getCurrentDateInIndia());
  }

  private static String getCurrentDateInIndia() {
    Date date = new Date();
    DateFormat df = new SimpleDateFormat(Constant.SECRET_MESSAGE_FORMAT);
    df.setTimeZone(TimeZone.getTimeZone(Constant.INDIA_TIMEZONE));
    return df.format(date);
  }

}
