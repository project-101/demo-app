package com.example.demo.security;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES with Hex codecs
 */
@Slf4j
public class AES {

  private static final byte[] KEY = Constant.SECRET_KEY.getBytes();
  private static final String ALGO = "AES";
  private static Cipher ecipher;
  private static Cipher dcipher;

  static {
    try {
      ecipher = Cipher.getInstance(ALGO);
      SecretKeySpec eSpec = new SecretKeySpec(KEY, ALGO);
      ecipher.init(Cipher.ENCRYPT_MODE, eSpec);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }

    try {
      dcipher = Cipher.getInstance(ALGO);
      SecretKeySpec dSpec = new SecretKeySpec(KEY, ALGO);
      dcipher.init(Cipher.DECRYPT_MODE, dSpec);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static String encrypt(String value) {
    try {
      return Hex.encodeHexString(ecipher.doFinal(value.getBytes()));
    } catch (IllegalBlockSizeException | BadPaddingException e) {
      log.error("Error encrypting data: {}", value);
      throw new IllegalArgumentException();
    }
  }

  public static String decrypt(String encryptedValue) {
    try {
      byte[] decryptedValue = Hex.decodeHex(encryptedValue.toCharArray());
      return new String(dcipher.doFinal(decryptedValue));
    } catch (DecoderException | IllegalBlockSizeException | BadPaddingException e) {
      log.error("Error decrypting data: {}", encryptedValue);
      throw new IllegalArgumentException();
    }
  }

  public static boolean authenticate(String value, String encryptedValue) {
    try {
      return value.equals(decrypt(encryptedValue));
    } catch (Exception e) {
      return false;
    }
  }

}
