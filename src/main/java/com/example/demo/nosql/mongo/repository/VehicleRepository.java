package com.example.demo.nosql.mongo.repository;

import com.example.demo.nosql.mongo.model.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Deprecated
@Repository("vehicleRepositoryMongo")
public interface VehicleRepository extends MongoRepository<Vehicle, String> {

}
