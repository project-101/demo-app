package com.example.demo.nosql.mongo.model;

import javax.validation.constraints.NotEmpty;


public class User extends Person {

    @NotEmpty
    private String email;

    public String getEmail() {
        return email.toLowerCase();
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }
}
