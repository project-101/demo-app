package com.example.demo.nosql.mongo.model;

import org.springframework.data.annotation.Id;

import java.io.Serializable;


public class BaseEntity implements Serializable {

    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isNew() {
        return this.id == null;
    }

}
