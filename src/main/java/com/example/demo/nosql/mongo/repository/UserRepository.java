package com.example.demo.nosql.mongo.repository;

import com.example.demo.nosql.mongo.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepositoryMongo")
public interface UserRepository extends MongoRepository<User, String> {

    boolean existsByEmail(String email);

    long deleteByEmail(String email);

    User findByEmail(String email);

}
