package com.example.demo.nosql.mongo.model;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.core.style.ToStringCreator;

import java.util.*;


public class Owner extends Person {

    private Set<Vehicle> vehicles;

    protected Set<Vehicle> getVehiclesInternal() {
        if (this.vehicles == null) {
            this.vehicles = new HashSet<>();
        }
        return this.vehicles;
    }

    protected void setVehiclesInternal(Set<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Vehicle> getVehicles() {
        List<Vehicle> sortedVehicles = new ArrayList<>(getVehiclesInternal());
        PropertyComparator.sort(sortedVehicles,
                new MutableSortDefinition("name", true, true));
        return Collections.unmodifiableList(sortedVehicles);
    }

    public void addVehicle(Vehicle vehicle) {
        getVehiclesInternal().add(vehicle);
    }

    public Vehicle getVehicle(String name) {
        return getVehicle(name, false);
    }

    public Vehicle getVehicle(String name, boolean ignoreNew) {
        name = name.toLowerCase();
        for (Vehicle vehicle : getVehiclesInternal()) {
            if (!ignoreNew) {
                String compName = vehicle.getLicencePlateNo();
                compName = compName.toLowerCase();
                if (compName.equals(name)) {
                    return vehicle;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)

                .append("id", this.getId()).append("new", this.isNew())
                .append("lastName", this.getLastName()).append("firstName", this.getFirstName())
                .append("telephone", this.getTelephone())
                .append("Vehicles", this.vehicles.toString())
                .toString();
    }

}