package com.example.demo.nosql.mongo.model;


import org.springframework.core.style.ToStringCreator;


public class Vehicle {

    private String name;

    private String licencePlateNo;

    private int seats;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicencePlateNo() {
        return licencePlateNo.toUpperCase();
    }

    public void setLicencePlateNo(String licencePlateNo) {
        this.licencePlateNo = licencePlateNo.toUpperCase();
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }


    @Override
    public String toString() {
        return new ToStringCreator(this)

                .append("Name", this.getName())
                .append("Licence-Plate-No", this.licencePlateNo)
                .append("seats", this.seats)
                .toString();
    }

}
