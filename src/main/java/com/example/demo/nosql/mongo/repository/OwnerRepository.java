package com.example.demo.nosql.mongo.repository;

import com.example.demo.nosql.mongo.model.Owner;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("ownerRepositoryMongo")
public interface OwnerRepository extends MongoRepository<Owner, String> {

    long deleteByFirstNameAndLastName(String firstName, String lastName);

    Owner findByFirstNameAndLastName(String firstName, String lastName);

    default Owner findByName(String name) {
        String[] temp = name.split("&");
        String firstName = temp[0].toLowerCase();
        String lastName = temp[1].toLowerCase();
        return this.findByFirstNameAndLastName(firstName, lastName);
    }

    @Query(value = "{'firstName' : ?0}")
    List<Owner> findAllByFirstName(String firstName);

}

//@Slf4j
//class Test {
//
//    public enum X{
//        A("Hello"), B("World");
//
//        String b;
//
//        X(String str){
//            this.b = str;
//        }
//
//    }
//
//    public static void main(String[] args) {
////        for(X a: X.values()){
////            System.out.println(a.b);
////        }
////
////        if(EnumUtils.isValidEnum(X.class,"A"))
////            System.out.println(1);
////        System.out.println(X.A);
////        System.out.println(X.A.b);
//        try{
//            int a = 1/0;
//        }catch (ArithmeticException e) {
//            log.error("this is a error: {}", e);
//        }
//    }
//}