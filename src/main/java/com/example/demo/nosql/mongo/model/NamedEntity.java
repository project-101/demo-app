package com.example.demo.nosql.mongo.model;

import javax.validation.constraints.NotEmpty;


public class NamedEntity extends BaseEntity {

    @NotEmpty
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name.toLowerCase();
    }

    @Override
    public String toString() {
        return this.getName();
    }

}

