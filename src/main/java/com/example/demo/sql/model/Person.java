package com.example.demo.sql.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

@EqualsAndHashCode(callSuper = false)
@Data
@MappedSuperclass
public class Person extends BaseEntity {

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "telephone", length = 10)
  @Size(min = 10, max = 10)
  private String telephone;

}
