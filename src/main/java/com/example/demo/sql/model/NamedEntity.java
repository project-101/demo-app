package com.example.demo.sql.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;


@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
@Data
public class NamedEntity extends BaseEntity {

  @Column(name = "name")
  private String name;

}

