package com.example.demo.sql.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "vehicles")
@Data
public class Vehicle extends NamedEntity {

  @Column(name = "licence_plate_no")
  private String licencePlateNo;

  @Column(name = "seats")
  private int seats;

  @ManyToOne
  @JoinColumn(name = "driver_id")
  @JsonBackReference
  private Driver driver;

}
