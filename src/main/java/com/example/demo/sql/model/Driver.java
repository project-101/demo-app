package com.example.demo.sql.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.ToString;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.*;

@Entity
@Table(name = "drivers")
@ToString
public class Driver extends Person {

  @OneToMany(mappedBy = "driver")
  @JsonManagedReference
  private Set<Vehicle> vehicles;

  protected Set<Vehicle> getVehiclesInternal() {
    if (this.vehicles == null) {
      this.vehicles = new HashSet<>();
    }
    return this.vehicles;
  }

  protected void setVehiclesInternal(Set<Vehicle> vehicles) {
    this.vehicles = vehicles;
  }

  public List<Vehicle> getVehicles() {
    List<Vehicle> sortedVehicles = new ArrayList<>(getVehiclesInternal());
    PropertyComparator.sort(sortedVehicles,
        new MutableSortDefinition("name", true, true));
    return Collections.unmodifiableList(sortedVehicles);
  }

  public void addVehicle(Vehicle vehicle) {
    if (vehicle.isNew()) {
      getVehiclesInternal().add(vehicle);
    }
    vehicle.setDriver(this);
  }

  public Vehicle getVehicle(String name) {
    return getVehicle(name, false);
  }

  public Vehicle getVehicle(String name, boolean ignoreNew) {
    name = name.toUpperCase();
    for (Vehicle vehicle : getVehiclesInternal()) {
      if (!ignoreNew || !vehicle.isNew()) {
        String compName = vehicle.getLicencePlateNo();
        if (compName.equals(name)) {
          return vehicle;
        }
      }
    }
    return null;
  }

}