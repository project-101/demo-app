package com.example.demo.sql.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "address_data")
@Data
public class Address extends BaseEntity {

  @Column(name = "street")
  @NotBlank
  private String street;

  @Column(name = "city")
  @NotBlank
  private String city;

  @Column(name = "state")
  @NotBlank
  private String state;

  @Column(name = "pincode")
  @Size(min = 6, max = 6)
  private String pincode;
}
