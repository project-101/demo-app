package com.example.demo.sql.model;


import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;


@Entity
@Table(name = "trips")
public class Trip extends BaseEntity {

  @Column(name = "schedule_date_time")
  @CreatedDate
  private LocalDateTime scheduleDateTime;

  @OneToOne
  @JoinColumn(name = "vehicle_id")
  private Vehicle vehicle;

  @ManyToOne
  @JoinColumn(name = "route_id")
  private Route route;

  @ManyToMany
  @JoinColumn(name = "user_id")
  private Set<User> users;

  public List<User> getUsers() {
    List<User> sortedUsers = new ArrayList<>(getUsersInternal());
    PropertyComparator.sort(sortedUsers,
        new MutableSortDefinition("firstName", true, true));
    return Collections.unmodifiableList(sortedUsers);
  }

  public void addUsers(User users) {
    if (users.isNew()) {
      getUsersInternal().add(users);
    }
  }

  protected Set<User> getUsersInternal() {
    if (this.users == null) {
      this.users = new HashSet<>();
    }
    return this.users;
  }

  public User getUser(String telephone) {
    return getUser(telephone, false);
  }

  public User getUser(String telephone, boolean ignoreNew) {
    telephone = telephone.toUpperCase();
    for (User user : getUsersInternal()) {
      if (!ignoreNew || !user.isNew()) {
        String compName = user.getTelephone();
        if (compName.equals(telephone)) {
          return user;
        }
      }
    }
    return null;
  }

}
