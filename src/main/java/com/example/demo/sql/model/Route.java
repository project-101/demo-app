package com.example.demo.sql.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = false)
@Entity
@Data
@Table(name = "routes")
public class Route extends BaseEntity {

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "start_address_id")
  private Address start_point;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "end_address_id")
  private Address end_point;

}
