package com.example.demo.sql.repository;

import com.example.demo.sql.model.User;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepositorySql extends Repository<User, Integer> {

  @Transactional(readOnly = true)
  User findById(@Param("id") int id);

  @Transactional(readOnly = true)
  List<User> findAll();

  void save(User user);

  @Transactional
  void deleteById(int id);

}
