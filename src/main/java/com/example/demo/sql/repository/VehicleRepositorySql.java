package com.example.demo.sql.repository;

import com.example.demo.sql.model.Vehicle;
import org.springframework.data.repository.Repository;

public interface VehicleRepositorySql extends Repository<Vehicle, Integer> {
  void save(Vehicle vehicle);
}
