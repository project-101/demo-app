package com.example.demo.sql.repository;

import com.example.demo.sql.model.Address;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

public interface AddressRepositorySql extends Repository<Address, Integer> {

  @Transactional(readOnly = true)
  Address findById(int id);

  void save(Address address);
}
