package com.example.demo.sql.repository;

import com.example.demo.sql.model.Route;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface RouteRepositorySql extends Repository<Route, Integer> {

  @Transactional(readOnly = true)
  Route findById(int id);

  @Transactional(readOnly = true)
  List<Route> findAll();

  void save(Route route);

}
