package com.example.demo.sql.repository;


import com.example.demo.sql.model.Driver;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DriverRepositorySql extends Repository<Driver, Integer> {

  @Query("SELECT driver FROM Driver driver left join fetch driver.vehicles WHERE driver.id =:id")
  @Transactional(readOnly = true)
  Driver findById(@Param("id") Integer id);

  @Transactional(readOnly = true)
  List<Driver> findAll();

  @Transactional
  void deleteById(int id);

  void save(Driver owner);

}
