package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class ThreadPoolConfig {

  public final static String ASYNC_THREAD_POOL = "asyncThreadPool";
  public final static String SYNC_THREAD_POOL = "syncThreadPool";

  @Bean(name = SYNC_THREAD_POOL)
  @ConditionalOnProperty(value = {"thread.sync.core-pool-size",
      "thread.sync.max-pool-size", "thread.sync.queue-size"})
  public Executor syncThreadPool(
      @Value("${thread.sync.core-pool-size}") int corePoolSize,
      @Value("${thread.sync.max-pool-size}") int maxPoolSize,
      @Value("${thread.sync.queue-size}") int queueCapacity
  ) {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setQueueCapacity(queueCapacity);
    executor.setThreadNamePrefix("Sync-Thread-Pool-");
    executor.initialize();
    return executor;
  }

  @Primary
  @Bean(name = ASYNC_THREAD_POOL)
  public Executor asyncThreadPool(
      @Value("${thread.async.core-pool-size}") int corePoolSize,
      @Value("${thread.async.max-pool-size}") int maxPoolSize,
      @Value("${thread.async.queue-size}") int queueCapacity
  ) {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setQueueCapacity(queueCapacity);
    executor.setThreadNamePrefix("Async-Thread-Pool-");
    executor.initialize();
    return executor;
  }

}