package com.example.demo.config;

import com.example.demo.aspect.redislock.TransactionAction;
import com.example.demo.aspect.redislock.TransactionLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.lang.ref.WeakReference;

@Slf4j
@Component
@EnableScheduling
public class JobsScheduleConfig {

  private final static String INDIA_TIMEZONE = "Asia/Kolkata";

  @Scheduled(cron = "0 0 23 ? * *", zone = INDIA_TIMEZONE)
  public void forceGC() {
    log.info("force calling gc");
    Object obj = new Object();
    WeakReference<Object> ref = new WeakReference<>(obj);
    obj = null;
    long count = 0;
    while (ref.get() != null) {
      count++;
      System.gc();
    }
    log.info("Forced called gc after: {} tries", count);
  }

  @Scheduled(cron = "0 0 22 ? * *", zone = INDIA_TIMEZONE)
  @TransactionLock(action = TransactionAction.BYVALUE, value = "GC")
  public void suggestGC() {
    log.info("suggesting gc");
    System.gc();
  }

}