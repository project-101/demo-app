package com.example.demo.config;

import com.google.maps.GeoApiContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoogleConfig {

  @Value("${google.api.key}")
  private String apiKey;

  @Bean
  public GeoApiContext geoApiContext() {
    return new GeoApiContext.Builder()
        .apiKey(apiKey)
        .maxRetries(3)
        .build();
  }

}
