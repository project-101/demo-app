package com.example.demo.config;

import com.example.common.utils.Helper;
import io.lettuce.core.resource.ClientResources;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.StringUtils;

import java.time.Duration;

import static io.lettuce.core.ReadFrom.NEAREST;
import static org.springframework.beans.factory.config.BeanDefinition.SCOPE_PROTOTYPE;

@Configuration
@EnableRedisRepositories(redisTemplateRef = RedisConfig.REDIS_TEMPLATE_BEAN_NAME)
public class RedisConfig {

  public final static String REDIS_CONNECTION_BEAN_NAME = "demoRedisConnectionFactory";
  public final static String REACTIVE_REDIS_CONNECTION_BEAN_NAME = "demoReactiveRedisConnectionFactory";
  public final static String REDIS_CONFIGURATION_BEAN_NAME = "demoRedisSentinelConfiguration";
  public final static String REDIS_TEMPLATE_BEAN_NAME = "demoRedisTemplate";
  public final static String LETTUCE_CONFIGURATION_BEAN_NAME = "demoLettuceClientConfiguration";


  @Value("${redis.demo.sentinel.master}")
  private String master;
  @Value("${redis.demo.sentinel.nodes}")
  private String nodes;
  @Value("${redis.demo.sentinel.password:}")
  private String password;
  @Value("${redis.demo.sentinel.default.database:0}")
  private int database;

  public static <K, V> RedisTemplate<K, V> redisTemplate(int dbIndex) {
    RedisTemplate<K, V> template = new RedisTemplate<>();
    template.setConnectionFactory(lettuceConnectionFactory(dbIndex));
    template.setKeySerializer(new StringRedisSerializer());
    template.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
    template.afterPropertiesSet();
    return template;
  }

  public static ReactiveRedisTemplate<String, Object> reactiveRedisTemplate(int dbIndex) {
    RedisSerializationContext.RedisSerializationContextBuilder<String, Object> builder =
        RedisSerializationContext.newSerializationContext(new StringRedisSerializer());
    return new ReactiveRedisTemplate<>(lettuceConnectionFactory(dbIndex), builder.value(new GenericToStringSerializer<>(Object.class)).hashValue(new Jackson2JsonRedisSerializer<>(Object.class)).build());
  }

  @Primary
  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_CONNECTION_BEAN_NAME)
  public RedisConnectionFactory lettuceConnectionFactory(@Qualifier(REDIS_CONFIGURATION_BEAN_NAME) RedisSentinelConfiguration redisSentinelConfiguration,
                                                         @Qualifier(LETTUCE_CONFIGURATION_BEAN_NAME) LettuceClientConfiguration lettuceClientConfiguration) {
    return new LettuceConnectionFactory(redisSentinelConfiguration, lettuceClientConfiguration);
  }

  @Scope(SCOPE_PROTOTYPE)
  @Bean(REACTIVE_REDIS_CONNECTION_BEAN_NAME)
  public ReactiveRedisConnectionFactory lettuceReactiveConnectionFactory(@Qualifier(REDIS_CONFIGURATION_BEAN_NAME) RedisSentinelConfiguration redisSentinelConfiguration,
                                                                         @Qualifier(LETTUCE_CONFIGURATION_BEAN_NAME) LettuceClientConfiguration lettuceClientConfiguration) {
    return new LettuceConnectionFactory(redisSentinelConfiguration, lettuceClientConfiguration);
  }

  public static LettuceConnectionFactory lettuceConnectionFactory(int dbIndex) {
    LettuceConnectionFactory connectionFactory = (LettuceConnectionFactory) Helper.getContext().getBean(REDIS_CONNECTION_BEAN_NAME, RedisConnectionFactory.class);
    connectionFactory.setDatabase(dbIndex);
    connectionFactory.afterPropertiesSet();
    return connectionFactory;
  }

  @Primary
  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_CONFIGURATION_BEAN_NAME)
  public RedisSentinelConfiguration redisSentinelConfiguration() {
    RedisSentinelConfiguration redisSentinelConfiguration = new RedisSentinelConfiguration(master, StringUtils.commaDelimitedListToSet(nodes));
    redisSentinelConfiguration.setPassword(RedisPassword.of(password));
    redisSentinelConfiguration.setDatabase(database);
    return redisSentinelConfiguration;
  }

  @Primary
  @Scope(SCOPE_PROTOTYPE)
  @Bean(REDIS_TEMPLATE_BEAN_NAME)
  public <K, V> RedisTemplate<K, V> redisTemplate() {
    return redisTemplate(database);
  }

  @Bean(LETTUCE_CONFIGURATION_BEAN_NAME)
  public LettuceClientConfiguration lettuceClientConfiguration() {
    return LettuceClientConfiguration.builder()
        .readFrom(NEAREST)
        .clientResources(ClientResources.create())
        .commandTimeout(Duration.ofSeconds(1))
        .build();
  }

}
