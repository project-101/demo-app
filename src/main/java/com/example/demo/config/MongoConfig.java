package com.example.demo.config;


import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"com.example.schema.repository.mongo"}, mongoTemplateRef = "demoMongoTemplate")
public class MongoConfig extends AbstractMongoClientConfiguration {

  @Value("${mongodb.demo.database}")
  private String databaseName;
  @Value("${mongodb.demo.host}")
  private String host;
  @Value("${mongodb.demo.port}")
  private String port;

  @Primary
  @Bean("demoMongoTemplate")
  @Override
  public MongoTemplate mongoTemplate() throws Exception {
    return new MongoTemplate(this.mongoClient(), this.getDatabaseName());
  }

  @Primary
  @Bean("demoMongoClient")
  @Override
  public MongoClient mongoClient() {
    return MongoClients.create(String.format("mongodb://%s:%s/%s", host, port, this.getDatabaseName()));
  }

  @Override
  protected String getDatabaseName() {
    return databaseName;
  }
}
