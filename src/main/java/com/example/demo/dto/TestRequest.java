package com.example.demo.dto;

import org.springframework.http.HttpMethod;

import javax.validation.constraints.NotBlank;
import java.util.Map;

public class TestRequest {

  @NotBlank(message = "url cannot be empty")
  public String url;

  @NotBlank(message = "method cannot be empty")
  public HttpMethod method;

  @NotBlank(message = "headers cannot be empty")
  public Map<String, String> header;

  public Object data;
}
