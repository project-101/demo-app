package com.example.demo.dto.docsap;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class UnitPriceSummary extends PriceSummary {

  private BigDecimal unitPrice = new BigDecimal(0);
  private BigDecimal discount = new BigDecimal(0);
  private BigDecimal totalPrice = new BigDecimal(0);
  private long qty = 1;

  public String getUnitPrice() {
    return df2.format(unitPrice);
  }

  public String getDiscount() {
    return df2.format(discount);
  }

  public String getTotalPrice() {
    return df2.format(totalPrice);
  }

  public long getQty() {
    return qty;
  }
}
