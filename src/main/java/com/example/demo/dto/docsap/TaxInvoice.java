package com.example.demo.dto.docsap;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
public class TaxInvoice {

  private String title;
  private String invoiceNumber;
  private LocalDate invoiceDate;
  private InvoiceAddress shippingAddress;
  private InvoiceAddress billingAddress;
  private List<Goods> goods;
  private PriceSummary priceSummary;

}
