package com.example.demo.dto.docsap;

import com.example.demo.juno.model.Address;

public class InvoiceAddress extends Address {

  @Override
  public String getFirstName() {
    return super.getFirstName() == null ? "" : super.getFirstName();
  }

  @Override
  public String getLastName() {
    return super.getLastName() == null ? "" : super.getLastName();
  }

  @Override
  public String getPhone() {
    return super.getPhone() == null ? "" : super.getPhone();
  }

  @Override
  public String getPhoneCode() {
    return super.getPhoneCode() == null ? "" : super.getPhoneCode();
  }

  @Override
  public String getEmail() {
    return super.getEmail() == null ? "" : super.getEmail();
  }

  @Override
  public String getAddressline1() {
    return super.getAddressline1() == null ? "" : super.getAddressline1();
  }

  @Override
  public String getAddressline2() {
    return super.getAddressline2() == null ? "" : super.getAddressline2();
  }

  @Override
  public String getCity() {
    return super.getCity() == null ? "" : super.getCity();
  }

  @Override
  public String getState() {
    return super.getState() == null ? "" : super.getState();
  }

  @Override
  public String getPostcode() {
    return super.getPostcode() == null ? "" : super.getPostcode();
  }
}
