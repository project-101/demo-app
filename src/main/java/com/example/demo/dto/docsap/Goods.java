package com.example.demo.dto.docsap;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Goods {

  private String description;
  private UnitPriceSummary unitPriceSummary;

}
