package com.example.demo.dto.docsap;

import lombok.Data;

import java.math.BigDecimal;
import java.text.DecimalFormat;

@Data
public class PriceSummary {

  protected final static DecimalFormat df2 = new DecimalFormat("0.00");

  private BigDecimal subTotal = new BigDecimal(0);
  private BigDecimal IGST = new BigDecimal(0);
  private BigDecimal CGST = new BigDecimal(0);
  private BigDecimal SGST = new BigDecimal(0);
  private BigDecimal grandTotal = new BigDecimal(0);

  public String getSubTotal() {
    return df2.format(subTotal);
  }

  public String getIGST() {
    return df2.format(IGST);
  }

  public String getCGST() {
    return df2.format(CGST);
  }

  public String getSGST() {
    return df2.format(SGST);
  }

  public String getGrandTotal() {
    return df2.format(grandTotal);
  }

  public String getMetadataIGST() {
    if (IGST.compareTo(new BigDecimal(0)) > 0)
      return " @ 12.0";
    return " @ 0.0";
  }

  public String getMetadataCGST() {
    if (CGST.compareTo(new BigDecimal(0)) > 0)
      return " @ 9.0";
    return " @ 0.0";
  }

  public String getMetadataSGST() {
    if (SGST.compareTo(new BigDecimal(0)) > 0)
      return " @ 9.0";
    return " @ 0.0";
  }
}
