package com.example.demo.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TestBackSync {

  @Data
  public static class Items {

    @Data
    public static class Status {
      String state;
      String status;
      boolean returnable = false;
      boolean draftStatus = false;
      boolean cancellable = true;
    }

    Status status = new Status();
    Long itemId;
  }

  @Data
  public static class OrderSync {
    List<Items> items = new ArrayList<>();

    {
      items.add(new Items());
    }
  }

  OrderSync order = new OrderSync();

  Long orderId;

  public void setItemId(Long id) {
    order.items.get(0).itemId = id;
  }

  public void setStatus(String status) {
    order.items.get(0).status.status = status;
  }

  public void setState(String state) {
    order.items.get(0).status.state = state;
  }

  public void setReturnable(boolean state) {
    order.items.get(0).status.returnable = state;
  }

  public void setDraftStatus(boolean state) {
    order.items.get(0).status.draftStatus = state;
  }

  public void setCancellable(boolean state) {
    order.items.get(0).status.cancellable = state;
  }

}

