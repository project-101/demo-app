package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.MediaType;

@Data
@AllArgsConstructor
public class DownloadDto {

  private String filename;

  private byte[] file;

  private MediaType type;

}
