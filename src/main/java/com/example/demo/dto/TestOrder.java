package com.example.demo.dto;

import lombok.Data;

@Data
public class TestOrder {

  Long id;
  String customerEmail;
  TestAddress billingAddress;
  TestAddress shippingAddress;

}
