package com.example.demo.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class TestAddress {

  @Field("id")
  private Long id;
  @Field("customerAddressId")
  private Long customerAddressId;
  @Field("firstName")
  private String firstName;
  @Field("lastName")
  private String lastName;
  @Field("phone")
  private String phone;
  @Field("phoneCode")
  private String phoneCode;
  @Field("email")
  private String email;
  @Field("addressType")
  private String addressType;
  @Field("addressline1")
  private String addressline1;
  @Field("addressline2")
  private String addressline2;
  @Field("city")
  private String city;
  @Field("floor")
  private Integer floor;
  @Field("liftAvailable")
  private Boolean liftAvailable;
  @Field("landmark")
  private String landmark;
  @Field("state")
  private String state;
  @Field("postcode")
  private String postcode;
  @Field("country")
  private String country;
  @Field("locality")
  private String locality;
  @Field("defaultAddress")
  private Boolean defaultAddress;
  @Field("gender")
  private String gender;
}
