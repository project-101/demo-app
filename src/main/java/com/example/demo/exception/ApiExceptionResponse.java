package com.example.demo.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@JsonPropertyOrder({"timestamp", "status", "error", "exception", "message", "path", "traceId", "subExceptions", "debugMessage"})
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiExceptionResponse {

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss", timezone = "Asia/Kolkata")
  private Date timestamp;
  private HttpStatus status;
  private Class exception;
  private String error;
  private String message;
  private String path;
  //New params
  private String debugMessage;
  private List<ApiSubError> subExceptions;
  private String traceId = MDC.get("X-B3-TraceId");

  private ApiExceptionResponse() {
    timestamp = new Date();
    exception = ApiExceptionResponse.class;
    error = "Request failed due to unknown reason";
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex) {
    this();
    exception = ex.getClass();
    this.status = status;
    this.message = "Unexpected Exception";
    this.debugMessage = ex.getLocalizedMessage();
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message) {
    this(status, ex);
    this.message = message;
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message, String error) {
    this(status, ex, message);
    this.error = error;
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message, String error, String path) {
    this(status, ex, message, error);
    this.path = path;
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message, String error, String path, String debugMessage) {
    this(status, ex, message, error, path);
    this.debugMessage = debugMessage;
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message, String error,
                              String path, String debugMessage, List<ApiSubError> subExceptions) {
    this(status, ex, message, error, path, debugMessage);
    this.subExceptions = subExceptions;
  }

  public ApiExceptionResponse(HttpStatus status, Throwable ex, String message, String error,
                              String path, String debugMessage, ApiSubError subException) {
    this(status, ex, message, error, path, debugMessage, Collections.singletonList(subException));
  }

}
