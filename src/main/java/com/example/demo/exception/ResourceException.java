package com.example.demo.exception;


public class ResourceException extends RuntimeException {

  private String issue;

  private String reason;

  private Object solution;

  public ResourceException(String issue, String reason, String solution) {
    super(String.format("Issue: %s, Reason: %s, Solution: %s", issue, reason, solution));
    this.issue = issue;
    this.reason = reason;
    this.solution = solution;
  }

  public String getIssue() {
    return issue;
  }

  public String getReason() {
    return reason;
  }

  public Object getSolution() {
    return solution;
  }
}