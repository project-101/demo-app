package com.example.demo.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiValidationError implements ApiSubError {

  private String object;

  private String field;

  private Object rejectedValue;

  private String message;

  private List<ApiSubError> subExceptions;

  public ApiValidationError(String object, String field) {
    this.object = object;
    this.field = field;
    this.rejectedValue = null;
    this.message = "field have some problem";
  }

  public ApiValidationError(String object, String field, List<ApiSubError> subExceptions) {
    this(object, field);
    this.subExceptions = subExceptions;
  }

  public ApiValidationError(String object, String field, Object rejectedValue) {
    this(object, field);
    this.rejectedValue = rejectedValue;
  }

  public ApiValidationError(String object, String field, Object rejectedValue, String message) {
    this(object, field);
    this.rejectedValue = rejectedValue;
    this.message = message;
  }

}
