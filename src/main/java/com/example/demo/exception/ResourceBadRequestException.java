package com.example.demo.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ResourceBadRequestException extends ResourceException {
  public ResourceBadRequestException(String issue, String reason, String solution) {
    super(issue, reason, solution);
  }
}
