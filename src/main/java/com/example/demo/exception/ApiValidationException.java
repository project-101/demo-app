package com.example.demo.exception;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ApiValidationException extends RuntimeException {

  List<ApiSubError> apiSubError;

  String reason;

  public ApiValidationException(List<ApiSubError> error, String message, String reason) {
    super(message);
    this.reason = reason;
    this.apiSubError = error;
  }

  public ApiValidationException(ApiSubError error, String message, String reason) {
    super(message);
    this.reason = reason;
    this.apiSubError = Collections.singletonList(error);
  }

  public ApiValidationException(String message) {
    this(new ApiValidationError("request body", "all", "null", message), "ValidationError", "Validation failed!");
  }

  public List<ApiSubError> getApiSubError() {
    return apiSubError;
  }

  public String getReason() {
    return reason;
  }
}
