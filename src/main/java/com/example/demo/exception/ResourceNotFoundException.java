package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends ResourceException {
  public ResourceNotFoundException(String issue, String reason, String solution) {
    super(issue, reason, solution);
  }
}
