package com.example.demo.utils;

import com.example.demo.headers.MultiThreadingSafeRequestHeaders;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriTemplateHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@Component
@Slf4j
public class LogUtils {

  private static class Request {
    String headers;
    String path;
    String method;

    @Override
    public String toString() {
      return String.format("Method: %s, Path: %s \nHeaders: %s", method, path, headers);
    }
  }

  @Autowired
  private MultiThreadingSafeRequestHeaders requestHeaders;

  @Autowired
  @Lazy
  private LogUtils logUtils;

  private final static UriTemplateHandler uriTemplateHandler = new DefaultUriBuilderFactory();

  public <T> void logRequest(Object message, T data, Object... params) {
    Request request = new Request();
    if (requestHeaders.getHttpServletRequest() != null) {
      HttpServletRequest servletRequest = requestHeaders.getHttpServletRequest();
      request.headers = ConverterUtils.toStringUsingJavaProperty(
          Collections.list(servletRequest.getHeaderNames()).stream().collect(Collectors.toMap(x -> x, servletRequest::getHeader, (a, b) -> b)));
      request.method = servletRequest.getMethod();
      request.path = servletRequest.getRequestURL().toString();
    }
    logUtils.logRequest(request, message, data, params);
  }

  @Async
  public <T> void logRequest(Request request, Object message, T data, Object... params) {
    StringJoiner loggingData = new StringJoiner(System.lineSeparator());
    loggingData.add("");
    loggingData.add("=========================== LogUtils-->request start ===========================");
    loggingData.add(String.format("=========================== %s ===========================", message));
    loggingData.add(request.toString());
    loggingData.add(String.format("Params: %s", Arrays.toString(params)));
    loggingData.add(String.format("Data: %s", ConverterUtils.toStringUsingJavaProperty(data)));
    loggingData.add("=========================== LogUtils-->request end ===========================");
    log.info(loggingData.toString());
  }

  @Async
  public <T> void logResponse(Object message, T data, Object... params) {
    StringJoiner loggingData = new StringJoiner(System.lineSeparator());
    loggingData.add("");
    loggingData.add("=========================== LogUtils-->response start ===========================");
    loggingData.add(String.format("=========================== %s ===========================", message));
    loggingData.add(String.format("Params: %s", ConverterUtils.toStringUsingJavaProperty(params)));
    loggingData.add(String.format("Data: %s", ConverterUtils.toStringUsingJavaProperty(data)));
    loggingData.add("=========================== LogUtils-->response end ===========================");
    log.info(loggingData.toString());
  }

  @Async
  public void logCurl(String url, HttpMethod method, HttpEntity<?> requestEntity, Object... uriVariables) {
    String nextLine = System.lineSeparator();
    StringJoiner curlRequest = new StringJoiner(nextLine);
    curlRequest.add("");
    curlRequest.add("=========================== LogUtils --> curl start ===========================");
    curlRequest.add(String.format("curl --request %s \\", method));
    curlRequest.add(String.format("--url '%s' \\", uriTemplateHandler.expand(url, uriVariables)));
    if (requestEntity != null) {
      if (!CollectionUtils.isEmpty(requestEntity.getHeaders())) {
        requestEntity.getHeaders().forEach((k, v) ->
            curlRequest.add(String.format("--header '%s: %s' \\",
                k, v.toString().replace("[", "").replace("]", ""))));
      }
      curlRequest.add(String.format("--data '%s'", ConverterUtils.toStringUsingJsonProperty(requestEntity.getBody())));
    }
    curlRequest.add("=========================== LogUtils --> curl end =============================");
    log.info(curlRequest.toString());
  }

}
