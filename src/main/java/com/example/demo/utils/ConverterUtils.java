package com.example.demo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class ConverterUtils {

  private final static Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private final static ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

  /**
   * Json string using java property
   */
  public static String toStringUsingJavaProperty(Object obj) {
    return gson.toJson(obj);
  }

  /**
   * Json string using json property
   */
  public static String toStringUsingJsonProperty(Object obj) {
    try {
      return objectMapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      log.error("Exception while converting to string: {}, Exception: ", obj, e);
    }
    return "";
  }

  /**
   * Convert one class to another class using java properties without using getter and setter.
   */
  public static <T> T convertClassByJavaProperty(Object convertFrom, Class<T> covertTo) {
    return gson.fromJson(gson.toJson(convertFrom), covertTo);
  }

  /**
   * Convert one class to another class using java properties without using getter and setter.
   */
  public static <T> T convertClassByJavaProperty(Object convertFrom, ParameterizedTypeReference<T> covertTo) {
    return gson.fromJson(gson.toJson(convertFrom), covertTo.getType());
  }

  /**
   * Convert one class to another class using json properties using getter and setter.
   */
  public static <T> T convertClassByJsonProperty(Object convertFrom, Class<T> covertTo) {
    try {
      return objectMapper.readValue(objectMapper.writeValueAsString(convertFrom), covertTo);
    } catch (IOException e) {
      log.error("Exception while converting: ConvertFrom: {}, ConvertTo: {}, Exception: {}", convertFrom, covertTo, e);
    }
    return null;
  }

  /**
   * Convert one class to another class using json properties using getter and setter.
   */
  @SuppressWarnings("unchecked")
  public static <T> T convertClassByJsonProperty(Object convertFrom, ParameterizedTypeReference<T> covertTo) {
    try {
      return (T) objectMapper.readValue(objectMapper.writeValueAsString(convertFrom), covertTo.getClass());
    } catch (IOException e) {
      log.error("Exception while converting: ConvertFrom: {}, ConvertTo: {}, Exception: ", convertFrom, covertTo, e);
    }
    return null;
  }

  public static Date convertToDate(String sDate) {
    return convertToDateType1(sDate);
  }

  private static Date convertToDateType1(String date) {
    try {
      return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(date);
    } catch (ParseException e) {
      return convertToDateType2(date);
    }
  }

  private static Date convertToDateType2(String date) {
    try {
      return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(date);
    } catch (ParseException e) {
      return convertToDateType3(date);
    }
  }

  private static Date convertToDateType3(String date) {
    try {
      return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(date);
    } catch (ParseException e) {
      return convertToDateType4(date);
    }
  }

  private static Date convertToDateType4(String date) {
    try {
      return new SimpleDateFormat("yyyy/dd/MM HH:mm:ss").parse(date);
    } catch (ParseException e) {
      throw new RuntimeException("Cannot parse the date");
    }
  }

}
