package com.example.demo.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class HttpHeadersUtils {

  private static String API_AUTH_TOKEN;

  @Value("${juno.api.auth.token:8e8b0816-4c73-4f08-8f7d-022dcd186a91}")
  public void setApiAuthToken(String apiAuthToken) {
    if (HttpHeadersUtils.API_AUTH_TOKEN == null) {
      HttpHeadersUtils.API_AUTH_TOKEN = apiAuthToken;
    }
  }

  public static HttpHeaders getHttpHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return headers;
  }

  public static HttpHeaders getAuthHttpHeader() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.add("X-Auth-Token", API_AUTH_TOKEN);
    return headers;
  }

}
