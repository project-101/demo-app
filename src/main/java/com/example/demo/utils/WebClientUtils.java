package com.example.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
@Slf4j
public class WebClientUtils {

  private final static WebClient webClient = WebClient.builder()
      .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE).build();
  private static LogUtils logUtils;

  public WebClientUtils(ApplicationContext context) {
    logUtils = context.getBean(LogUtils.class);
  }

  private static void logRequest(String url, HttpMethod method, HttpEntity<?> requestEntity, Object... uriVariables) {
    if (log.isDebugEnabled()) {
      logUtils.logCurl(url, method, requestEntity, uriVariables);
    }
  }

  private static WebClient.RequestBodySpec getRequestBodySpec(String url, HttpMethod method, HttpEntity<?> requestEntity, Object... uriVariables) {
    WebClient.RequestBodySpec requestSpec = webClient.method(method).uri(url, uriVariables)
        .headers(headers -> requestEntity.getHeaders().forEach((k, v) -> v.forEach(val -> headers.add(k, val))));
    if (requestEntity.getBody() != null) {
      requestSpec.bodyValue(Objects.requireNonNull(requestEntity.getBody()));
    }
    logRequest(url, method, requestEntity, uriVariables);
    return requestSpec;
  }

  public static <T> Mono<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
                                     Class<T> responseType, Object... uriVariables) {
    return getRequestBodySpec(url, method, requestEntity, uriVariables).retrieve().bodyToMono(responseType);
  }

  public static <T> Mono<T> exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
                                     ParameterizedTypeReference<T> responseType, Object... uriVariables) {
    return getRequestBodySpec(url, method, requestEntity, uriVariables).retrieve().bodyToMono(responseType);
  }

}
