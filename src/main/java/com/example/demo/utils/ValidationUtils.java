package com.example.demo.utils;

import com.example.demo.exception.ApiSubError;
import com.example.demo.exception.ApiValidationError;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class ValidationUtils {

  /**
   * Returns true if expression exist and is not empty or false else returns false
   */
  public static boolean assertThat(Object expression) {
    return expression != null &&
        !((expression instanceof CharSequence && StringUtils.isBlank((CharSequence) expression)) ||
            (expression instanceof Boolean && !(Boolean) expression) ||
            (expression instanceof Collection && ((Collection) expression).isEmpty()) ||
            (expression instanceof Map && ((Map) expression).isEmpty()));
  }

  /**
   * Returns true if any expression exist and is not empty or false
   * else returns false if every expression is either null/empty or false and also append the error with the message.
   */
  public static boolean assertAny(Object... expressions) {
    for (Object expression : expressions) {
      if (assertThat(expression))
        return true;
    }
    return false;
  }

  /**
   * Returns true if every expression exist and is not empty or false
   * else returns false if any expression is either null/empty or false and also append the error with the message.
   */
  public static boolean assertAll(Object... expressions) {
    for (Object expression : expressions) {
      if (!assertThat(expression)) {
        return false;
      }
    }
    return true;
  }

  public static List<ApiSubError> setValidationException(String object, String field, Object rejectedValue, String message,
                                                         List<ApiSubError> validationExceptions) {
    if (validationExceptions == null) {
      validationExceptions = new ArrayList<>();
    }
    validationExceptions.add(new ApiValidationError(object, field, String.valueOf(rejectedValue), message));
    return validationExceptions;
  }

  public static List<ApiSubError> validateThat(String object, String field, Object rejectedValue, String message,
                                               List<ApiSubError> validationExceptions, Object expression) {
    if (!assertThat(expression)) {
      validationExceptions = setValidationException(object, field, rejectedValue, message, validationExceptions);
    }
    return validationExceptions;
  }

  public static List<ApiSubError> validateAny(String object, String field, Object rejectedValue, String message,
                                              List<ApiSubError> validationExceptions, Object... expressions) {
    if (!assertAny(expressions)) {
      validationExceptions = setValidationException(object, field, rejectedValue, message, validationExceptions);
    }
    return validationExceptions;
  }

  public List<ApiSubError> validateAll(String object, String field, Object rejectedValue, String message,
                                       List<ApiSubError> validationExceptions, Object... expressions) {
    if (!assertAll(expressions)) {
      validationExceptions = setValidationException(object, field, rejectedValue, message, validationExceptions);
    }
    return validationExceptions;
  }

  /**
   * @param subException: This method will assert subException and if it is not null then
   *                      it will return a ApiSubError object else null
   */
  public static ApiSubError filter(String object, String field, ApiSubError subException) {
    if (assertThat(subException)) {
      return new ApiValidationError(object, field, Collections.singletonList(subException));
    }
    return null;
  }

  /**
   * @param subExceptions: This method will assert subExceptions and if there some exception in subExceptions then
   *                       it will return a ApiSubError object else null
   */
  public static ApiSubError filter(String object, String field, List<ApiSubError> subExceptions) {
    if (assertThat(subExceptions)) {
      return new ApiValidationError(object, field, subExceptions);
    }
    return null;
  }

  /**
   * @param subException: This method will assert subException and if it is not null then
   *                      it will append a new exception in the subException and returns List of ApiSubError
   */
  public static List<ApiSubError> append(List<ApiSubError> validationExceptions, ApiSubError subException) {
    if (assertThat(subException)) {
      if (validationExceptions == null) {
        validationExceptions = new ArrayList<>();
      }
      validationExceptions.add(subException);
    }
    return validationExceptions;
  }

  /**
   * @param subExceptions: This method will assert subExceptions and if there some exception in subExceptions then
   *                       it will append a new exception in the subExceptions and returns List of ApiSubError
   */
  public static List<ApiSubError> append(List<ApiSubError> validationExceptions, List<ApiSubError> subExceptions) {
    if (assertThat(subExceptions)) {
      if (validationExceptions == null) {
        validationExceptions = new ArrayList<>();
      }
      validationExceptions.addAll(subExceptions);
    }
    return validationExceptions;
  }

}
