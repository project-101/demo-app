package com.example.demo.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.time.Duration;

@Slf4j
@Configuration
public class RestTemplateUtils {

  @Autowired
  private ApplicationContext context;

  private static RestTemplate restTemplate;

  private static LogUtils logUtils;

  @PostConstruct
  public void init() {
    logUtils = context.getBean(LogUtils.class);
  }

  @Value("#{${rest-template-utils.connect-read.timeout:{'3000','4000'}}}")
  public void setTimeout(String[] timeouts) {
    int connectTimeout = 3_000;
    int readTimeout = 4_000;
    if (timeouts.length > 0) {
      try {
        connectTimeout = Integer.parseInt(timeouts[0]);
      } catch (NumberFormatException e) {
        log.error("NumberFormatException, Timeouts are: {}", timeouts[0]);
      }
    }
    if (timeouts.length > 1) {
      try {
        readTimeout = Integer.parseInt(timeouts[1]);
      } catch (NumberFormatException e) {
        log.error("NumberFormatException, Timeouts are: {}", timeouts[1]);
      }
    }
    if (restTemplate == null) {
      restTemplate = new RestTemplateBuilder().setConnectTimeout(Duration.ofMillis(connectTimeout))
          .setReadTimeout(Duration.ofMillis(readTimeout)).build();
    }
  }

  public static void logRequest(String url, HttpMethod method, HttpEntity<?> requestEntity, Object... uriVariables) {
    if (log.isDebugEnabled()) {
      logUtils.logCurl(url, method, requestEntity, uriVariables);
    }
  }

  public static void logResponse(ResponseEntity data) {
    if (log.isDebugEnabled()) {
      logUtils.logResponse("RestTemplateUtils", data.getBody(), data.getStatusCode(), data.getHeaders());
    }
  }

  public static <T> T exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
                               Class<T> responseType, Object... uriVariables) throws RestClientException {
    logRequest(url, method, requestEntity, uriVariables);
    ResponseEntity<T> data = restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
    logResponse(data);
    return data.getBody();
  }

  public static <T> T exchange(String url, HttpMethod method, HttpEntity<?> requestEntity,
                               ParameterizedTypeReference<T> responseType, Object... uriVariables) throws RestClientException {
    logRequest(url, method, requestEntity, uriVariables);
    ResponseEntity<T> data = restTemplate.exchange(url, method, requestEntity, responseType, uriVariables);
    logResponse(data);
    return data.getBody();
  }

  public static <T, K> K exchangePostConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       Class<T> responseType, Class<K> convertType, Object... uriVariables) throws RestClientException {
    T data = exchange(url, method, requestEntity, responseType, uriVariables);
    return ConverterUtils.convertClassByJavaProperty(data, convertType);
  }

  public static <T, K> K exchangePostConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       Class<T> responseType, ParameterizedTypeReference<K> convertType, Object... uriVariables) throws RestClientException {
    T data = exchange(url, method, requestEntity, responseType, uriVariables);
    return ConverterUtils.convertClassByJavaProperty(data, convertType);
  }

  public static <T, K> K exchangePostConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       ParameterizedTypeReference<T> responseType, Class<K> convertType, Object... uriVariables) throws RestClientException {
    T data = exchange(url, method, requestEntity, responseType, uriVariables);
    return ConverterUtils.convertClassByJavaProperty(data, convertType);
  }

  public static <T, K> K exchangePostConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       ParameterizedTypeReference<T> responseType, ParameterizedTypeReference<K> convertType, Object... uriVariables)
      throws RestClientException {
    T data = exchange(url, method, requestEntity, responseType, uriVariables);
    return ConverterUtils.convertClassByJavaProperty(data, convertType);
  }

  public static <T, K> T exchangePreConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       Class<T> responseType, Class<K> convertType, Object... uriVariables) throws RestClientException {
    return exchange(url, method,
        new HttpEntity<>(ConverterUtils.convertClassByJavaProperty(requestEntity.getBody(), convertType), requestEntity.getHeaders()),
        responseType, uriVariables);
  }

  public static <T, K> T exchangePreConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       Class<T> responseType, ParameterizedTypeReference<K> convertType, Object... uriVariables) throws RestClientException {
    return exchange(url, method,
        new HttpEntity<>(ConverterUtils.convertClassByJavaProperty(requestEntity.getBody(), convertType), requestEntity.getHeaders()),
        responseType, uriVariables);
  }

  public static <T, K> T exchangePreConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       ParameterizedTypeReference<T> responseType, Class<K> convertType, Object... uriVariables) throws RestClientException {
    return exchange(url, method,
        new HttpEntity<>(ConverterUtils.convertClassByJavaProperty(requestEntity.getBody(), convertType), requestEntity.getHeaders()),
        responseType, uriVariables);
  }

  public static <T, K> T exchangePreConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity,
       ParameterizedTypeReference<T> responseType, ParameterizedTypeReference<K> convertType, Object... uriVariables)
      throws RestClientException {
    return exchange(url, method,
        new HttpEntity<>(ConverterUtils.convertClassByJavaProperty(requestEntity.getBody(), convertType), requestEntity.getHeaders()),
        responseType, uriVariables);
  }

  public static <T, K, V> V exchangePrePostConversion
      (String url, HttpMethod method, HttpEntity<?> requestEntity, Class<T> responseType,
       Class<K> convertTypePre, Class<V> convertTypePost, Object... uriVariables) throws RestClientException {
    T data = exchange(url, method,
        new HttpEntity<>(ConverterUtils.convertClassByJavaProperty(requestEntity.getBody(), convertTypePre), requestEntity.getHeaders()),
        responseType, uriVariables);
    return ConverterUtils.convertClassByJavaProperty(data, convertTypePost);
  }

}
