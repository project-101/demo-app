package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RefundDetails {
  @JsonProperty("refundId")
  private String refundId;
  @JsonProperty("refund_method")
  private String refundMethod;
  @JsonProperty("refund_amount")
  private BigDecimal refundAmount;
  @JsonProperty("date")
  private Date date;
  @JsonProperty("status")
  private String status;
}
