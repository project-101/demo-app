package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ExchangeDetails {
  @JsonProperty("exchangeParentId")
  private Long exchangeParentId;
  @JsonProperty("exchangeChildId")
  private Long exchangeChildId;
}
