package com.example.demo.returns.model.dto.common;

import lombok.Data;

@Data
public class BaseReason {

  private Long id;

  private String reason;

}
