package com.example.demo.returns.model.dto.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OrderRefundAmount {

  private BigDecimal amountToRefund;

  private String currencyCode;

}

