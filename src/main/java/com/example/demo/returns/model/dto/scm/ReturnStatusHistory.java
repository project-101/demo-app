package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class ReturnStatusHistory {
  @JsonProperty("return_status")
  private String returnStatus;
  @JsonProperty("date")
  private Date date;
}
