package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use resttemplate and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RefundItemRequest {
  @JsonProperty("magento_item_id")
  private Long itemId;
  @JsonProperty("refund_method")
  private String refundMethod;
}
