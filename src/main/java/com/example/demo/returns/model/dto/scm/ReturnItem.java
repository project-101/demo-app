package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use rest template and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReturnItem {

  @JsonProperty("magento_item_id")
  private Long id;
  @JsonProperty("qc_status")
  private String qcStatus;
  @JsonProperty("need_approval")
  private Boolean needApproval;
  @JsonProperty("reasons")
  private List<Reason> reasons;
  @JsonProperty("refund_method")
  private String refundMethod;
  @JsonProperty("do_refund")
  private Boolean doRefund;

  @Data
  @AllArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public static class Reason {
    @JsonProperty("type")
    private String type = "RETURN";
    @JsonProperty("primary_reason_id")
    private Long primaryReasonId;
    @JsonProperty("secondary_reason_id")
    private Long secondaryReasonId;
    @JsonProperty("additional_comments")
    private String additionalComments;
  }

}
