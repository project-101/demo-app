package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use resttemplate and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Address {

  @JsonProperty("first_name")
  private String firstName;

  @JsonProperty("last_name")
  private String lastName;

  @JsonProperty("street_1")
  private String addressline1;

  @JsonProperty("street_2")
  private String addressline2;

  @JsonProperty("city")
  private String city;

  @JsonProperty("state")
  private String state;

  @JsonProperty("country")
  private String country;

  @JsonProperty("post_code")
  private String postcode;

  @JsonProperty("country_code")
  private String countryCode;

  @JsonProperty("email")
  private String email;

  @JsonProperty("phone")
  private String phone = "";

  @JsonIgnore
  private String phoneCode;

  @JsonIgnore
  private String locality;

  @JsonIgnore
  private String landmark;

  public String getCountryCode() {
    return this.country;
  }

  public String getLastName() {
    if (StringUtils.isBlank(this.lastName)) {
      return this.firstName;
    }
    return this.lastName;
  }

  public String getPhone() {
    if (this.phone.length() <= 10) {
      this.phone = String.format("%s-%s", this.phoneCode, this.phone);
    }
    return this.phone;
  }

  public String getAddressline2() {
    if (this.addressline2 != null) {
      String locality = "";
      String street2 = (!StringUtils.isEmpty(this.addressline2)) ? this.addressline2 : "";
      if (!StringUtils.isEmpty(this.locality)) {
        locality = this.locality;
      } else if (!StringUtils.isEmpty(this.landmark)) {
        locality = this.landmark;
      }
      if (StringUtils.isNotBlank(locality)) {
        this.addressline2 = String.format("%s\n%s", street2, locality);
      } else {
        this.addressline2 = street2;
      }
    }
    return this.addressline2;
  }

}
