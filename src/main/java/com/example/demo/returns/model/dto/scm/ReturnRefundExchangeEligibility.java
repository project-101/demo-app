package com.example.demo.returns.model.dto.scm;

import com.example.demo.returns.model.dto.common.OrderRefundAmount;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use resttemplate and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReturnRefundExchangeEligibility {

  @JsonProperty("items")
  private List<ItemEligibility> items = new ArrayList<>();

  @JsonProperty("order_refunded_amount")
  private BigDecimal order_refunded_amount;

  @JsonIgnore
  private OrderRefundAmount orderRefundAmount = new OrderRefundAmount();

  public void setOrder_refunded_amount(BigDecimal order_refunded_amount) {
    this.order_refunded_amount = order_refunded_amount;
    orderRefundAmount.setAmountToRefund(order_refunded_amount);
  }
}
