package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use resttemplate and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReturnCreateResultDTO {

  @JsonProperty("success")
  private Boolean success;
  @JsonProperty("group_id")
  private Long groupId;
  @JsonProperty("returns")
  private List<ReturnData> returns = new ArrayList<>();

  @Data
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public static class ReturnData {
    @JsonProperty("magento_item_id")
    private Long id;
    @JsonProperty("return_id")
    private Long returnId;
  }
}
