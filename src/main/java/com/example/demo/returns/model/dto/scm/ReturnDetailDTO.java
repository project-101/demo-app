package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReturnDetailDTO {

  @JsonProperty("magento_item_id")
  private Long id;
  @JsonProperty("return_id")
  private Long returnId;
  @JsonProperty("pickup_address")
  private Address pickupAddress;
  @JsonProperty("is_self_dispatch")
  private Boolean isSelfDispatch;
  @JsonProperty("uwItemId")
  private Long uwItemId;
  @JsonProperty("exchangeDetails")
  private ExchangeDetails exchangeDetails;
  @JsonProperty("refund_details")
  private List<RefundDetails> refundDetails;
  @JsonProperty("order_refunded_amount")
  private BigDecimal orderRefundedAmount;
  @JsonProperty("return_method")
  private String returnMethod;
  @JsonProperty("return_source")
  private String returnSource;
  @JsonProperty("facility_code")
  private String facilityCode;
  @JsonProperty("return_status_history")
  private List<ReturnStatusHistory> returnStatusHistory;
  @JsonProperty("return_status")
  private String returnStatus;
  @JsonProperty("is_cancellable")
  private Boolean isCancellable;
  @JsonProperty("initiate_refund")
  private Boolean initiateRefund;

}
