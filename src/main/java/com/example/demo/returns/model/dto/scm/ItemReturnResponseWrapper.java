package com.example.demo.returns.model.dto.scm;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemReturnResponseWrapper {
  @JsonProperty("result")
  private ReturnCreateResultDTO result;
}
