package com.example.demo.returns.model.dto.scm;

import com.example.demo.returns.model.dto.common.ItemRefundAmount;
import com.example.demo.returns.model.dto.common.Method;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Use this model only for conversion from juno to scm and vice versa and sending/getting data from scm
 * For converting the data from juno to scm  or scm to juno use Gson only.
 * For sending data or getting data from scm use resttemplate and immediately convert this model
 * into juno using Gson  and not objectmapper
 * Do not use object mapper for converting data from juno to scm or scm to juno
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ItemEligibility {

  @JsonProperty("magento_item_id")
  private Long id;
  @JsonProperty("is_returnable")
  private Boolean isReturnable;
  @JsonProperty("draftStatus")
  private Boolean draftStatus;
  @JsonProperty("do_refund")
  private Boolean isRefundable;
  @JsonProperty("exchange")
  private Boolean isExchangeable;
  @JsonIgnore
  private List<Method> refundMethods;
  @JsonProperty("refund_methods")
  private Set<String> refund_methods = new HashSet<>();
  @JsonIgnore
  private List<Method> exchangeMethods;
  @JsonIgnore
  private ItemRefundAmount refundAmount = new ItemRefundAmount();
  @JsonProperty("item_price")
  private BigDecimal itemPrice;
  @JsonProperty("item_refunded_amount")
  private BigDecimal itemRefundedAmount;
  @JsonProperty("amount_to_refund")
  private BigDecimal amountToRefund;


}
