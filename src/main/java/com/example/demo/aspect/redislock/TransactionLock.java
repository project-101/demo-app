package com.example.demo.aspect.redislock;

import java.lang.annotation.*;

/**
 * A type of distribution lock based on the Redis
 * Create and store a unique combination in redis default db by redisTemplate and table Transaction.
 * first argument of method if exists will act as one of the composite key
 * action - defines the type of transaction
 * Action + first param of method will act as the primary key
 * If duplicate primary key found in the table throws DuplicateTransactionException
 * Life of primary key in the table is 2 minutes while phantom key will have +5 minutes.
 * removes primary key on completion of the transaction/failure of the transaction.
 *
 * @see Transaction
 * @see TransactionAction
 * @see TransactionLockAspect
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionLock {
  TransactionAction action();
  String value() default "";
}
