package com.example.demo.aspect.redislock;

import org.springframework.transaction.IllegalTransactionStateException;

public class DuplicateTransactionException extends IllegalTransactionStateException {
  public DuplicateTransactionException(String msg) {
    super(msg);
  }

  public DuplicateTransactionException(String msg, Throwable cause) {
    super(msg, cause);
  }
}
