package com.example.demo.aspect.redislock;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.StringJoiner;

@Slf4j
@Service
@AllArgsConstructor
public class TransactionService {

  private Gson gson;
  private TransactionRepository transactionRepository;

  private final static String NULL = "";

  public String lock(TransactionLock action) {
    return lock(action, NULL);
  }

  public String lock(TransactionLock action, Object hash) {
    String hashCode = getHash(action, hash);
    if (transactionRepository.findById(hashCode).isPresent()) {
      log.error("Transaction: {} is already in progress", hashCode);
      throw new DuplicateTransactionException(String.format("Transaction: %s is already in progress", hashCode));
    }
    transactionRepository.save(new Transaction(hashCode));
    log.info("Transaction: {} is locked", hashCode);
    return hashCode;
  }

  public String unlock(TransactionLock action) {
    return unlock(action, NULL);
  }

  public String unlock(TransactionLock action, Object hash) {
    String hashCode = getHash(action, hash);
    transactionRepository.delete(new Transaction(hashCode));
    log.info("Transaction: {} is unlocked", hashCode);
    return hashCode;
  }

  private String getHash(TransactionLock action, Object id) {
    StringJoiner hashCode = new StringJoiner("-");
    String code = action.action().name();
    if (TransactionAction.BYVALUE.name().equals(code)) {
      code = action.value();
    }
    hashCode.add(code);
    hashCode.add(gson.toJson(id));
    return hashCode.toString();
  }

}
