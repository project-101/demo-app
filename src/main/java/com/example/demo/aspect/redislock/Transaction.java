package com.example.demo.aspect.redislock;

import lombok.AllArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@AllArgsConstructor
@RedisHash(value = "TRANSACTION_KEYS", timeToLive = 120)
class Transaction {
  @Id
  private String hashCode;
}