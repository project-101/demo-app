package com.example.demo.aspect.redislock;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Aspect
@Service
@Slf4j
@AllArgsConstructor
public class TransactionLockAspect {

  private TransactionService transactionService;

  @Before("@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args()")
  public void before(JoinPoint joinPoint, TransactionLock transactionLock) {
    transactionService.lock(transactionLock);
  }

  @Before("@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args(hash,..)")
  public void before(JoinPoint joinPoint, TransactionLock transactionLock, Object hash) {
    transactionService.lock(transactionLock, hash);
  }

  @AfterReturning("@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args()")
  public void afterReturning(JoinPoint joinPoint, TransactionLock transactionLock) {
    String hashCode = transactionService.unlock(transactionLock);
    log.info("Transaction: {} is finished", hashCode);
  }

  @AfterReturning("@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args(hash,..)")
  public void afterReturning(JoinPoint joinPoint, TransactionLock transactionLock, Object hash) {
    String hashCode = transactionService.unlock(transactionLock, hash);
    log.info("Transaction: {} is finished", hashCode);
  }

  @AfterThrowing(pointcut = "@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args()", throwing = "thrownException")
  public void afterThrowing(JoinPoint joinPoint, final TransactionLock transactionLock, Throwable thrownException) {
    log.error("Transaction failed, Action: {}, StackTrace: ", transactionLock.action(), thrownException);
    if (!(thrownException instanceof DuplicateTransactionException)) {
      transactionService.unlock(transactionLock);
    }
  }

  @AfterThrowing(pointcut = "@annotation(com.example.demo.aspect.redislock.TransactionLock) && @annotation(transactionLock) && args(hash,..)", throwing = "thrownException")
  public void afterThrowing(JoinPoint joinPoint, final TransactionLock transactionLock, Object hash, Throwable thrownException) {
    log.error("Transaction failed, Action: {}, Ref: {}, StackTrace: ", transactionLock.action(), hash, thrownException);
    if (!(thrownException instanceof DuplicateTransactionException)) {
      transactionService.unlock(transactionLock, hash);
    }
  }

}
