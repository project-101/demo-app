package com.example.demo.aspect.redislock;

public enum TransactionAction {
  BYVALUE, CREATE_RETURN_ORDER, CREATE_REFUND_ORDER, CREATE_STORE_CREDIT, CREATE_EXCHANGE_ORDER,
}
