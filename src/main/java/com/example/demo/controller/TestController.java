package com.example.demo.controller;

import com.example.common.utils.sequence.SequenceGenerator;
import com.example.demo.config.ThreadPoolConfig;
import com.example.demo.dto.DownloadDto;
import com.example.demo.dto.TestBackSync;
import com.example.demo.juno.model.Session;
import com.example.demo.service.JunoService;
import com.example.demo.service.RedisLockTestService;
import com.example.demo.service.TestService;
import com.example.demo.service.ThymeLeafPractice;
import com.example.demo.utils.ConverterUtils;
import com.example.demo.utils.HttpHeadersUtils;
import com.example.demo.utils.WebClientUtils;
import com.example.schema.dao.mongo.Player;
import com.example.schema.dto.PlayerMongoDTO;
import com.example.schema.repository.mongo.PlayerMongoRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Mono;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

@AllArgsConstructor
@RestController
@Slf4j
public class TestController {

  private static Queue<String> customerEmail;
  @Qualifier(ThreadPoolConfig.ASYNC_THREAD_POOL)
  private final Executor executor;

  @Value("#{${juno.customer.email.set}}")
  public void setCustomerEmails(Set<String> emails) {
    TestController.customerEmail = new ArrayDeque<>(emails);
  }

  private final SequenceGenerator sequenceGenerator;
  private final ThymeLeafPractice thymeLeafPractice;
  private final TestService testService;
  private final PlayerMongoRepository playerMongoRepository;
  @Qualifier("commonRedisTemplateDefault")
  private final RedisTemplate<String, String> redisTemplateString;
  @Qualifier("commonRedisTemplateDefault")
  private final RedisTemplate<String, Object> redisTemplateObject;
  private final RedisLockTestService redisLockTestService;

  @GetMapping("/test/redislock")
  public void testRedisLock() {
    try {
      redisLockTestService.test1();
    } catch (Exception ignored) {
    }
    try {
      redisLockTestService.test2();
    } catch (Exception ignored) {
    }
    try {
      redisLockTestService.test3("1");
    } catch (Exception ignored) {
    }
    try {
      redisLockTestService.test4("1");
    } catch (Exception ignored) {
    }
    try {
      redisLockTestService.test5("1","2");
    } catch (Exception ignored) {
    }
  }

  @GetMapping("/test/web")
  public int testWebService(@RequestParam(value = "count", defaultValue = "1") int count) {
    int failCount = 0;
    List<CompletableFuture<Mono<Object>>> future = new ArrayList<>(count);
    for (int i = 0; i < count; i++) {
      future.add(testService.testWebClient());
    }
    for (int i = 0; i < count; i++) {
      try {
        future.get(i).get().block();
      } catch (Exception e) {
        failCount++;
        System.out.println(e.getMessage());
      }
    }
    return failCount;
  }

  @GetMapping("/test/rest")
  public int testRestService(@RequestParam(value = "count", defaultValue = "1") int count) {
    int failCount = 0;
    List<CompletableFuture<Object>> future = new ArrayList<>(count);
    for (int i = 0; i < count; i++) {
      future.add(testService.testRestClient());
    }
    for (int i = 0; i < count; i++) {
      try {
        future.get(i).get();
      } catch (Exception e) {
        System.out.println(e.getMessage());
        failCount++;
      }
    }
    return failCount;
  }

  @GetMapping("/test/completable-future")
  public long testCompletableFuture() {
    long start = System.nanoTime();
    testService.testCompletableFuture();
    long end = System.nanoTime();
    return end - start;
  }

  @GetMapping("/test/async")
  public long testAsync() {
    long start = System.nanoTime();
    testService.testAsyncThread();
    long end = System.nanoTime();
    return end - start;
  }

  @GetMapping("/test/sync")
  public long testSync() {
    long start = System.nanoTime();
    testService.testSyncThread();
    long end = System.nanoTime();
    return end - start;
  }

  @GetMapping("/order/address")
  public void syncAddress(@RequestParam("orderIds") Set<Long> orderIds) {
    for (Long id : orderIds) {
      testService.syncShippingAndBillingAddress(id);
    }
  }

  @PostMapping("/order/sync/status")
  public void syncOrderStatus(@RequestBody List<TestBackSync> requests) {
    for (TestBackSync request : requests) {
      System.out.println(ConverterUtils.toStringUsingJavaProperty(request));
      testService.syncOrderStatus(request);
    }
  }

  @PostMapping("/order/sync/status/raw")
  public void syncOrderStatusRaw(@RequestBody String data) {
    String[] rows = data.split("\n");
    String[] columns = rows[0].split("\\s+");
    List<TestBackSync> request = new ArrayList<>();
    for (int i = 1; i < rows.length; i++) {
      TestBackSync perRequest = new TestBackSync();
      String[] row = rows[i].split("\\s+");
      for (int j = 0; j < columns.length; j++) {
        if (j < row.length) {
          switch (columns[j]) {
            case "#id":
              perRequest.setOrderId(new Long(row[j]));
              break;
            case "#itemId":
              perRequest.setItemId(new Long(row[j]));
              break;
            case "#state":
              perRequest.setState(row[j]);
              break;
            case "#status":
              perRequest.setStatus(row[j]);
              break;
            case "#returnable":
              perRequest.setReturnable(Boolean.parseBoolean(row[j]));
              break;
            case "#draftStatus":
              perRequest.setDraftStatus(Boolean.parseBoolean(row[j]));
              break;
            case "#cancelleable":
              perRequest.setCancellable(Boolean.parseBoolean(row[j]));
              break;
          }
        }
      }
      request.add(perRequest);
    }
    WebClientUtils.exchange("http://127.0.0.1:8081/order/sync/status", HttpMethod.POST, new HttpEntity<>(request, HttpHeadersUtils.getHttpHeaders()), Object.class).block();
  }

  @PostMapping("/test/common/mongo/player")
  public Player testMongoSave(@RequestBody PlayerMongoDTO player) {
    player.setId(sequenceGenerator.generateId(Player.class));
    return playerMongoRepository.save(player.playerMongoDao());
  }

  @GetMapping("/test/common/sequence-generator/redis")
  public long testRedisSequenceGenerator(@RequestParam(name = "count", defaultValue = "100") long count) {
    List<CompletableFuture<Long>> x = new ArrayList<>();
    for (long i = 0L; i < count; i++) {
      x.add(testService.testRedisSequenceGenerator());
    }
    x.forEach(a -> {
      try {
        a.get();
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    });
    return 0;
  }

  @GetMapping("/test/common/sequence-generator/redis/sync")
  public long testRedisSequenceGeneratorSync(@RequestParam(name = "count", defaultValue = "100") long count) {
    for (long i = 0L; i < count; i++) {
      testService.testRedisSequenceGeneratorSync();
    }
    return 0;
  }

  @GetMapping("/test/common/sequence-generator/mongo")
  public long testMongoSequenceGenerator(@RequestParam(name = "count", defaultValue = "100") long count) {
    List<CompletableFuture<Long>> x = new ArrayList<>();
    for (long i = 0L; i < count; i++) {
      x.add(testService.testMongoSequenceGenerator());
    }
    x.forEach(a -> {
      try {
        a.get();
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    });
    return 0;
  }

  @GetMapping("/test/common/sequence-generator/mongo/sync")
  public long testMongoSequenceGeneratorSync(@RequestParam(name = "count", defaultValue = "100") long count) {
    for (long i = 0L; i < count; i++) {
      testService.testMongoSequenceGeneratorSync();
    }
    return 0;
  }

  @PostMapping("/test/random")
  public double testRandom(
      @RequestParam("a") String a,
      @RequestBody Map<String, String> b
  ) {
    System.out.println(a);
    System.out.println(b);
    double x = Math.random();
    if (x < 0.5) {
      throw new RestClientException("Random error");
    }
    return x;
  }

  @GetMapping("/test/order/aggressive")
  public void testThreadConfig(@RequestParam(value = "tension", defaultValue = "4", required = false) int tension,
                               @RequestParam(value = "env", defaultValue = "preprod", required = false) String env) {
    JunoService.setup(env);
    AtomicInteger count = new AtomicInteger();
    for (int i = 0; i < tension; i++) {
      String email = customerEmail.poll();
      executor.execute(() -> {
        try {
          log.info("Started for email: {}", email);
          testService.orderRecursionTest(email);
          count.getAndIncrement();
          log.info("Order created successfully for email: {}", email);
          log.info("Current order count: {}", count.get());
        } catch (RestClientException ex) {
          if (ex instanceof RestClientResponseException) {
            String responseBody = ((RestClientResponseException) ex).getResponseBodyAsString();
            log.error("OrderRegression exception, {}", responseBody);
          } else {
            log.error("OrderRegression exception, {}", ex.getLocalizedMessage());
          }
        }
      });
      customerEmail.add(email);
    }
  }

  @GetMapping("/test/customer")
  public void makeCustomers(@RequestParam(value = "count", defaultValue = "10", required = false) int count,
                            @RequestParam(value = "env", defaultValue = "preprod", required = false) String env) {
    StringJoiner emails = new StringJoiner(", ");
    JunoService.setup(env);
    while (count > 0) {
      try {
        JunoService junoService = new JunoService();
        Session session = junoService.createCustomer();
        emails.add("'" + session.getUsername() + "'");
        count--;
      } catch (Exception ignored) {
      }
    }
    log.info(emails.toString());
  }

  @GetMapping("/download/{filename}")
  public ResponseEntity<byte[]> download(@PathVariable("filename")String filename) throws IOException {
    DownloadDto download = testService.imageDownload(filename);
    return ResponseEntity.ok().contentType(download.getType()).body(download.getFile());
  }

  @GetMapping("/test/thymeleaf")
  public String testThymeLeaf() {
    return thymeLeafPractice.getHtmlText(Locale.ENGLISH);
  }

  @GetMapping("/test/multi")
  public void testsome() {
    testService.testSomething();
  }

  @GetMapping("/test/json")
  public Map<String, String> getSampleJson() {
    Map<String, String> sample = new HashMap<>();
    sample.put("abc", "1");
    sample.put("xyz", "2");
    sample.put("123", "3");
    return sample;
  }

  @GetMapping("/test/random")
  public String tryRandom() {
    if(Math.random() < 0.5) {
      return "success";
    }
    throw new RuntimeException("Random failure");
  }

}