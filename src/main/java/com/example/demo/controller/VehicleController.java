package com.example.demo.controller;

import com.example.demo.exception.ResourceBadRequestException;
import com.example.demo.sql.model.Driver;
import com.example.demo.sql.model.Vehicle;
import com.example.demo.sql.repository.DriverRepositorySql;
import com.example.demo.sql.repository.VehicleRepositorySql;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/drivers/{driverId}")
public class VehicleController {

  private final VehicleRepositorySql vehicles;
  private final DriverRepositorySql drivers;

  @ModelAttribute("driver")
  public Driver findDriver(@PathVariable("driverId") int driverId) {
    return this.drivers.findById(driverId);
  }

  @InitBinder("driver")
  public void initOwnerBinder(WebDataBinder dataBinder) {
    dataBinder.setDisallowedFields("id");
  }

  @PostMapping("/vehicles")
  public ResponseEntity<Driver> vehicleCreation(Driver driver, @Valid Vehicle vehicle, BindingResult result) {
    if (StringUtils.hasLength(vehicle.getName()) && vehicle.isNew()
        && driver.getVehicle(vehicle.getLicencePlateNo(), true) != null) {
      result.rejectValue("name", "duplicate", "already exists");
    }
    if (result.hasErrors()) {
      throw new ResourceBadRequestException("POST request failed in add vehicle",
          "Invalid input", "Please correct inputs");
    } else {
      driver.addVehicle(vehicle);
      this.vehicles.save(vehicle);
      return new ResponseEntity<>(driver, HttpStatus.OK);
    }
  }

  @GetMapping("/vehicles")
  public List<Vehicle> getAllUsers(@PathVariable("driverId") int driverId) {
    return drivers.findById(driverId).getVehicles();
  }

}
