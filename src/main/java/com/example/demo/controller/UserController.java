package com.example.demo.controller;

import com.example.demo.exception.ResourceBadRequestException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.service.UserService;
import com.example.demo.sql.model.Address;
import com.example.demo.sql.model.User;
import com.example.demo.sql.repository.AddressRepositorySql;
import com.example.demo.sql.repository.UserRepositorySql;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.core.style.ToStringCreator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class UserController {

  private final UserRepositorySql users;
  private final AddressRepositorySql addresses;
  private final UserService userService;

  @CachePut(value = "usersCache", key = "#user.id")
  @PostMapping("/users")
  public ResponseEntity<User> addUser(@Valid User user, @Valid Address address, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ResourceBadRequestException("POST request failed in add user",
          "Invalid input", "Please correct inputs");
    } else {
      user.setAddress(address);
      this.users.save(user);
      return new ResponseEntity<>(user, HttpStatus.OK);
    }
  }

  @Cacheable(value = "usersCache", key = "#userId")
  @GetMapping("/users/{userId}")
  public User showUsers(@PathVariable("userId") int userId) {
    return this.users.findById(userId);
  }

  @Cacheable(value = "allUsersCache")
  @GetMapping("/users")
  public List<User> showUsers() {
    return this.users.findAll();
  }

  @Caching(
      evict = {@CacheEvict(value = "usersCache", key = "#userId"),
          @CacheEvict(value = "allUsersCache", allEntries = true)}
  )
  @DeleteMapping("/users/{userId}")
  public ResponseEntity<User> removeUser(@PathVariable("userId") int userId) {
    if (this.users.findById(userId) != null) {
      this.users.deleteById(userId);
      return ResponseEntity.ok().build();
    } else {
      throw new ResourceNotFoundException("POST request failed in remove user",
          "User does not exist", "Provide another input");
    }
  }

  @PostMapping("/users/{userId}/book")
  public void sendMessageToKafkaTopic(@PathVariable("userId") int userId) {
    User user = this.users.findById(userId);
    if (user != null) {
      this.userService.bookCab(
          new ToStringCreator(this)
              .append("firstName", user.getFirstName()).append("lastName", user.getLastName())
              .append("telephone", user.getTelephone())
              .append("response", "waiting for the cab")
              .toString());
    }
  }

  @PostMapping("/users/dummy/book")
  public void sendDummyMessage() {
    this.userService.bookCab(
        new ToStringCreator(this)
            .append("firstName", "test").append("lastName", "last")
            .append("telephone", "98101")
            .append("response", "waiting for the cab")
            .toString());

  }

}

