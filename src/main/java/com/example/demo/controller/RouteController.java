package com.example.demo.controller;

import com.example.demo.exception.ResourceBadRequestException;
import com.example.demo.sql.model.Route;
import com.example.demo.sql.repository.RouteRepositorySql;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class RouteController {

  private final RouteRepositorySql routes;

  @PostMapping("/routes")
  public ResponseEntity<Route> addRoute(@Valid @RequestBody Route route, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ResourceBadRequestException("POST request failed in add route",
          "Invalid input", "Please correct inputs");
    } else {
      this.routes.save(route);
      return new ResponseEntity<>(route, HttpStatus.OK);
    }
  }

  @GetMapping("/routes/{routeId}")
  public Route showRoute(@PathVariable("routeId") int routeId) {
    return this.routes.findById(routeId);
  }

  @GetMapping("/routes")
  public List<Route> showRoutes() {
    return this.routes.findAll();
  }

}
