package com.example.demo.controller;

import com.example.demo.exception.ResourceBadRequestException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.sql.model.Driver;
import com.example.demo.sql.repository.DriverRepositorySql;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class DriverController {
  private final DriverRepositorySql drivers;

  @PostMapping("/drivers")
  public ResponseEntity<Driver> addDriver(@Valid Driver driver, BindingResult result) {
    if (result.hasErrors()) {
      throw new ResourceBadRequestException("POST request failed in add driver",
          "Invalid input", "Please correct inputs");
    } else {
      this.drivers.save(driver);
      return new ResponseEntity<>(driver, HttpStatus.OK);
    }
  }

  @GetMapping("/drivers")
  public List<Driver> showAllDrivers() {
    return this.drivers.findAll();
  }

  @GetMapping("/drivers/{id}")
  public Driver showDriver(@PathVariable("id") int id) {
    return this.drivers.findById(id);
  }

  @DeleteMapping("/drivers/{id}")
  public ResponseEntity<Driver> removeDriver(@PathVariable("id") int id) {
    if (this.drivers.findById(id) != null) {
      this.drivers.deleteById(id);
      return ResponseEntity.ok().build();
    } else {
      throw new ResourceNotFoundException("POST request failed in remove driver",
          "Driver does not exist", "Provide another input");
    }
  }

}