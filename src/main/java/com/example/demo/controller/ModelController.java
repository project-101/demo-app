package com.example.demo.controller;

import com.example.demo.aspect.redislock.TransactionLockAspect;
import com.example.demo.returns.model.dto.scm.*;
import com.example.demo.utils.LogUtils;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

@RestController
public class ModelController {

  @Autowired
  private LogUtils logUtils;

  @Autowired
  private Gson gson;

  @Autowired
  private TransactionLockAspect te;

  @GetMapping("/test/bean")
  public void test() {
    logUtils.logRequest("test1", 123, 123, 123, 123, 123);
    logUtils.logRequest("test2", 1234, "qweq", "qwewq", "Qweqwe");
    logUtils.logRequest("test3", 12345);
  }

  @GetMapping("/items")
  public ReturnItem testItems() {
    ReturnItem returnItem = new ReturnItem();
    returnItem.setDoRefund(false);
    returnItem.setId(123L);
    returnItem.setNeedApproval(true);
    returnItem.setQcStatus("OK");
    returnItem.setRefundMethod("BANK");
    returnItem.setReasons(new ArrayList<>());
    returnItem.getReasons().add(new ReturnItem.Reason("RETURN", 1L, 2L, null));
    returnItem.getReasons().add(new ReturnItem.Reason("APPROVAL", 2L, 11L, null));
    return returnItem;
  }

  @GetMapping("/address")
  public Address giveAddress() {
    Address address = new Address();
    address.setCity("New Delhi");
    address.setCountry("India");
    address.setCountryCode("IN");
    address.setEmail("vineetisone@gmail.com");
    address.setFirstName("Vineet");
    address.setLastName("Narayan");
    address.setPhone("+1-9810123985");
    address.setState("Delhi");
    address.setAddressline1("D-5/12 Vashisht park");
    address.setAddressline2("opposite janak cinema");
    return address;
  }

  @PostMapping("/address")
  public void takeAddress(@RequestBody Address address) {
    System.out.println(gson.toJson(address));
  }

  @PostMapping("/refund-exchange")
  public void takeRefundExchange(RefundExchangeMethodDTO refundExchangeMethodDTO) {
    System.out.println(gson.toJson(refundExchangeMethodDTO));
  }

  @GetMapping("/refund/methods/{magento_item_id}")
  public RefundExchangeMethodDTO giveRefundExchange(@PathVariable("magento_item_id") Long itemId) {
    RefundExchangeMethodDTO refundExchangeMethodDTO = new RefundExchangeMethodDTO();
    refundExchangeMethodDTO.setIsExchangeable(Boolean.TRUE);
    refundExchangeMethodDTO.setId(itemId);
    refundExchangeMethodDTO.setRefund_methods(new ArrayList<>());
    refundExchangeMethodDTO.getRefund_methods().add("storecredit");
    refundExchangeMethodDTO.getRefund_methods().add("storecredit");
    refundExchangeMethodDTO.getRefund_methods().add("source");
    refundExchangeMethodDTO.getRefund_methods().add("neft");
//    refundExchangeMethodDTO.getRefund_methods().add("cashfree");
    return refundExchangeMethodDTO;
  }

  @PostMapping("/return/refund/eligibility")
  public ReturnRefundExchangeEligibility testReturnExchangeMethods(@RequestBody ReturnRefundExchangeEligibilityRequest request) {
    ReturnRefundExchangeEligibility output = new ReturnRefundExchangeEligibility();
    output.setItems(new ArrayList<>());
    for (ReturnItem item : request.getItems()) {
      ItemEligibility itemEligibility = new ItemEligibility();
      itemEligibility.setId(item.getId());
      itemEligibility.setIsRefundable(true);
      itemEligibility.setIsExchangeable(true);
      itemEligibility.setIsReturnable(true);
      itemEligibility.setDraftStatus(true);
      output.getItems().add(itemEligibility);
    }
    return output;
  }

  @GetMapping("/return/item/{magento_item_id}/refund/estimate")
  public RefundEstimate refundEstimate(@PathVariable("itemid") Long item) {
    RefundEstimate refundEstimate = new RefundEstimate();
    refundEstimate.setId(item);
    refundEstimate.setRefundAmount(new BigDecimal("1001"));
    return refundEstimate;
  }

  @PostMapping("/item/refund")
  public RefundResult refundCreate(@RequestBody RefundItemRequest request) {
    System.out.println(request);
    RefundResult refundResult = new RefundResult();
    refundResult.setRefundId(1101L);
    return refundResult;
  }

  @PostMapping("/return")
  public ItemReturnResponseWrapper returnOrder(@RequestBody ReturnOrderDTO returnOrder) {
    ItemReturnResponseWrapper rs = new ItemReturnResponseWrapper();
    ReturnCreateResultDTO result = new ReturnCreateResultDTO();
    System.out.println(returnOrder);
    result.setSuccess(Boolean.TRUE);
    result.setGroupId(System.currentTimeMillis());
    result.setReturns(new ArrayList<>());
    for (ReturnItem item : returnOrder.getItems()) {
      ReturnCreateResultDTO.ReturnData returnData = new ReturnCreateResultDTO.ReturnData();
      returnData.setId(item.getId());
      returnData.setReturnId(123L);
      result.getReturns().add(returnData);
    }
    rs.setResult(result);
    return rs;
  }

  @GetMapping("/return/item/{itemId}/return-details")
  public ReturnDetailDTO returnDetails(@PathVariable("itemId") Long itemId) {
    ReturnDetailDTO result = new ReturnDetailDTO();
    result.setId(itemId);
    result.setReturnId(123L);
    result.setPickupAddress(giveAddress());
    result.setIsSelfDispatch(Boolean.TRUE);
    result.setUwItemId(itemId + 2);
    result.setExchangeDetails(new ExchangeDetails());
    result.getExchangeDetails().setExchangeChildId(1123L);
    result.getExchangeDetails().setExchangeParentId(123123L);
    result.setRefundDetails(new ArrayList<>());
    RefundDetails refundDetails = new RefundDetails();
    refundDetails.setDate(new Date());
    refundDetails.setRefundAmount(new BigDecimal("1100"));
    refundDetails.setRefundId("110");
    refundDetails.setRefundMethod("store_credit");
    refundDetails.setStatus("Processing");
    result.getRefundDetails().add(refundDetails);
    result.setOrderRefundedAmount(new BigDecimal("1231"));
    result.setReturnMethod("pickup");
    result.setReturnSource("Web");
    result.setFacilityCode("ST02");
    result.setReturnStatusHistory(new ArrayList<>());
    ReturnStatusHistory returnStatusHistory = new ReturnStatusHistory();
    returnStatusHistory.setDate(new Date());
    returnStatusHistory.setReturnStatus("return_accepted");
    result.getReturnStatusHistory().add(returnStatusHistory);
    result.setReturnStatus("sdkal");
    result.setIsCancellable(true);
    return result;
  }


}
