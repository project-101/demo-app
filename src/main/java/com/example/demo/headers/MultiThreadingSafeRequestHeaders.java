package com.example.demo.headers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Component
public class MultiThreadingSafeRequestHeaders {

  @Autowired
  private RequestHeaders requestHeaders;

  public String getHeaderValue(String headerName) {
    try {
      return requestHeaders.getHeaderValue(headerName);
    } catch (Exception ex) {
      return null;
    }
  }

  public HttpServletRequest getHttpServletRequest() {
    try {
      return requestHeaders.getHttpServletRequest();
    } catch (Exception e) {
      return null;
    }
  }

  public Map<String, String> getAllHeaders() {
    try {
      return requestHeaders.getAllHeaders();
    } catch (Exception e) {
      return null;
    }
  }

  public Map<String, String> getAllCaseInsensitiveHeaders() {
    try {
      return requestHeaders.getAllCaseInsensitiveHeaders();
    } catch (Exception e) {
      return null;
    }
  }

  public MultiValueMap<String, String> getAllHeadersMultiValuedMap() {
    try {
      return requestHeaders.getAllHeadersMultiValuedMap();
    } catch (Exception e) {
      return null;
    }
  }

}
