package com.example.demo.headers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestHeaders {

  @Autowired
  private HttpServletRequest httpServletRequest;


  public String getHeaderValue(String headerName) {
    return httpServletRequest.getHeader(headerName);
  }

  public HttpServletRequest getHttpServletRequest() {
    return httpServletRequest;
  }

  public Map<String, String> getAllHeaders() {
    Map<String, String> headerMap = new HashMap<>();
    Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      headerMap.put(headerName, getHeaderValue(headerName));
    }
    return headerMap;
  }

  public Map<String, String> getAllCaseInsensitiveHeaders() {
    Map<String, String> headerMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      headerMap.put(headerName, getHeaderValue(headerName));
    }
    return headerMap;
  }

  public MultiValueMap<String, String> getAllHeadersMultiValuedMap() {
    MultiValueMap<String, String> headerMap = new LinkedMultiValueMap<>();
    Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      headerMap.add(headerName, getHeaderValue(headerName));
    }
    return headerMap;
  }

}