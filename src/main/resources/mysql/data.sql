USE test_db;

INSERT IGNORE INTO address_data
VALUES (1, 'D-5/12 Vashisht Park', 'New Delhi', 'Delhi', '110046');
INSERT IGNORE INTO address_data
VALUES (2, 'C-5D/6-3E Janakpuri', 'New Delhi', 'Delhi', '110046');
INSERT IGNORE INTO address_data
VALUES (3, 'A-8/12 Uttamnagar', 'New Delhi', 'Delhi', '110046');
INSERT IGNORE INTO address_data
VALUES (4, 'M-45FE Sector-17', 'Gurgoan', 'Haryana', '122026');
INSERT IGNORE INTO address_data
VALUES (5, 'B-9E GreenBolevard, Sector-62 ', 'Noida', 'Uttar Pradesh', '201309');
INSERT IGNORE INTO address_data
VALUES (6, 'DLF Phase 2, Sector-24', 'Gurgoan', 'Haryana', '110046');

INSERT IGNORE INTO users
VALUES (1, 'Vineet', 'Narayan', '9810123985', 1);
INSERT IGNORE INTO users
VALUES (2, 'Vikalp', 'Sharma', '7389429809', 3);
INSERT IGNORE INTO users
VALUES (3, 'Vaibhav', 'Garg', '8932123239', 4);
INSERT IGNORE INTO users
VALUES (4, 'Neha', 'Singh', '8392092309', 2);
INSERT IGNORE INTO users
VALUES (5, 'Faza', 'Aslam', '7832973989', 6);
INSERT IGNORE INTO users
VALUES (6, 'Shruti', 'Malhotra', '8923938273', 5);

INSERT IGNORE INTO drivers
VALUES (1, 'Chandar', 'Paul', '8290128321');
INSERT IGNORE INTO drivers
VALUES (2, 'Raju', 'Sharma', '8907123489');
INSERT IGNORE INTO drivers
VALUES (3, 'Ritesh', 'Singh', '8902123489');
INSERT IGNORE INTO drivers
VALUES (4, 'Sparsh', 'Bansal', '8907123421');
INSERT IGNORE INTO drivers
VALUES (5, 'Kalu', 'Khan', '8907567489');
INSERT IGNORE INTO drivers
VALUES (6, 'Ritu', 'Mittal', '9817123489');

INSERT IGNORE INTO vehicles
VALUES (1, 'Maruti Suzuki Swift Dzire', 'DL98UT0032', 5, 1);
INSERT IGNORE INTO vehicles
VALUES (2, 'Maruti Suzuki Baleno', 'DL78KT0122', 5, 1);
INSERT IGNORE INTO vehicles
VALUES (3, 'Maruti Suzuki Wagon R', 'DL82LT0212', 4, 2);
INSERT IGNORE INTO vehicles
VALUES (4, 'Maruti Suzuki Alto 800', 'DL21MH9022', 4, 2);
INSERT IGNORE INTO vehicles
VALUES (5, 'Hyundai Elite i20', 'DL78MC3456', 5, 2);
INSERT IGNORE INTO vehicles
VALUES (6, 'Hyundai Creta', 'DL11FH9403', 6, 3);
INSERT IGNORE INTO vehicles
VALUES (7, 'Maruti Suzuki Ertiga', 'HR78QW1293', 7, 4);
INSERT IGNORE INTO vehicles
VALUES (8, 'Maruti Suzuki Swift', 'UP72XC4682', 4, 5);
INSERT IGNORE INTO vehicles
VALUES (9, 'Maruti Suzuki Baleno', 'UP81PD4367', 5, 5);
INSERT IGNORE INTO vehicles
VALUES (10, 'Maruti Suzuki Ciaz', 'UP21SA4829', 5, 5);
INSERT IGNORE INTO vehicles
VALUES (11, 'Maruti Suzuki Swift Dzire', 'HR45JK2192', 5, 6);
INSERT IGNORE INTO vehicles
VALUES (12, 'Maruti Suzuki Wagon R', 'HR28OU4738', 4, 6);

INSERT IGNORE INTO routes
VALUES (1, 2, 4);
INSERT IGNORE INTO routes
VALUES (2, 1, 2);
INSERT IGNORE INTO routes
VALUES (3, 5, 3);
INSERT IGNORE INTO routes
VALUES (4, 4, 6);
INSERT IGNORE INTO routes
VALUES (5, 2, 3);
INSERT IGNORE INTO routes
VALUES (6, 6, 1);

INSERT IGNORE INTO trips
VALUES (1, '2019-05-25 12:00:00', 4, 1);
INSERT IGNORE INTO trips
VALUES (2, '2019-05-26 09:15:00', 5, 2);
INSERT IGNORE INTO trips
VALUES (3, '2019-05-27 19:20:00', 1, 3);
INSERT IGNORE INTO trips
VALUES (4, '2019-05-28 07:10:00', 10, 4);
INSERT IGNORE INTO trips
VALUES (5, '2019-05-29 15:30:00', 3, 5);
INSERT IGNORE INTO trips
VALUES (6, '2019-05-30 20:45:00', 8, 6);

INSERT IGNORE INTO trips_users
VALUES (1, 1);
INSERT IGNORE INTO trips_users
VALUES (1, 2);
INSERT IGNORE INTO trips_users
VALUES (1, 3);
INSERT IGNORE INTO trips_users
VALUES (2, 3);
INSERT IGNORE INTO trips_users
VALUES (2, 5);
INSERT IGNORE INTO trips_users
VALUES (3, 4);
INSERT IGNORE INTO trips_users
VALUES (3, 5);
INSERT IGNORE INTO trips_users
VALUES (3, 6);
INSERT IGNORE INTO trips_users
VALUES (4, 1);
INSERT IGNORE INTO trips_users
VALUES (4, 5);
INSERT IGNORE INTO trips_users
VALUES (5, 2);
INSERT IGNORE INTO trips_users
VALUES (5, 3);
INSERT IGNORE INTO trips_users
VALUES (6, 1);
INSERT IGNORE INTO trips_users
VALUES (6, 4);
INSERT IGNORE INTO trips_users
VALUES (6, 6);
