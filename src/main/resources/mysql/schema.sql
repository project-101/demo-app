CREATE DATABASE IF NOT EXISTS test_db;

ALTER DATABASE test_db
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

GRANT ALL PRIVILEGES ON test_db.* TO test@localhost IDENTIFIED BY 'test@1234';

USE test_db;

CREATE TABLE IF NOT EXISTS address_data
(
    id      int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    street  varchar(50) DEFAULT NULL,
    city    varchar(30) DEFAULT NULL,
    state   varchar(30) DEFAULT NULL,
    pincode varchar(6)  DEFAULT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS users
(
    id         int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name varchar(30) DEFAULT NULL,
    last_name  varchar(30) DEFAULT NULL,
    telephone  varchar(10) DEFAULT NULL,
    address_id int(11)     DEFAULT NULL,
    FOREIGN KEY (address_id) REFERENCES address_data (id),
    INDEX (telephone)
) engine = InnoDB;

CREATE TABLE IF NOT EXISTS drivers
(
    id         int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    first_name varchar(255) DEFAULT NULL,
    last_name  varchar(255) DEFAULT NULL,
    telephone  varchar(10)  DEFAULT NULL
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS vehicles
(
    id               int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name             varchar(255) DEFAULT NULL,
    licence_plate_no varchar(255) DEFAULT NULL,
    seats            int(11)      DEFAULT NULL,
    driver_id        int(11)      DEFAULT NULL,
    FOREIGN KEY (driver_id) REFERENCES drivers (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS routes
(
    id               int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    start_address_id int(11) DEFAULT NULL,
    end_address_id   int(11) DEFAULT NULL,
    FOREIGN KEY (start_address_id) REFERENCES address_data (id),
    FOREIGN KEY (end_address_id) REFERENCES address_data (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS trips
(
    id                 int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    schedule_date_time datetime DEFAULT NULL,
    vehicle_id         int(11)  DEFAULT NULL,
    route_id           int(11)  DEFAULT NULL,
    FOREIGN KEY (route_id) REFERENCES routes (id),
    FOREIGN KEY (vehicle_id) REFERENCES vehicles (id)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS trips_users
(
    trip_id  int(11) NOT NULL PRIMARY KEY,
    users_id int(11) NOT NULL,
    FOREIGN KEY (trip_id) REFERENCES trips (id),
    FOREIGN KEY (users_id) REFERENCES users (id)
) ENGINE = InnoDB;
