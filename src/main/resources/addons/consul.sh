curl --location --request PUT 'http://localhost:8500/v1/kv/myconfig/demo%2Cdev/data' \
--header 'Content-Type: text/plain' \
--data-raw '# MY SQL
spring.datasource.url=jdbc:mysql://localhost:3306/test_db
spring.datasource.username=test
spring.datasource.password=test@1234
#spring.datasource.schema=classpath*:db/mysql/schema.sql
#spring.datasource.data=classpath*:db/mysql/data.sql
#spring.datasource.initialization-mode=always
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQLDialect
spring.jpa.hibernate.ddl-auto=none

# Kafka
spring.kafka.consumer.bootstrap-servers=localhost:9092
spring.kafka.consumer.group-id=recent_activity
spring.kafka.consumer.auto-offset-reset=earliest
spring.kafka.consumer.key-deserializer=org.apache.kafka.common.serialization.StringDeserializer
spring.kafka.consumer.value-deserializer=org.apache.kafka.common.serialization.StringDeserializer
spring.kafka.producer.bootstrap-servers=localhost:9092
spring.kafka.producer.key-serializer=org.apache.kafka.common.serialization.StringSerializer
spring.kafka.producer.value-serializer=org.apache.kafka.common.serialization.StringSerializer

# Redis
spring.cache.type=redis
redis.demo.sentinel.master=mymaster
redis.demo.sentinel.nodes=127.0.0.1:26379,127.0.0.1:26380

# Consul
spring.cloud.loadbalancer.ribbon.enabled=false

## Demo Mongo
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
mongodb.demo.database=demo
mongodb.demo.host=localhost
mongodb.demo.port=27017

# Solr
spring.data.solr.host=http://localhost:8983/solr/

# Thread Pool
thread.running.core-pool-size=30
thread.running.max-pool-size=30
thread.running.queue-size=10000
thread.background.core-pool-size=20
thread.background.max-pool-size=20
thread.background.queue-size=10000
'

curl --location --request PUT 'http://localhost:8500/v1/kv/myconfig/common%2Cdev/data' \
--header 'Content-Type: text/plain' \
--data-raw '## Google Api Key
google.api.key=AIzaSyCWbsYDZotlgaOkZNURJ-l-KhaPbocO0Uo

## Utils
rest-template-utils.connect-read.timeout={'\''3000'\'','\''4000'\''}

## Header
juno.api.auth.token=8e8b0816-4c73-4f08-8f7d-022dcd186a91

## Common Mongo
mongodb.common.database=demo_common
mongodb.common.host=localhost
mongodb.common.port=27017

# Redis
spring.cache.type=redis
redis.common.sentinel.master=mymaster